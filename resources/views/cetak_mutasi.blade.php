<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $data->no_sk }}</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <style>
        @page {
            margin: 100px 10px;
        }

        main {
            margin: 100px 10px;
        }

        header {
            position: fixed;
            top: -70px;
            height: 30px;
            text-align: right;
            width: 100%;
        }

        footer {
            position: fixed;
            bottom: -80px;
            background-color: white;
            color: black;
            line-height: 15px;
        }

        body {
            font-family: 'Arial', sans-serif;
            margin: 0;
            padding: 0;
        }

        .container {
            width: 85%;
            margin: 0 auto;
        }

        .headertext {
            margin-top: -70px;
            text-align: center;
            font-size: 15px;
        }

        .content {
            text-align: justify;
            font-size: 13px;
            margin-top: 30px;
        }

        .tentang {
            margin-top: 5px;
            text-align: center;
            font-size: 15px;
            font-weight: bold;
            margin-bottom: -15px;
        }

        .headerbag {
            padding: 8px;
            text-align: center;
            font-size: 15px;
            font-weight: bold;
        }

        .direksi {
            margin-top: -10px;
        }

        .custom-list-aa {
            padding-left: 20px;
        }

        .custom-list-ab {
            padding-left: 20px;
            padding-bottom: 0px;
            margin-bottom: 0px;
        }

        .nomor-huruf {
            font-weight: bold;
            margin-top: -21px;
        }

        .nomor-huruf-a {
            font-weight: bold;
            margin-top: -5px;
        }

        .custom-list-ac li:first-child {
            list-style-type: none;
        }

        .custom-list-ac {
            margin-left: -30px;
            padding-bottom: 0px;
            margin-bottom: 0px;
        }

        .li-a {
            margin-left: 0px;
        }

        .titik-dua-a1 {
            margin-left: 35px;
            margin-top: -58px;
            margin-right: 15px;
        }

        .titik-dua-a2 {
            margin-left: 35px;
            margin-bottom: 0px;
            margin-right: 15px;
        }

        .titik-dua-a3 {
            margin-left: 28px;
            margin-right: 8px;
            margin-top: -5px;
        }

        .titik-dua-a4 {
            margin-left: 28px;
            margin-top: -23px;
            margin-right: 8px;
        }

        .menimbang {
            margin-top: -58px;
        }

        .mengingat {
            margin-bottom: -16px;
        }

        .memutuskan {
            font-weight: bold;
            text-align: center;
            font-size: 13px;
            margin-top: 15px;
            margin-bottom: 10px;
        }

        table {
            border-collapse: collapse;
        }

        tr {
            margin: 0;
            padding: 0;
            border-collapse: collapse;
        }

        td {
            margin: 0;
            padding: 0;
            border-collapse: collapse;
        }

        .container-ttd {
            float: right;
            margin-top: 30px;
        }

        hr.custom-hr {
            width: 110%;
            background-color: black;
            height: 1px;
            float: center;
            margin-top: -10px;
        }

        .table-peg table {
            width: 100%;
            border-collapse: collapse;
            border: 1px solid black;
        }

        .table-peg table th {
            border: 1px solid black;
            padding: 5px;
            text-align: center;
            font-size: 13px;
            font-weight: bold;
        }

        .table-peg table td {
            border: 1px solid black;
            padding: 5px;
            text-align: left;
            font-size: 12px;
        }
    </style>
</head>

<body>

    <header>
        <img src="{{ public_path('images/ims.png') }}" style="width: 15%" align="right">
    </header>

    <footer>
        <font size="2"><strong>PT INKA MULTI SOLUSI</strong></font>
        <p class="text-justify">
            <font size="1">Kantor Pusat :Jl. Raya Surabaya-Madiun KM 161,No.01, Madiun. No Telp.(0351)
                2812105/2812256 Website: www.inkamultisolusi.co.id, E-mail:sekretariat@inkamultisolusi.co.id</font><br>
            <img src="{{ public_path('images/header.jpg') }}" width="100%">
        </p>
    </footer>

    <main>
        <div class="container">
            <div class="headertext">
                <b>
                    <p>KEPUTUSAN DIREKSI PT INKA MULTI SOLUSI <br> Nomor: {{ $data->no_sk }}</p>
                </b>
            </div>
            <div class="tentang">
                <p>TENTANG </p>
            </div>
            <div class="headerbag">
                <p>TENTANG PENEMPATAN DAN ROTASI PEGAWAI
                    <br> DI
                    LINGKUNGAN PT INKA
                    MULTI SOLUSI <br>
                <div class="direksi">DIREKSI PT INKA
                    MULTI
                    SOLUSI</div>
                </p>
            </div>
            <div class="content">
                <table>
                    {{-- menimbang --}}
                    {{-- list 1 --}}
                    <tr>
                        <td>
                            <div class="menimbang">
                                <p>Menimbang</p>
                            </div>
                        </td>
                        <td>
                            <p class="titik-dua-a1">:</p>
                        </td>
                        <td>
                            <ol type="a" class="custom-list-aa">
                                <li>bahwa merujuk pada Peraturan Direksi PT INKA
                                    Multi
                                    Solusi Nomor PER-007/IMS/2023 tentang Struktur
                                    Organisasi PT INKA Multi Solusi dan Peraturan
                                    Direksi PT INKA Multi Solusi Nomor
                                    PER-008/IMS/2023
                                    tentang Struktur Organisasi Tingkat Bagian dan
                                    Fungsi PT INKA Multi Solusi, maka dipandang
                                    perlu
                                    untuk melakukan Penempatan dan Rotasi Pagawai di
                                    Lingkungan PT INKA Mulu Solusi</li>
                            </ol>
                        </td>
                    </tr>
                    {{-- list 2 --}}
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <ol type="a" start="2" class="custom-list-aa">
                                <li>bahwa berdasarkan pertimbangan huruf a diatas,
                                    serta guna tertib administrasinya. maka
                                    Penempatan dan Rotasi Pegawai di Lingkungan PT
                                    INKA Multi Solusi tersebut perlu untuk
                                    dituangkan dalam Koputunan Diroksi.
                                </li>
                            </ol>
                        </td>
                    </tr>
                    {{-- mengingat --}}
                    {{-- list 1 --}}
                    <tr>
                        <td>
                            <div class="mengingat">
                                <p>Mengingat</p>
                            </div>
                        </td>
                        <td>
                            <p class="titik-dua-a2">:</p>
                        </td>
                        <td>
                            <ol type="1" start="1" class="custom-list-ab"
                                style="padding-bottom: 0px; margin-bottom: 0px;">
                                <li>Undang-Undang Nomor 40 Tahun 2007 tentang
                                    Perseroan Terbatas
                                </li>
                            </ol>
                        </td>
                    </tr>
                    {{-- list 2 --}}
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <ol type="1" start="2" class="custom-list-ab">
                                <li>Anggaran dasar PT INKA Multi Solusi berdasarkan
                                    Akta Notaris Maria Liliana Handojo, S.H., Nomor
                                    66
                                    tanggal 23 Desember 2009 yang telah mendapatkan
                                    pengesahan dari Menteri Hukum dan Hak Asasi
                                    Manusia Republik Indonesia berdasarkan Keputusan
                                    Nomor AHU-10305.AH.01.01 Tahun 2010 tanggal 25
                                    Februari 2010 yang telah beberapa kali diubah,
                                    terakhir dengan Akta Notaris Nanik Yuniarti,
                                    S.H., M.Kn., Nomor 575 tanggal 17 Oktober 2022
                                    yang telah mendapatkan pengesahan dari Menteri
                                    Hukum dan Hak Asasi Manusia Republik Indonesia
                                    berdasarkan Keputusan Nomor AHU-0077912.AH.01.02
                                    Tahun 2022 tanggal 27 Oktober 2022
                                </li>
                            </ol>
                        </td>
                    </tr>
                    {{-- list 3 --}}
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <ol type="1" start="3" class="custom-list-ab">
                                <li>Akta Susunan Pengurus PT INKA Multi Solusi yang
                                    telah mengalami beberapa kali perubahan,
                                    terakhir dengan Akta Notaris Nanik Yuniarti,
                                    S.H.
                                    M.Kn.. Nomor 261 tanggal 16 Agustus 2023.
                                </li>
                            </ol>
                        </td>
                    </tr>
                    {{-- list 4 --}}
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <ol type="1" start="4" class="custom-list-ab">
                                <li>Peraturan Direksi PT INKA Multi Solusi Nomor:
                                    PER-007/IMS/2019 tentang Pedoman Tata Naskah
                                    Dinas PT INKA Multi Solusi.
                                </li>
                            </ol>
                        </td>
                    </tr>
                    {{-- list 5 --}}
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <ol type="1" start="5" class="custom-list-ab">
                                <li>Peraturan Direksi PT INKA Multi Solusi Nomor
                                    PER-004/IMS/2021 tentang Jenjang Karir Pegawai
                                    PT
                                    INKA Multi Solusi
                                </li>
                            </ol>
                        </td>
                    </tr>
                    {{-- list 6 --}}
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <ol type="1" start="6" class="custom-list-ab">
                                <li>Peraturan Direksi PT INKA Multi Solusi Nomor
                                    PER-001A/IMS/2023 tentang Sistem Remunerasi PT
                                    INKA Multi Solusi
                                </li>
                            </ol>
                        </td>
                    </tr>
                    {{-- list 7 --}}
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <ol type="1" start="7" class="custom-list-ab">
                                <li>
                                    Peraturan Direksi PT INKA Multi Solusi Nomor
                                    PER-007/IMS/2023 tentang Struktur Organisasi PT
                                    INKA Multi Solusi
                                </li>
                            </ol>
                        </td>
                    </tr>
                    {{-- list 8 --}}
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <ol type="1" start="8" class="custom-list-ab">
                                <li>
                                    Peraturan Direksi PT INKA Multi Solusi Nomor
                                    PER-008/IMS/2023 tentang Struktur Organisasi
                                    Tingkat Bagian dan Fungsi PT INKA Multi Solusi
                                </li>
                            </ol>
                        </td>
                    </tr>
                    {{-- list 9 --}}
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <ol type="1" start="9" class="custom-list-ab">
                                <li>Perjanjian Kerja Bersama PT INKA Multi Solusi
                                    periode 2023-2025</li>
                            </ol>
                        </td>
                    </tr>
                </table>
                <p class="memutuskan">MEMUTUSKAN:</p>
                <table>
                    <tr>
                        <td>
                            <p class="nomor-huruf-a">Menetapkan</p>
                        </td>
                        <td>
                            <p class="titik-dua-a3">:</p>
                        </td>
                        <td>
                            <ol class="custom-list-ac">
                                <b>
                                    <li class="li-a">KEPUTUSAN DIREKSI TENTANG
                                        PENEMPATAN DAN
                                        ROTASI PEGAWAI DI LINGKUNGAN PT INKA MULTI
                                        SOLUSI</li>
                                </b>
                            </ol>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="nomor-huruf">Kesatu</p>
                        </td>
                        <td>
                            <p class="titik-dua-a4">:</p>
                        </td>
                        <td>
                            <ol class="custom-list-ac">
                                <li>
                                    Pegawai yang namanya tersebut dalam Lampiran
                                    Keputusan Direksi ini, yaitu dalam kolom (2)
                                    ditetapkan untuk ditempatkan dan dirotasi di
                                    Unit Kerja sebagaimana tersebut dalam kolom (7)
                                    Lampiran Surat Keputusan Direksi ini.
                                </li>
                            </ol>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="nomor-huruf">Kedua</p>
                        </td>
                        <td>
                            <p class="titik-dua-a4">:</p>
                        </td>
                        <td>
                            <ol class="custom-list-ac">
                                <li>
                                    Kepada Pegawai yang ditempatkan sebagaimana
                                    tersebut pada diktum KESATU diberikan
                                    penghasilan dan fasilitas sesuai dengan
                                    ketentuann yang berlaku di PT INKA Multi Solusi
                                </li>
                            </ol>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="nomor-huruf">Ketiga</p>
                        </td>
                        <td>
                            <p class="titik-dua-a4">:</p>
                        </td>
                        <td>
                            <ol class="custom-list-ac">
                                <li>
                                    Keputusan Direksi ini berlaku terhitung sejak
                                    tanggal ditetapkan dengan ketentuan bahwa
                                    apabila dikemudian hari terdapat kekeliruan
                                    didalamnya akan dilakukan pembetulan sebagaimana
                                    mestinya.
                                </li>
                            </ol>
                        </td>
                    </tr>
                </table>
                <div class="container-ttd">
                    <p>Ditetapkan di : Madiun</p>
                    <p style="margin-top: -10px;">Pada tanggal : {{ $data->tgl_indo }}</p>
                    <hr class="custom-hr">
                    <b>
                        <p style="text-align: end; font-weight: bold;">
                            DIREKSI PT INKA MULTI
                            SOLUSI</p>
                        <img src="data:image/png;base64, {!! $qrcode !!}"
                            style="height: 70px;margin-bottom:10px;">
                        <p style="text-align: end; font-weight: bold;">
                            @if (Auth::check())
                                Pengguna sudah login.
                            @else
                                Pengguna belum login.
                            @endif
                        </p>
                    </b>
                </div>
            </div>
            <div class="table-peg">
                <table style="page-break-before: always; border: 1px solid black;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Unit Sebelum</th>
                            <th>Unit Sesudah</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($data->mutasiLists as $mutasiList)
                            <tr>
                                <td style="text-align: center;">{{ $i++ }}</td>
                                <td>{{ $mutasiList->pegawai_nip }}</td>
                                <td>{{ $mutasiList->pegawai_nama }}</td>
                                <td>{{ $mutasiList->pegawai_unit_name_prev }}</td>
                                <td>{{ $mutasiList->pegawai_unit_name_aft }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </main>
</body>

</html>
