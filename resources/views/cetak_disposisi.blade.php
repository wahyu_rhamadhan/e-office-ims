<!DOCTYPE html>
<html>
<head>
	<title>Surat Perintah Perjalanan Dinas</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    
	<style type="text/css">
        body {font-family: Arial, Helvetica, sans-serif;}
        table {border-collapse: collapse; width: 100%; }
        table { margin: 1px; }
		table tr td,
		table tr th{
			font-size: 12px;
		}
        td{
            word-wrap: break-word;
        }
         .border { border: 1.5px solid black;  }
         .page_break { page-break-before: always; }
         input[type=checkbox] {
            transform: scale(1.5);
            display: inline-block;
            vertical-align: middle;
        }
        .checkbox {
            display: flex!important;
            align-items: center!important;
            justify-content: center;
            align-self: center;
        }

        .label-checkbox {
            margin-left:5px;
            position: relative;
            top:5
        }
	</style>
 
	<table border="2">
        <thead>
            <tr>
                <td width="25%" align="center"><img src="{{ $logo }}" style="height: 50px;"></td>
                <th width="50%" style="text-align: center!important"><span style="font-size: 20px!important;text-align: center!important"><b>SURAT MASUK</b></span></th>
                <td width="25%">
                    No           : 1212121212
                    <br>
                    Tgl diterima : 1212121212
                    </p>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="3">
                    <table border="0">
                        <tr>
                            <td width="10%">Surat dari</td>
                            <td width="3%">:</td>
                            <td width="87%">Kadiv Teknologi</td>
                        </tr>
                        <tr>
                            <td width="10%">No. Surat</td>
                            <td width="3%">:</td>
                            <td width="87%">Kadiv Teknologi</td>
                        </tr>
                        <tr>
                            <td width="10%">Tgl Surat</td>
                            <td width="3%">:</td>
                            <td width="87%">Kadiv Teknologi</td>
                        </tr>
                        <tr>
                            <td width="10%">Perihal</td>
                            <td width="3%">:</td>
                            <td width="87%">Kadiv Teknologi</td>
                        </tr>
                        <tr>
                            <td width="10%">Kepada</td>
                            <td width="3%">:</td>
                            <td width="87%">Kadiv Teknologi</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center"><span style="font-size: 14px!important;text-align: center!important"><b>DISPOSISI</b></span></td>
            </tr>
            <tr>
                <td colspan="3">
                    Untuk : 
                    <br>
                    <table border="0">
                        <tr>
                            <td width="30%">Dendi Madisanto</td>
                            <td width="20%">652100045</td>
                            <td width="50%">Staf Teknologi Informasi</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr> 
                <td colspan="3">
                    <table border="0">
                        <tr>
                            <td width="50%" style="vertical-align: center;">
                                <div class="checkbox">
                                    <input type='checkbox'>
                                    <label class="label-checkbox">Diskusikan dengan saya</label>
                                </div>
                            </td>
                            <td width="50%">
                                <div class="checkbox">
                                    <input type='checkbox'>
                                    <label class="label-checkbox">Untuk dibalas</label>
                                </div>
                            </td>
                        </tr>
                         <tr>
                            <td width="50%" style="vertical-align: center;">
                                <div class="checkbox">
                                    <input type='checkbox'>
                                    <label class="label-checkbox">Untuk dilaksanakan</label>
                                </div>
                            </td>
                            <td width="50%">
                                <div class="checkbox">
                                    <input type='checkbox'>
                                    <label class="label-checkbox">Untuk diketahui</label>
                                </div>
                            </td>
                        </tr>
                         <tr>
                            <td width="50%" style="vertical-align: center;">
                                <div class="checkbox">
                                    <input type='checkbox'>
                                    <label class="label-checkbox">Untuk ditindaklanjuti</label>
                                </div>
                            </td>
                            <td width="50%">
                                <div class="checkbox">
                                    <input type='checkbox'>
                                    <label class="label-checkbox">Untuk diarsip</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> 
                                Catatan :
                                <br>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae eum fugit, praesentium debitis ullam et numquam temporibus eligendi amet odio. Exercitationem deleniti reiciendis sapiente laborum, maiores facere dolore voluptatem aspernatur?</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    

</body>
</html>