<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $data->nomor_surat }}</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

        <style>
            @page {
                margin: 100px 25px;
            }

            main {
                margin: 100px 25px;
            }

            header {
                position: fixed;
                top: -70px;
                height: 30px;
                text-align: right;
                width: 100%;
            }

            footer {
                position: fixed;
                bottom: -80px;
                background-color: white;
                color: black;
                line-height: 15px;
            }

            .konten{
                font-size: 13px;
                text-align: justify;
            }

            /* .konten p {
                line-height: 1.5!important;
                margin-top: -15px;
            } */

            .table {
                page-break-after: auto!important;page-break-inside:avoid;
            }

            .table table {
                border : 1px solid black;
            }

            .table table td {
                border : 1px solid black;
            }

            /* .konten ol {
                margin-top: -15px!important;
            }

            .konten ul {
                margin-top: -15px!important;
            } */
        </style>
</head>

<body>

    <header>
        <img src="{{ public_path('images/ims.png') }}" style="width: 15%" align="right">
    </header>

    <footer>
        <font size="2"><strong>PT INKA MULTI SOLUSI</strong></font>
        <p class="text-justify"><font size="1">Kantor Pusat :Jl. Raya Surabaya-Madiun KM 161,No.01, Madiun. No Telp.(0351) 2812105/2812256 Website: www.inkamultisolusi.co.id, E-mail:sekretariat@inkamultisolusi.co.id</font><br>
        <img src="{{ public_path('images/header.jpg') }}" width="100%"></p>
    </footer>

    <main class="mt-4">
        <h3 align="center" style="font-family: 'Arial Black', 'Arial Bold', Gadget, sans-serif !important;"><b>MEMO</b></h3>
        <table align="left" border="0">
            <tr>
                <td width="60"><font size="2">Tanggal</font></td>
                <td width="350"><font size="2">: {{ $data->tgl_indo }}</font></td>
            </tr>
            <tr>
                <td width="60"><font size="2">Nomor</font></td>
                <td width="350"><font size="2">: {{ $data->nomor_surat }} </font></td>
            </tr>
            <tr>
                <td width="60"><font size="2">Lampiran</font></td>
                <td width="350"><font size="2">: {{ $data->ket_lampiran }}</font></td>
            </tr>
            <tr>
                <td width="60"><font size="2">Perihal</font></td>
                <td width="350"><font size="2">: <u><strong>{{ $data->perihal }}</strong></u></font></td>
            </tr>
        </table>
        <br>
        <table align="left" border="1" style="width:100%"> 
            <tr style="vertical-align: top;">
                <td  style="width:100%">
                    <table style="padding: 2px;" border="0">
                        <tr>
                            <td>
                                <font size="2">Dari :</font><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <font size="2"><strong>{{ ucwords(strtolower($data->pengirim->jabatan)) }}</strong></font>
                            </td>
                        </tr>
    
                    </table>
                </td>
                <td  style="width:100%">
                    <table style="padding: 2px;" border="0">
                        <tr>
                            <td>
                                <font size="2">Kepada Yth :</font><br>
                            </td>
                        </tr>
                        @foreach ($data->penerima as $penerima)
                        <tr>
                            <td>
                                <font size="2"><strong>{{ ucwords(strtolower($penerima->jabatan)) }}</strong></font>
                            </td>
                        </tr>
                        @endforeach
                    </table>

                </td>
            </tr>
        </table>
        <br>
       
        <div class="konten">
            {!! $data->konten !!}
        </div>
        <br>
        <table border="0" align="right" style="page-break-after: auto!important;page-break-inside:avoid;">
            <tr>
                <td><center><font size="2"><strong>Hormat Kami,</center></strong>
                <p class="text-center"><strong><font size="2">{{ ucwords(strtolower($data->pengirim->jabatan)) }}</font></strong></td></p>
            </tr>
            <tr>
                <td><center>
                    @if($data->is_approve == 1)
                    <img src="data:image/png;base64, {!! $qrcode !!}" style="height: 70px;margin-bottom:10px;">
                    @else
                        <br>
                        <br>
                    @endif
                    
                    </center>
                </td>
            </tr>
            <tr> 
                <td><center><font size="2" class="text-justify"><strong>{{ ucwords(strtolower($data->pengirim->nama)) }}</center></strong>
                <p class="text-center"><strong><font size="2">NIP {{ ucwords(strtolower($data->pengirim->nip)) }}</font></strong></td></p><br>
            </tr>
        </table>
        <br>
    </main>

</body>

</html>