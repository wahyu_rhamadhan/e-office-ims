<table>
    <thead>
    <tr>
        <th>No</th>
        <th>Jenis Sppd</th>
        <th>Tanggal Mulai</th>
        <th>Tanggal Selesai</th>
        <th>Durasi</th>
        <th>Proyek</th>
        <th>Destinasi</th>
        <th>Deskripsi</th>
        <th>Transportasi</th>
        <th>Lumsump</th>
        <th>Biaya Transportasi</th>
        <th>Biaya Hotel</th>
        <th>Pengajuan</th>
        <th>Status Izin</th>
    </tr>
    </thead>
    <tbody>
        @foreach($data as $val)
        <tr>
            <td>{{ $val['id'] }}</td>
            <td>{{ $val['jenis_sppd'] }}</td>
            <td>{{ $val['tgl_mulai'] }}</td>
            <td>{{ $val['tgl_selesai'] }}</td>
            <td>{{ $val['durasi'] }}</td>
            <td>{{ $val['proyek'] }}</td>
            <td>{{ $val['destinasi'] }}</td>
            <td>{{ $val['deskripsi'] }}</td>
            <td>{{ $val['transportasi'] }}</td>
            <td>{{ $val['lumsump'] }}</td>
            <td>{{ $val['biaya_transportasi'] }}</td>
            <td>{{ $val['biaya_hotel'] }}</td>
            <td>{{ $val['pengajuan'] }}</td>
            <td>{{ $val['status'] }}</td>
            
        </tr>
    @endforeach
    </tbody>
</table>