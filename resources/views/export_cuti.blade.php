<table>
    <thead>
    <tr>
        <th>No</th>
        <th>Jenis Cuti</th>
        <th>NIP</th>
        <th>Nama</th>
        <th>Unit Kerja</th>
        <th>Tanggal Cuti</th>
        <th>Keperluan</th>
        <th>Alamat Cuti</th>
        <th>No. Hp</th>
        <th>Atasan Langsung</th>
        <th>Atasan 2</th>
        <th>Status Cuti</th>
    </tr>
    </thead>
    <tbody>
        @foreach($data as $val)
        <tr>
            <td>{{ $val['nomor'] }}</td>
            <td>{{ $val['jenis'] }}</td>
            <td>{{ $val['nip'] }}</td>
            <td>{{ $val['nama'] }}</td>
            <td>{{ $val['detailPegawai']['unit']['nama'] }}</td>
            <td>{{ $val['tgl_cuti'] }}</td>
            <td>{{ $val['keperluan'] }}</td>
            <td>{{ $val['alamat_cuti'] }}</td>
            <td>{{ $val['no_hp'] }}</td>
            <td>{{ $val['nama_atasan'] }}</td>
            <td>{{ $val['nama_atasan2'] }}</td>
            <td>{{ $val['status_cuti'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>