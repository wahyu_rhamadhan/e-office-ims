<!DOCTYPE html>
<html>
<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
    table, td, div, h1, p {font-family: Arial, sans-serif;}
  </style>
<body>
    

      {{-- sampe sini --}}
	<div class="container">
		<br><br>
		<a href="/api/sppd/view-sppd/cetak-pdf" class="btn btn-primary" target="_blank">CETAK PDF</a> <br><br>
		<table class='table table-bordered'>
            <thead>
                <tr>
                    <td width="25%" colspan="2" align="center">Gambar Logo IMS</td>
                    <th width="50%" colspan="2"><H1><center><b>SURAT PERINTAH <br> PERJALANAN DINAS</b></center></H1></th>
                    <td width="25%">
                        <p>Form SPPD-1
                        <br><br>
                        Nomor   :
                        <br>
                        Tanggal :
                        </p>
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="5">Dengan ini ditugaskan kepada</td>
                </tr>
            </tbody>
			<thead>
				<tr>
					<th width="5%">No</th>
					<th width="20%">Nama</th>
					<th width="20%">NIP</th>
					<th width="30%">JABATAN</th>
					<th width="25%">UNIT KERJA</th>
				</tr>
			</thead>
			<tbody>
				{{-- @php $i=1 @endphp
				@foreach($sppd as $p)
				<tr>
					<td>{{ $i++ }}</td>
					<td>{{$p->empname}}</td>
					<td>{{$p->empcode}}</td>
					<td>{{$p->pegawai_id}}</td>
					<td>{{$p->unit_tree_id}}</td>
					<td>{{$p->tblposition}}</td>
				</tr>
				@endforeach --}}
			
				<?php
            for ($i=1; $i <= 10; $i++) {
            ?> 
            <tr>
                <td>{{ $i }}</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>5</td>
            </tr>
                <?php }
                ?>
			</tbody>
            
            <tbody>
                <tr>
                    <td style="border: 0px" colspan="2">
                        Untuk melakukan perjalanan dinas <br>
                        a. Tujuan/Instansi <br>
                        b. Keperluan <br>
                        c. Lama Perjalanan <br>
                        d. Terhitung Mulai Tanggal <br>
                        e. Kendaraan <br>
                    </td>
                    
                    <td style="border: 0px">
                        <br>
                        : <br>
                        : <br>
                        : <br>
                        : <br>
                        : Umum/Dinas
                    </td>
                    
                    <td style="border: 0px">
                        <br>
                        <br>
                        <br>
                        <br>
                        s/d <br>
                        <br>
                    </td>
                </tr>
            </tbody>
            <tbody>
                <tr>
                    <td style="border: 0px" width="5%">
                        <br>
                    </td>
                    <td style="border: 0px" colspan="2" width="40%">
                        <br>
                    </td>
                    <td style="border: 0px" colspan="2">
                        Dikeluarkan di &nbsp;&nbsp;&nbsp;&nbsp;:
                        <br>
                        Pada Tanggal &nbsp;&nbsp;&nbsp;&nbsp; :
                    </td>
                </tr>
                <tr>
                    <td style="border: 0px" width="5%">
                        <br>
                    </td>
                    <td style="border: 0px" colspan="2" width="40%">
                        Pejalan Dinas
                    </td>
                    <td style="border: 0px" colspan="2">
                        Pejabat Yang Menugaskan
                    </td>
                </tr>
                <tr>
                    <td style="border: 0px" width="5%">
                        <br>
                    </td>
                    <td style="border: 0px" colspan="2" width="40%">
                        <br>
                    </td>
                    <td style="border: 0px" colspan="2">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="border: 0px" width="5%">
                        <br>
                    </td>
                    <td style="border: 0px" colspan="2" width="40%">
                        <br>
                    </td>
                    <td style="border: 0px" colspan="2">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="border: 0px" width="5%">
                        <br>
                    </td>
                    <td style="border: 0px" colspan="2" width="40%">
                        Nama &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                        <br>
                        NIP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :
                    </td>
                    <td style="border: 0px" colspan="2">
                        Nama &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                        <br>
                        NIP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :
                    </td>
                </tr>
            </tbody>
		</table>
        <p><b>Catatan:</b> <br>
        Lembar 1 disampaikan Ke Divisi Keuangan <br>
        Lembar 2 disampaikan Ke Divisi SDM & Umum<br>
        Lembar 3 sebagai arsip Pejalan Dinas <br>
        </p>
 
	</div>
 
</body>
</html>