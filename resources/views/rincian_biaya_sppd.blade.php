<!DOCTYPE html>
<html>
<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    

<table>
    <tbody>
        <tr style="vertical-align: top !important;">
            <td style="border:none;padding-top: 10px;" width="25%" colspan="2" rowspan="4" align="left" ><img src="{{ $logo }}" style="height: 30px;"></td>
            <td style="border:none;" width="50%" colspan="4"></td>
            <td style="border:none;padding-top: 10px;text-align: center" width="25%" colspan="2" rowspan="4"><b> Form SPPD-2</b>
            </td>
        </tr>
        <tr><th style="height: 20px !important;border:none;" colspan="4"></th></tr>
        <tr>
            <th class="text-center"colspan="4" style="border:none;font-size: 12px"><b><u>RINCIAN BIAYA PERJALANAN DINAS</u></b></th>
        </tr>
        <tr>
            <td colspan="2" style="text-align: end;border:none;padding-bottom: 10px;">Nomor&nbsp;&nbsp;: {{ $sppd->no_doc }}&nbsp;&nbsp;</td>
            <td colspan="2" style="text-align: center;border:none;padding-bottom: 10px;">&nbsp;&nbsp;Tanggal&nbsp;&nbsp;: {{ $sppd->_tgl_created }}</td>
        </tr>
        <tr>
            <td colspan="8"><b>A. BIAYA <i>LUMPSUM</i></b></td>
        </tr>
    </tbody>
  
    <tbody style="line-height: 15px !important;">
        <tr class="text-center">
            <th width="5%" rowspan="2" style="vertical-align: middle;">No</th>
            <th width="15%" rowspan="2" style="vertical-align: middle;">Nama</th>
            <th width="12%" rowspan="2" style="vertical-align: middle;">NIP</th>
            <th width="13%" rowspan="2" style="vertical-align: middle;">Jabatan</th>
            <th width="40%" colspan="3">Uang <i>Lumpsum</i></th>
            <th width="15%" rowspan="2" style="vertical-align: middle;">TTD</th>
        </tr>
        <tr class="text-center">
            <th width="10%">Hari</th>
            <th width="10%">Tarif</th>
            <th width="10%">Jumlah</th>
        </tr>
    </tbody>
    <tbody>
            <?php
                $sppd_x_durasi = $sppd->durasi * $sppd->lumsump;
            ?>
        <tr>
                <td>1.</td>
                <td style="word-wrap: break-word !important;">{{ $sppd->pengaju_nama }}</td>
                <td>{{ $sppd->pengaju_nip  }}</td>
                <td style="word-wrap: break-word !important;">{{ $sppd->pengaju_jabatan  }}</td>
                <td style="word-wrap: break-word !important;">{{ $sppd->durasi }}</td>
                <td>{{ $sppd->lumsump }}</td>
                <td>{{ $sppd->durasi * $sppd->lumsump }}</td>
                <td><i>approved by system</i></td>
            </tr>
            @foreach($sppd->partisipan as $key => $value)
                <?php
                    $sppd_x_durasi+= $sppd->durasi * $value->lumpsump;
                ?>
                 <tr>
                    <td>1.</td>
                    <td style="word-wrap: break-word !important;">{{ $value->pegawai_nama }}</td>
                    <td>{{ $value->pegawai_nip  }}</td>
                    <td style="word-wrap: break-word !important;">{{ $value->pegawai_jabatan  }}</td>
                    <td style="word-wrap: break-word !important;">{{ $sppd->durasi }}</td>
                    <td>{{ $value->lumpsump }}</td>
                    <td>{{ $sppd->durasi * $value->lumpsump }}</td>
                    <td><i>approved by system</i></td>
                </tr>

             @endforeach
            <tr>
                <td colspan="6" class="text-center"><b>JUMLAH A</b></td>
                <td>{{ $sppd_x_durasi }}</td>
                <td></td>
            </tr>
    </tbody>
    <tbody class="table table-bordered" style="outline:0.01em solid rgb(218, 206, 206);">
        <tr>
            <td colspan="8"><b>B. BIAYA LAIN-LAIN</b></td>
        </tr>
    </tbody>
    <tbody>
        <tr class="text-center">
            <th width="5%">No</th>
            <th width="30%" colspan="3">URAIAN</th>
            <th width="10%">Hari</th>
            <th width="10%">Tarif</th>
            <th width="10%">Jumlah</th>
            <th width="15%">TTD</th>
        </tr>
    </tbody>
    <tbody>
            <tr>
                    <td>1.</td>
                    <td colspan="3">Transportasi</td>
                    <td>{{ $sppd->durasi }}</td>
                    <td></td>
                    <td>{{ $sppd->total_biaya_transportasi }}</td>
                    <td><i>approved by system</i></td>
            </tr>
            <tr>
                    <td>2.</td>
                    <td colspan="3">Hotel</td>
                    <td>{{ $sppd->durasi }}</td>
                    <td></td>
                    <td>{{ $sppd->total_biaya_hotel }}</td>
                    <td><i>approved by system</i></td>
                </tr>
            <tr>
                <td colspan="6" class="text-center"><b>JUMLAH B</b></td>
                <td>{{ $sppd->total_biaya_hotel + $sppd->total_biaya_transportasi }}</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="6" class="text-center"><b>JUMLAH TOTAL (A+B)</b></td>
                <td>{{ $sppd->total_biaya_hotel + $sppd->total_biaya_transportasi + $sppd_x_durasi }}<</td>
                <td></td>
            </tr>
    </tbody>
    <tbody style="height: 300px;" class="text-center">
        <tr>
            <td colspan="2" style="border:none;padding-top:20px;">Pejabat Yang <br> Menugaskan</td>
            <td colspan="2" style="border:none;padding-top:20px;">Direktur Keuangan, SDM, <br> dan Manajemen Risiko</td>
            <td colspan="2" style="border:none;padding-top:20px;">Kepala Divisi <br> Keuangan</td>
            <td colspan="2" style="border:none;padding-top:20px;">Pejalan <br> Dinas</td>
        </tr>
        <tr>
            <td colspan="2" style="border:none;"><img style="" src="" alt=""></td>
            <td colspan="2" style="border:none;"><img src="" alt=""></td>
            <td colspan="2" style="border:none;"><img src="" alt=""></td>
            <td colspan="2" style="border:none;"><img src="" alt=""></td>
        </tr>
        <tr>
            <td style="border:none;" colspan="2">{{ $sppd->pejabat_penugasan_nama }}</td>
            <td style="border:none;" colspan="2"></td>
            <td style="border:none;" colspan="2"></td>
            <td style="border:none;" colspan="2">{{ $sppd->pengaju_nama }}</td>
        </tr>
    </tbody>
    
    
</table>
<p style="font-size: 8px !important;"><b>Catatan:</b> <br><small>
Lembar 1 untuk pertanggungjawaban biaya (sebagai lampiran daftar pembayaran/bukti kas keluar) <br>
Lembar 2 untuk arsip Divisi Keuangan<br>
</p></small>
{{-- <div class="container">

</div> --}}

 
</body>
</html>