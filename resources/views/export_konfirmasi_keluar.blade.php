<table>
    <thead>
    <tr>
        <th>NIP</th>
        <th>Nama</th>
        <th>Jabatan</th>
        <th>Unit Kerja</th>
        <th>Jenis Izin</th>
        <th>Keperluan</th>
        <th>Konfirmasi Keluar</th>
        <th>Konfirmasi Kembali</th>
    </tr>
    </thead>
    <tbody>
        @foreach($data as $val)
        <tr>
            <td>{{ $val['detailPegawai']['empcode'] }}</td>
            <td>{{ $val['detailPegawai']['empname'] }}</td>
            <td>{{ $val['detailPegawai']['posname'] }}</td>
            <td>{{ $val['detailPegawai']['unit']['nama'] }}</td>
            <td>{{ $val['jenis_izin'] }}</td>
            <td>{{ $val['keperluan'] }}</td>
            <td>{{ $val['confirm_out'] }}</td>
            <td>{{ $val['confirm_in'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>