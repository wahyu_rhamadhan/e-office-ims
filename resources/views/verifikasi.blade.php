<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Verifikasi Registrasi</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;background-color: #CFD8DC">
 <table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
   <td>
   <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
	 <tr>
	  <td>
	  	 <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
			 <tr>
			  <td align="center" bgcolor="#CFD8DC" style="padding: 40px 0 30px 0;">
				 <img src="https://rekrutmen.inkamultisolusi.co.id/images/logo.gif" alt="ims" width="100" height="50" style="display: block;" />
				</td>
			 </tr>
			 <tr>
			  	<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
				 <table border="0" cellpadding="0" cellspacing="0" width="100%">
					 <tr>
					  <td>
					   Dear {{ $name }},
					  </td>
					 </tr>
					 <tr>
					  <td style="padding: 20px 0 30px 0;">
					   Mohon untuk dapat melakukan verifikasi atas registrasi yang dilakukan dengan memasukkan kode berikut pada halaman konfirmasi:
					  </td>
					 </tr>
					 <tr style="text-align: center;">
					  <td>
					   <h2>{{ $otp }}</h2>
					  </td>
					 </tr>
					 <tr>
					 	<td>
					 		Abaikan email ini apabila Anda tidak merasa melakukan pendaftaran akun pada sistem kami.
					 	</td>
					 </tr>
					  <tr>
					 	<td>
					 		<br>
					 		<br>
					 		 {!! html_entity_decode($footer) !!}
					 	</td>
					 </tr>
					</table>
				</td>
			 </tr>
			</table>
	  </td>
	 </tr>
	 <tr></tr>
	</table>
   </td>
  </tr>
 </table>
</body>
</html>