<!DOCTYPE html>
<html>
<head>
	<title>Surat Perintah Perjalanan Dinas</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    
	<style type="text/css">
        body {font-family: Arial, Helvetica, sans-serif;}
        table {border-collapse: collapse; width: 100%; }
        table {border: 1.5px solid black; margin: 1px;}
		table tr td,
		table tr th{
			font-size: 10px;
            border: 1.5px solid black; 
		}
        td{
            word-wrap: break-word;
            padding-left: 10px;
        }
         .border { border: 1.5px solid black;  }
         .page_break { page-break-before: always; }
	</style>
 
	<table>
        <tbody>
            <tr>
                <td width="25%" colspan="2" align="center"><img src="{{ $logo }}" style="height: 50px;"></td>
                <th width="50%" colspan="2" style="text-align: center!important"><span style="font-size: 20px!important;text-align: center!important"><b>SURAT PERINTAH <br> PERJALANAN DINAS</b></span></th>
                <td width="25%">
                    <p>Form SPPD-1
                    <br><br>
                    Nomor   : {{ $sppd->no_doc }}
                    <br>
                    Tanggal : {{ $sppd->_tgl_created }}
                    </p>
                </td>
            </tr>
        </tbody>
        <tbody  style="line-height: 15px !important;">
            <tr>
                <td colspan="5" style="padding-top: 10px;">Dengan ini ditugaskan kepada</td>
            </tr>
        </tbody>
        <tbody style="line-height: 15px !important;">
            <tr>
                <th width="5%" style="text-align: center;">No</th>
                <th width="20%" style="text-align: center;">Nama</th>
                <th width="20%" style="text-align: center;">NIP</th>
                <th width="30%" style="text-align: center;">JABATAN</th>
                <th width="25%" style="text-align: center;">UNIT KERJA</th>
            </tr>
        </tbody>
        <tbody style="line-height: 15px !important;">
            <tr>
                <td>1.</td>
                <td style="word-wrap: break-word !important;">{{ $sppd->pengaju_nama }}</td>
                <td>{{ $sppd->pengaju_nip  }}</td>
                <td style="word-wrap: break-word !important;">{{ $sppd->pengaju_jabatan  }}</td>
                <td style="word-wrap: break-word !important;">{{ $sppd->pengaju_unit }}</td>
            </tr>
             @foreach($sppd->partisipan as $key => $value)
                 <tr>
                    <td>{{ $key + 2}}.</td>
                    <td style="word-wrap: break-word !important;">{{ $value->pegawai_nama }}</td>
                    <td>{{ $value->pegawai_nip  }}</td>
                    <td style="word-wrap: break-word !important;">{{ $value->pegawai_jabatan  }}</td>
                    <td style="word-wrap: break-word !important;">{{ $value->pegawai_unit }}</td>
                </tr>

             @endforeach
             <tr>
                <td colspan="5" style="border:none;padding-top: 20px;">Untuk melakukan perjalan dinas</td>
            </tr>
        </tbody>
        
        <tbody>
            <tr>
                <td colspan="5" style="border:none;">
                    <table style="border:none !important;width: 100%" >
                        <tr>
                            <td style="border: 0px;padding-left: 0px !important;" width="24%">a. Tujuan/Instansi</td>
                            <td style="border: 0px;" width="1%">:</td>
                            <td style="border: 0px;" width="73%">{{ $sppd->deskripsi }}</td>
                        </tr>
                        <tr>
                            <td style="border: 0px;padding-left: 0px !important;" width="24%">b. Keperluan</td>
                            <td style="border: 0px;" width="1%">:</td>
                            <td style="border: 0px;" width="73%">{{ $sppd->proyek }}</td>
                        </tr>
                        <tr>
                            <td style="border: 0px;padding-left: 0px !important;" width="24%">c. Lama Perjalanan</td>
                            <td style="border: 0px;" width="1%">:</td>
                            <td style="border: 0px;" width="73%">{{ $sppd->durasi }} Hari</td>
                        </tr>
                        <tr>
                            <td style="border: 0px;padding-left: 0px !important;" width="24%">c. Terhitung Mulai Tanggal</td>
                            <td style="border: 0px;" width="1%">:</td>
                            <td style="border: 0px;" width="73%">{{ $sppd->_tgl_mulai }} s/d {{ $sppd->_tgl_selesai }}</td>
                        </tr>
                        <tr>
                            <td style="border: 0px;padding-left: 0px !important;" width="24%">c. Kendaraan</td>
                            <td style="border: 0px;" width="1%">:</td>
                            <td style="border: 0px;" width="73%">{{ $sppd->transportasi == 1 ? 'Umum' : 'Dinas' }}</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="border:none;padding-top: 20px;padding-bottom: 10px;">Demikian surat perjalanan dinas ini diterbitkan untuk dipergunakan sebagaimana mestinya.</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr>
                <td colspan="5">
                    <table style="border:none;">
                         <tr>
                            <td style="border: 0px" width="5%">
                                <br>
                            </td>
                            <td style="border: 0px" colspan="2" width="40%">
                                <br>
                            </td>
                            <td style="border: 0px" colspan="2">
                                Dikeluarkan di &nbsp;&nbsp;&nbsp;&nbsp;: <i>System</i>
                                <br>
                                Pada Tanggal &nbsp;&nbsp;&nbsp;&nbsp; : {{ $sppd->_tgl_created }}
                            </td>
                        </tr>
                        <tr style="padding-top: 10px;">
                            <td style="border: 0px" width="5%">
                                <br>
                            </td>
                            <td style="border: 0px" colspan="2" width="40%">
                                Pejalan Dinas
                            </td>
                            <td style="border: 0px" colspan="2">
                                Pejabat Yang Menugaskan
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 0px" width="5%">
                                <br>
                            </td>
                            <td style="border: 0px" colspan="2" width="40%">
                                <br>
                            </td>
                            <td style="border: 0px" colspan="2">
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 0px" width="5%">
                                <br>
                            </td>
                            <td style="border: 0px" colspan="2" width="40%">
                                <br>
                            </td>
                            <td style="border: 0px" colspan="2">
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 0px" width="5%">
                                <br>
                            </td>
                            <td style="border: 0px" colspan="2" width="40%">
                                Nama &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ $sppd->pengaju_nama }}
                                <br>
                                NIP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{ $sppd->pengaju_nip }}
                            </td>
                            <td style="border: 0px" colspan="2">
                                Nama &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{ $sppd->pejabat_penugasan_nama }}
                                <br>
                                NIP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{ $sppd->pejabat_penugasan_nip }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
           
        </tbody>
    </table>
    <p style="font-size: 8px !important;"><b>Catatan:</b> <br><small>
    Lembar 1 disampaikan Ke Divisi Keuangan <br>
    Lembar 2 disampaikan Ke Divisi SDM & Umum<br>
    Lembar 3 sebagai arsip Pejalan Dinas <br>
    </p></small>
    

    <div class="page_break">
        @include('rincian_biaya_sppd',['sppd'=>$sppd,'logo'=>$logo])
    </div>
</body>
</html>