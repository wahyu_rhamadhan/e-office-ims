<table>
    <thead>
    <tr>
        <th>No</th>
        <th>Jenis Izin</th>
        <th>NIP</th>
        <th>Nama</th>
        <th>Unit Kerja</th>
        <th>Tanggal Pengajuan</th>
        <th>Tujuan</th>
        <th>Keperluan</th>
        <th>Dari Jam</th>
        <th>Sampai Jam</th>
        <th>Konfirmasi Keluar</th>
        <th>Konfirmasi Kembali</th>
        <th>Status Izin</th>
    </tr>
    </thead>
    <tbody>
        @foreach($data as $val)
        <tr>
            <td>{{ $val['nomor'] }}</td>
            <td>{{ $val['jenis_izin'] }}</td>
            <td>{{ $val['empcode'] }}</td>
            <td>{{ $val['empname'] }}</td>
            <td>{{ $val['detailPegawai']['unit']['nama'] }}</td>
            <td>{{ $val['tgl'] }}</td>
            <td>{{ $val['tujuan'] }}</td>
            <td>{{ $val['keperluan'] }}</td>
            <td>{{ $val['jam_start'] }}</td>
            <td>{{ $val['jam_end'] }}</td>
            <td>{{ $val['confirm_in'] }}</td>
            <td>{{ $val['confirm_out'] }}</td>
            <td>{{ $val['status_izin'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>