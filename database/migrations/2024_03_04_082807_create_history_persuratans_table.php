<?php

use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryPersuratansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('history_persuratans', function (Blueprint $table) {
            $table->uuid('id')->default(Uuid::uuid4());
            $table->string('letter_id');
            $table->string('from');
            $table->string('departemen_from');
            $table->string('to');
            $table->string('departemen_to');
            $table->string('reviewer');
            $table->string('comment');
            $table->string('keterangan');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::conection('pgsql_second')->dropIfExists('history_persuratans');
    }
}
