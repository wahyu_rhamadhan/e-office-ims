<?php 
	namespace App\GlobalClass;

	class Response {


	private $code;
    private $message;
    private $data;
    private $errors;
    private $status;

	function  __construct($code, $message, $data, $errors, $status){
		$this->code = $code;
        $this->message = $message;
        $this->data = $data;
        $this->errors = $errors;
        $this->status = $status;
	} 

	public function getResponse()
    {
        $response = [
            'code' => $this->code,
            'message' => $this->message,
            'data' => $this->data,
            'errors' => $this->errors,
            'status'=>$this->status
        ];

         return response()->json( $response, $response[ 'code' ] );
    }
}

 ?>