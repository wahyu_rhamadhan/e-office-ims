<?php 

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class IzinExport implements FromView, ShouldAutoSize
{
	protected $data;
	public function __construct($data){
	   $this->data = $data;
	}

    public function view(): View
    {

        return view('export_izin', [
        	'data' => $this->data
        ]);
    }
}

 ?>