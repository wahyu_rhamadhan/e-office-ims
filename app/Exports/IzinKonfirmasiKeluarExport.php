<?php 

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class IzinKonfirmasiKeluarExport implements FromView, ShouldAutoSize
{
	protected $data;
	public function __construct($data){
	   $this->data = $data;
	}

    public function view(): View
    {

        return view('export_konfirmasi_keluar', [
        	'data' => $this->data
        ]);
    }
}

 ?>