<?php 
use App\Services\FtpService;
use App\Models\Unit;

function isWeekend($date) {
    return (date('N', strtotime($date)) >= 6);
}

function unit($id){
        $unit = Unit::where('tree_id',$id)->first();
        return $unit;
    }

function uploadFile($direktori, $file, $namafile = null){
    if(!empty($file)){
        /**
            create folder images slider
        */
        if (!file_exists($direktori)) {
            mkdir($direktori, 0777, true);
        }
            $folder = $direktori;
            $ekstensi = $file->getClientOriginalExtension();
            if ($ekstensi !== 'docx' && $ekstensi !== 'xlsx' && $ekstensi !== 'pdf' && $ekstensi !== 'png' && $ekstensi !== 'jpg' && $ekstensi !== 'jpeg' && $ekstensi !== 'PNG' && $ekstensi !== 'JPG' && $ekstensi !== 'JPEG' && $ekstensi !== 'PDF' && $ekstensi !== 'DOCX' && $ekstensi !== 'XLSX') {
            
                $response = array('status'=>false, 'message'=>'File yang anda masukkan tidak valid');
                return $response;
            }

            
            $namaFileAsli = $file->getClientOriginalName();
            $nama  = pathinfo(str_replace(' ', '_', $namaFileAsli), PATHINFO_FILENAME);
            $file_generated = date('YmdHis') . '_' . $nama;
            $file_generated = str_replace(' ', '_', $file_generated);
            $file_generated = $file_generated . "." . $ekstensi;

            $file_generated = $namafile ? $namafile : $file_generated;
            $file->move($folder, $file_generated);

            $response = array('status'=>true, 'file_asli'=> $namaFileAsli, 'file_generated'=>$file_generated);
            return $response;

        }

    }


function uploadFileFtp($file, $direktori = null, $key = 'file'){
         if(!empty($file)){
                $ekstensi = $file->getClientOriginalExtension();
                if ($ekstensi !== 'pdf' && $ekstensi !== 'png' && $ekstensi !== 'jpg' && $ekstensi !== 'jpeg' && $ekstensi !== 'PNG' && $ekstensi !== 'JPG' && $ekstensi !== 'JPEG' && $ekstensi !== 'PDF' && $ekstensi !== 'DOCX' && $ekstensi !== 'docx') {
                
                    $response = array('status'=>false, 'message'=>'File yang anda masukkan tidak valid');
                    return $response;
                }

                
                $direktori = is_null($direktori) ? config('app.FTP_FOLDER') : $direktori;
                $namaFileAsli = $file->getClientOriginalName();
                $nama  = pathinfo(str_replace(' ', '_', $namaFileAsli), PATHINFO_FILENAME);
                $file_generated = md5(date('YmdHis'));
                $file_generated = str_replace(' ', '_', $file_generated);
                $file_generated = $file_generated . "." . $ekstensi;
                // $file->move($folder, $file_generated);

                $ftp = new FtpService();
                $ftp->upload($_FILES[$key]['tmp_name'],$direktori.$file_generated);

                $response = array('status'=>true, 'file_asli'=> $namaFileAsli, 'file_generated'=>$file_generated);
                return $response;

            }
        
        }

function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    function tgl_indo($tanggal){
    $bulan = array (
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);
    
    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun
 
    return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

 ?>