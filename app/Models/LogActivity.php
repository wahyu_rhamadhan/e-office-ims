<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogActivity extends Model
{
    use HasFactory;


    //intial connection database
    protected $connection = 'pgsql_second';
    protected $table = 'log_activity';

    protected $fillable = [
        'id',
        'modul',
        'keterangan',
        'data_sebelum',
        'data_sesudah',
        'status',
        'ip_address',
        'user_nip',
        'user_deptcode',
        'user_deptname',
        'user_login',
        'user_nama',
        'fitur',
    ];
}
