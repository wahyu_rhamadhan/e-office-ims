<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkflowStagePrerequisite extends Model {

    use SoftDeletes;
    protected $table = 'workflow_stage_prerequisites';
    protected $dates = [ 'deleted_at' ];
    protected $keyType = 'string';
    protected $guarded = [];

}