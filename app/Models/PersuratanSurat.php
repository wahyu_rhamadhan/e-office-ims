<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersuratanSurat extends Model {
    use SoftDeletes;
    protected $table = 'persuratan_surat';
    protected $dates = [ 'deleted_at' ];
    protected $guarded = [];
    protected $keyType = 'string';

     public function mengetahui()
    {
        return $this->hasMany('App\Models\PersuratanSuratDtl','persuratan_surat_id','id')
        ->selectRaw("persuratan_surat_dtl.*, concat(nama, ' (',nip,')') as full_name ")
        ->where('flag',4);
    }

    public function penerima()
    {
        return $this->hasMany('App\Models\PersuratanSuratDtl','persuratan_surat_id','id')
        ->selectRaw("persuratan_surat_dtl.*, concat(nama, ' (',nip,')') as full_name ")
        ->where('flag',3);
    }

    public function pengirim()
    {
        return $this->hasOne('App\Models\PersuratanSuratDtl','persuratan_surat_id','id')
        ->selectRaw("persuratan_surat_dtl.*, concat(nama, ' (',nip,')') as full_name, DATE_FORMAT(created_at,'%d/%m/%Y') tgl_surat ")
        ->where('flag',2);
    }

    public function drafter()
    {
        return $this->hasOne('App\Models\PersuratanSuratDtl','persuratan_surat_id','id')
        ->selectRaw("persuratan_surat_dtl.*, concat(nama, ' (',nip,')') as full_name ")
        ->where('flag',1);
    }

     public function disposisi()
    {
        return $this->hasOne('App\Models\PersuratanDisposisi','persuratan_surat_id','id')
        ->selectRaw('persuratan_surat_id, id, catatan');
    }

     public function unit()
    {
        return $this->hasOne('App\Models\Unit','tree_id','unit_id')
        ->selectRaw("nama, coalesce(kelasjabatan,'-') kelasjabatan, tree_id");
    }
}
