<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaldoCuti extends Model {
    use SoftDeletes;
    protected $table = 'saldo_cuti';
    protected $dates = [ 'deleted_at' ];
    protected $guarded = [];
    protected $keyType = 'string';

}
