<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RiwPkwt extends Model {

    use SoftDeletes;
    protected $table = 'riw_pkwt';
    protected $dates = [ 'deleted_at' ];
    protected $keyType = 'string';
    protected $guarded = [];

}
