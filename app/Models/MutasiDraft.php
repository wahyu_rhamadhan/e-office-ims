<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\MutasiList;
use App\Models\RiwMutasi;

class MutasiDraft extends Model
{
    use SoftDeletes;
    protected $table = 'mutasi_draft';
    protected $guarded = [];
    protected $keyType = 'string';
    protected $dates = ['deleted_at'];

    public function mutasiLists()
    {
        return $this->hasMany(MutasiList::class, 'mutasi_draft_id', 'id');
    }
}
