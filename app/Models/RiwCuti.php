<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RiwCuti extends Model {
    use SoftDeletes;
    protected $table = 'riw_cuti';
    protected $dates = [ 'deleted_at' ];
    protected $guarded = [];
    protected $keyType = 'string';


    public function detailPegawai(){
        return $this->hasOne('App\Models\TblEmployee','pegawai_id','pegawai_id')
         ->leftJoin('tblposition', 'tblposition.poscode', 'TblEmployee.poscode')
        ->selectRaw('empname,empcode,pegawai_id,unit_tree_id, tblposition.posname');
    }
}
