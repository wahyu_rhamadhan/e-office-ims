<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersuratanSuratDtl extends Model {
    use SoftDeletes;
    protected $table = 'persuratan_surat_dtl';
    protected $dates = [ 'deleted_at' ];
    protected $guarded = [];
    protected $keyType = 'string';

    public function surat()
    {
        return $this->hasOne('App\Models\PersuratanSurat','id','persuratan_surat_id');
    }
}
