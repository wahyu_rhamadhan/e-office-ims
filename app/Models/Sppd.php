<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sppd extends Model
{
    use SoftDeletes;
    protected $table = 'sppd';
    protected $dates = ['deleted_at'];
    protected $guarded = [];
    protected $keyType = 'string';
    protected $hidden = ['created_at', 'updated_at','deleted_at'];

  public function partisipan()
    {
        return $this->hasMany('App\Models\SppdParticipant', 'sppd_id', 'id');
    }

}
