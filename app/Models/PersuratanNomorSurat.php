<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersuratanNomorSurat extends Model {
    use SoftDeletes;
    protected $table = 'persuratan_nomor_surat';
    protected $dates = [ 'deleted_at' ];
    protected $guarded = [];
    protected $keyType = 'string';
}
