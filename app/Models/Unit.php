<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'unit';
    protected $connection = 'pgsql';
    protected $keyType = 'string';

     public function pegawai()
    {
        return $this->hasOne('App\Models\TblEmployee','EmpCode','nip');
    }
}
