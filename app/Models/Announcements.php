<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Announcements extends Model
{
    use SoftDeletes;
    protected $table = 'announcements';
    protected $dates = ['deleted_at'];
    protected $guarded = [];
    protected $keyType = 'string';
}
