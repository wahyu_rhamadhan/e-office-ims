<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersuratanDisposisi extends Model {
    use SoftDeletes;
    protected $table = 'persuratan_disposisi';
    protected $dates = [ 'deleted_at' ];
    protected $guarded = [];
    protected $keyType = 'string';


    public function surat()
    {
        return $this->hasOne('App\Models\PersuratanSurat','id','persuratan_surat_id')
        ->selectRaw("persuratan_surat.*, DATE_FORMAT(persuratan_surat.created_at,'%d/%m/%Y') tgl_surat");
    }

    public function detail(){
    	 return $this->hasMany('App\Models\PersuratanDisposisiDtl','persuratan_disposisi_id','id')
    	 ->selectRaw('persuratan_disposisi_dtl.persuratan_disposisi_id, persuratan_disposisi_dtl.id,persuratan_disposisi_dtl.pegawai_id, persuratan_disposisi_dtl.id, empname as nama, empcode as nip')
    	 ->leftJoin('tblemployee','tblemployee.pegawai_id','persuratan_disposisi_dtl.pegawai_id');
    }

}
