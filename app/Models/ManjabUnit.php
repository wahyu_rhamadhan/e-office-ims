<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ManjabUnit extends Model {
    use SoftDeletes;
    protected $table = 'manjab_unit';
    protected $guarded = [];
    protected $keyType = 'string';
}
