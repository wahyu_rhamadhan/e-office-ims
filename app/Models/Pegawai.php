<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pegawai extends Model
 {
    use SoftDeletes;

    // public $timestamps = false;
    protected $table = 'pegawai';
    protected $connection = 'mysql';
    protected $dates = [ 'deleted_at' ];
    protected $keyType = 'string';
    protected $guarded = [];


     public function unit()
    {
        return $this->hasOne('App\Models\Unit','tree_id','unit_tree_id')
        ->selectRaw("nama, coalesce(kelasjabatan,'-') kelasjabatan, tree_id");
    }

}
