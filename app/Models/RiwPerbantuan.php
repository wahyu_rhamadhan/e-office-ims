<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RiwPerbantuan extends Model {

    use SoftDeletes;
    protected $table = 'riw_perbantuan';
    protected $dates = [ 'deleted_at' ];
    protected $keyType = 'string';
    protected $guarded = [];

}
