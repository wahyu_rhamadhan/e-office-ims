<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RiwCapeg extends Model {

    use SoftDeletes;
    public $timestamps = false;
    protected $table = 'riw_capeg';
    protected $dates = [ 'deleted_at' ];
    protected $connection = 'mysql';
    protected $keyType = 'string';
    protected $guarded = [];
}