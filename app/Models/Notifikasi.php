<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;


class Notifikasi extends Model
{
    use SoftDeletes;
    protected $table = 'notifikasi';
    protected $dates = ['deleted_at'];
    protected $connection = 'mysql';
    protected $guarded = [];
    protected $keyType = 'string';
    protected $appends = array('time');

    protected $fillable = [
        'id',
        'judul',
        'from_pegawai_id',
        'is_read',
        'keterangan',
        'to_pegawai_id',
        'riwayat',
        'riwayat_id',
        'catatan_penolakan'
    ];


    public function riw_izin()
    {
        return $this->hasOne('App\Models\RiwIzin', 'id', 'riwayat_id')->selectRaw("
        		riw_izin.*, case when status_id = 1 then 'Approved' when status_id = 2 then 'Declined' else 'Awaiting' end status_izin,case when jenis_izin_id = 1 then 'Izin Dinas' when jenis_izin_id = 2 then 'Izin Pribadi' end jenis_izin,DATE_FORMAT(created_at,'%d/%m/%Y') tgl, p.empname nama, p.empcode as nip,
        		 concat(atasan.empname,' - ',atasan.empcode) nama_atasan, DATE_FORMAT(confirmed_out_at,'%d/%m/%Y %H:%i') as confirm_out, DATE_FORMAT(confirmed_in_at,'%d/%m/%Y %H:%i') as confirm_in
        	")->leftjoin(DB::raw('tblemployee as atasan'), 'atasan.pegawai_id', 'riw_izin.atasan_id')
            ->leftjoin(DB::raw('tblemployee as p'), 'p.pegawai_id', 'riw_izin.pegawai_id');
    }

    public function riw_cuti()
    {
        return $this->hasOne('App\Models\RiwCuti', 'id', 'riwayat_id')->selectRaw("riw_cuti.*, m_cuti.jenis,ROW_NUMBER() OVER (partition by pegawai_id order by created_at) nomor,
                case when status_cuti_id = 1 then 'Disetujui Atasan 1' when status_cuti_id = 3 then 'Disetujui Atasan 2' when status_cuti_id = 2 then 'Ditolak Atasan 1' when status_cuti_id = 4 then 'Ditolak Atasan 2' else 'Awaiting' end status_cuti, case when tgl_mulai = tgl_selesai then  DATE_FORMAT(tgl_mulai,'%d/%m/%Y') else concat(DATE_FORMAT(tgl_mulai,'%d/%m/%Y'),' s.d. ', DATE_FORMAT(tgl_selesai,'%d/%m/%Y')) end as tgl_cuti,
                    concat(atasan1.empname, ' - ', atasan1.empcode) as nama_atasan, concat(atasan2.empname, ' - ', atasan2.empcode) as nama_atasan2,
                    DATE_FORMAT(tgl_mulai,'%Y-%m-%d') tgl_mulai_,   DATE_FORMAT(tgl_selesai,'%Y-%m-%d') tgl_selesai_, tblemployee.empname as nama, tblemployee.empcode as nip
            ")
            ->leftjoin('tblemployee', 'tblemployee.pegawai_id', '=', 'riw_cuti.pegawai_id')
            ->leftjoin('m_cuti', 'm_cuti.id', 'riw_cuti.jenis_cuti_id')
            ->leftjoin(DB::raw('tblemployee as atasan1'), 'atasan1.pegawai_id', 'riw_cuti.atasan_id')
            ->leftjoin(DB::raw('tblemployee as atasan2'), 'atasan2.pegawai_id', 'riw_cuti.atasan2_id');
    }

    public function getTimeAttribute()
    {
        return time_elapsed_string($this->attributes['created_at']);
    }
}
