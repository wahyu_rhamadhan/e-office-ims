<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RiwIzin extends Model {
    use SoftDeletes;
    protected $table = 'riw_izin';
    protected $dates = [ 'deleted_at' ];
    protected $guarded = [];
    protected $keyType = 'string';

    public function peserta()
    {
        return $this->hasMany('App\Models\RiwIzin','parent_id','id')
        ->leftJoin('pegawai','pegawai.id','riw_izin.pegawai_id')
        ->selectRaw('riw_izin.*, pegawai.nama_lengkap , pegawai.nip');
    }

    public function detailPegawai(){
        return $this->hasOne('App\Models\Pegawai','id','pegawai_id')
        ->selectRaw('id, master_unit_tree_id,nama_jabatan, nama_unit, nama_lengkap, nip, last_manjab_draft_id');
    }
}
