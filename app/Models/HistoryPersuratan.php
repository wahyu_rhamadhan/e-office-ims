<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HistoryPersuratan extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'history_persuratans';

    protected $fillable = [
        'from',
        'departemen_from',
        'to',
        'departemen_to',
        'letter_id',
        'reviewer',
        'comment',
        'keterangan',
    ];
}