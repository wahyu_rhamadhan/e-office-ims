<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkflowStage extends Model {

    use SoftDeletes;
    protected $table = 'workflow_stages';
    protected $dates = [ 'deleted_at' ];
    protected $keyType = 'string';
    protected $guarded = [];

}
