<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersuratanDisposisiDtl extends Model {
    use SoftDeletes;
    protected $table = 'persuratan_disposisi_dtl';
    protected $dates = [ 'deleted_at' ];
    protected $guarded = [];
    protected $keyType = 'string';

    

}
