<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ManjabDraft extends Model {
    use SoftDeletes;
    protected $table = 'manjab_draft';
    protected $guarded = [];
    protected $keyType = 'string';
}
