<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\MutasiDraft;

class MutasiList extends Model
{
    use SoftDeletes;
    protected $table = 'mutasi_list';
    protected $guarded = [];
    protected $keyType = 'string';

    public function mutasiDraft()
    {
        return $this->belongsTo(MutasiDraft::class, 'mutasi_draft_id', 'id');
    }
}
