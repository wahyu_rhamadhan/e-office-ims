<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RiwMutasi extends Model
{
    //use SoftDeletes;
    use HasFactory;
    //public $timestamps = false;
    protected $table = 'riw_mutasis';
    //protected $dates = [ 'deleted_at' ];
   // protected $connection = 'mysql';
    //protected $keyType = 'string';
    //protected $guarded = [];

    protected $fillable = ['pegawai_id', 'unit_name_prev', 'unit_tree_id_prev', 'unit_manjab_draft_id_prev', 'unit_name_aft', 'unit_tree_id_aft', 'unit_manjab_draft_id_aft', 'no_sk', 'tgl_mulai', 'created_at', 'update_at', 'delete_at'];
}
