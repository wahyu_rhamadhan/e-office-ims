<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblEmployee extends Model
 {
    use SoftDeletes;

    protected $primaryKey = 'EmpCode';
    public $timestamps = false;
    protected $table = 'tblemployee';
     protected $connection = 'mysql';
    protected $dates = [ 'deleted_at' ];
    protected $guarded = [];


     public function unit()
    {
        return $this->hasOne('App\Models\Unit','tree_id','unit_tree_id')
        ->selectRaw("nama, coalesce(kelasjabatan,'-') kelasjabatan, tree_id");
    }

}
