<?php

namespace App\Jobs;


use App\Models\Notifikasi;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Ramsey\Uuid\Uuid;

class SendNotificationIms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $params;
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $this->params['id'] = Uuid::uuid4();
        Notifikasi::create($this->params);

        $fields['include_external_user_ids'] = ['3a242032-9705-11ed-a8fc-0242ac120002'];
        $fields['contents'] = array(
            "en" => $this->params['keterangan'],
        );
        $fields['headings'] = array(
            "en" => $this->params['judul']
        );

        \OneSignal::sendPush($fields);
    }
}
