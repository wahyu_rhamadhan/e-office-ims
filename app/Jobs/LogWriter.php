<?php

namespace App\Jobs;

use Ramsey\Uuid\Uuid;
use App\Models\LogActivity;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class LogWriter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $user;
    protected $modul;
    protected $keterangan;
    protected $status;
    protected $dataSebelum;
    protected $dataSesudah;
    protected $fitur;


    public function __construct($user, $modul, $keterangan = null, $status = null, $dataSebelum = null, $dataSesudah = null, $fitur = null)
    {
        $this->user =  $user;
        $this->modul = $modul;
        $this->keterangan = $keterangan;
        $this->status = $status;
        $this->dataSebelum = $dataSebelum;
        $this->dataSesudah = $dataSesudah;
        $this->fitur = $fitur;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = [
            'data_sebelum' => json_encode($this->dataSebelum),
            'data_sesudah' =>  json_encode($this->dataSesudah)
        ];


        // create data log activity
        $log = LogActivity::create([
            'id' => Uuid::uuid4()->toString(),
            'modul' => $this->modul ?? null,
            'keterangan' => $this->keterangan ?? null,
            'data_sebelum' => $data['data_sebelum'] ?? null,
            'data_sesudah' =>  $data['data_sesudah'] ?? null,
            'status' =>  $this->status ?? null,
            'ip_address' =>  $this->user['ip'] ?? null,
            'user_nama' =>  $this->user['name'] ?? null,
            'user_nip' => $this->user['nip'] ?? null,
            'user_deptcode' => $this->user['id_department'] ?? null,
            'user_deptname' => $this->user['job'] ?? null,
            'user_login' =>  $this->user['id'] ?? null,
            'fitur' =>  $this->fitur ?? null
        ]);
    }
}
