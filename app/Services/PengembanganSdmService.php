<?php 

namespace App\Services;


class PengembanganSdmService extends GenericService{

	public function __construct(){
		$this->Endpoint = config('app.SERVICE_PENGEMBANGAN_SDM');
	}

    public function sendWaKelulusan($data = [])
    {
        $urlRequest =  $this->Endpoint.'/pegawai/send-wa-kelulusan';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }


    public function getSosmed($data = []){
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/pegawai/get-sosmed?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

     public function getMasterKompetensi($data = []){
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/master/kompetensi?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }



	 public function getMasterDokumen($data = [])
    {
        $urlRequest =  $this->Endpoint.'/master/dokumen';
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

     public function lamar($data = [])
    {
        $urlRequest =  $this->Endpoint.'/rekrutmen/lamar';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

     public function getLowongan($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/rekrutmen/get-rekrutmen-release?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

     public function getTemplateFooter($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/master/get-master-footer';
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

    public function getAlertProfile($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/rekrutmen/get-alert-peserta?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

    public function getRiwayatSeleksi($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/rekrutmen/get-riwayat-seleksi?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

     public function getPegawaiDokumenRekrutmen($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/pegawai/get-pegawai-dokumen-rekrutmen?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

     public function getDokumenLain($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/rekrutmen/get-dokumen-lainnya?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

     public function cekEmail($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/pegawai/cek-email?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

     public function cariPendaftar($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/pegawai/cari-pendaftar?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

    public function cekNik($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/pegawai/cek-nik?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

    public function getPegawaiPendidikan($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/pegawai/get-pegawai-pendidikan?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

    public function getPesertaKeluarga($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/pegawai/get-peserta-keluarga?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

    public function getPesertaPengalamanKerja($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/pegawai/get-peserta-pengalaman-kerja?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }


     public function getMasterPosisi($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/master/posisi?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

    public function getMasterAgama($data = [])
    {
        $urlRequest =  $this->Endpoint.'/master/agama';
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

    public function getMasterBank($data = [])
    {
        $urlRequest =  $this->Endpoint.'/master/bank';
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

    public function getMasterStatusKawin($data = [])
    {
        $urlRequest =  $this->Endpoint.'/master/status-kawin';
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

     public function getMemoRekrutmen($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/master/memo?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

     public function getFormasi($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/master/formasi?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

     public function getById($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/pegawai/get-by-id?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

    public function getDataPeserta($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/pegawai/get-data-peserta?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

    public function simpanMasterDokumen($data = [])
    {
        $urlRequest =  $this->Endpoint.'/master/simpan-dokumen';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

    public function simpanMasterPosisi($data = [])
    {
        $urlRequest =  $this->Endpoint.'/master/simpan-posisi';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

     public function simpanRekrutmen($data = [])
    {
        $urlRequest =  $this->Endpoint.'/rekrutmen/simpan-rekrutmen';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

    public function simpanSosmed($data = [])
    {
        $urlRequest =  $this->Endpoint.'/pegawai/simpan-sosmed';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

     public function tambahPeserta($data = [])
    {
        $urlRequest =  $this->Endpoint.'/rekrutmen/tambah-peserta';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

     public function simpanRekrutmenTahapan($data = [])
    {
        $urlRequest =  $this->Endpoint.'/rekrutmen/simpan-rekrutmen-tahapan';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

    public function simpanPegawai($data = [])
    {
        $urlRequest =  $this->Endpoint.'/pegawai/simpan-calon';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

    public function simpanPegawaiPendidikan($data = [])
    {
        $urlRequest =  $this->Endpoint.'/pegawai/simpan-pegawai-pendidikan';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

    public function simpanPesertaKeluarga($data = [])
    {
        $urlRequest =  $this->Endpoint.'/pegawai/simpan-peserta-keluarga';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

    public function simpanPesertaPengalamanKerja($data = [])
    {
        $urlRequest =  $this->Endpoint.'/pegawai/simpan-pegawai-pengalaman-kerja';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

    public function hapusPegawaiPendidikan($data = [])
    {
        $urlRequest =  $this->Endpoint.'/pegawai/hapus-pegawai-pendidikan';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

    public function hapusPegawaiPengalamanKerja($data = [])
    {
        $urlRequest =  $this->Endpoint.'/pegawai/hapus-peserta-pengalaman-kerja';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

     public function hapusPesertaKeluarga($data = [])
    {
        $urlRequest =  $this->Endpoint.'/pegawai/hapus-peserta-keluarga';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

     public function updatePegawai($data = [])
    {
        $urlRequest =  $this->Endpoint.'/pegawai/update-pegawai';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

      public function simpanFormasi($data = [])
    {
        $urlRequest =  $this->Endpoint.'/master/simpan-formasi';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

     public function setLulus($data = [])
    {
        $urlRequest =  $this->Endpoint.'/rekrutmen/set-lulus';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

    public function simpanMemo($data = [])
    {
        $urlRequest =  $this->Endpoint.'/rekrutmen/simpan-memo';
        $hasil = $this->curlAdapterPostRequestMultipart($urlRequest, $data);
        return $hasil;
    }

     public function hapusDokumenLain($data = [])
    {
        $urlRequest =  $this->Endpoint.'/rekrutmen/hapus-dokumen-lainnya';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

     public function hapusDokumenPegawai($data = [])
    {
        $urlRequest =  $this->Endpoint.'/pegawai/hapus-dokumen';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

      public function simpanDokumenLain($data = [])
    {
        $urlRequest =  $this->Endpoint.'/rekrutmen/simpan-dokumen-lainnya';
        $hasil = $this->curlAdapterPostRequestMultipart($urlRequest, $data);
        return $hasil;
    }

    public function simpanPegawaiDokumen($data = [])
    {
        $urlRequest =  $this->Endpoint.'/pegawai/simpan-dokumen';
        $hasil = $this->curlAdapterPostRequestMultipart($urlRequest, $data);
        return $hasil;
    }

     public function simpanRekrutmenDokumen($data = [])
    {
        $urlRequest =  $this->Endpoint.'/rekrutmen/simpan-rekrutmen-dokumen';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

    public function deleteRekrutmenDokumen($data = [])
    {
        $urlRequest =  $this->Endpoint.'/rekrutmen/delete-rekrutmen-dokumen';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

    public function deleteLamaran($data = [])
    {
        $urlRequest =  $this->Endpoint.'/rekrutmen/batalkan-rekrutmen';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

    public function deletePegawaiSeleksi($data = [])
    {
        $urlRequest =  $this->Endpoint.'/rekrutmen/hapus-rekrutmen';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

     public function deleteRekrutmenFormasi($data = [])
    {
        $urlRequest =  $this->Endpoint.'/master/hapus-formasi';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

     public function getRekrutmen($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/rekrutmen/get-rekrutmen?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

    public function getRekrutmenDokumen($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/rekrutmen/get-rekrutmen-dokumen?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

     public function getRekrutmenTahapan($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/rekrutmen/get-rekrutmen-tahapan?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

     public function getRekrutmenTahapanList($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/rekrutmen/get-rekrutmen-tahapan-list?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

    public function getRekrutmenById($data = [])
    {
        $query = $this->build_http_query($data);
        $urlRequest =  $this->Endpoint.'/rekrutmen/get-rekrutmen-by-id?'.$query;
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

     public function hapusMasterDokumen($data = [])
    {
        $urlRequest =  $this->Endpoint.'/master/hapus-dokumen';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

     public function hapusMasterPosisi($data = [])
    {
        $urlRequest =  $this->Endpoint.'/master/hapus-posisi';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }


    public function getJenjang($data = [])
    {
        $urlRequest =  $this->Endpoint.'/master/jenjang';
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

    public function getTahapan($data = [])
    {
        $urlRequest =  $this->Endpoint.'/master/tahapan';
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }
}

 ?>