<?php 

namespace App\Services;


class FtpService extends GenericService{

    public function __construct(){
        $this->ftp_host = config('app.FTP_HOST');
        $this->ftp_username = config('app.FTP_USERNAME');
        $this->ftp_password = config('app.FTP_PASSWORD');
    }

     public function _connect()
    {
        // FTP server 
        $ftpHost   = $this->ftp_host;
        $ftpUsername = $this->ftp_username;
        $ftpPassword = $this->ftp_password;

        // Membuat FTP connection
        $connId = ftp_connect($ftpHost) or die("Couldn't connect to $ftpHost");
        ftp_pasv($connId, true) or die("Passive mode failed");

        // try to login
        // if(@ftp_login($connId, $ftpUsername, $ftpPassword)){
        //     echo "Connected as $ftpUsername@$ftpHost";
        // }else{
        //     echo "Couldn't connect as $ftpUsername";
        // }

        // close / Tutup the connection
        // ftp_close($connId);

        return $connId;
    }

    public function check_exist($file){
         // FTP server 
        $ftpHost   = $this->ftp_host;
        $ftpUsername = $this->ftp_username;
        $ftpPassword = $this->ftp_password;

        // Membuat FTP connection
        $connId = ftp_connect($ftpHost) or die("Couldn't connect to $ftpHost");
        $ftpLogin = ftp_login($connId, $ftpUsername, $ftpPassword);
        ftp_pasv($connId, true) or die("Passive mode failed");
        $file_size = ftp_size($connId, $file);


        if ($file_size != -1) {
            ftp_close($connId);
            return true;

        } else {
            ftp_close($connId);
            return false;
        }
       
    }

    public function unlink($file){
        // FTP server details
           $ftpHost   = $this->ftp_host;
            $ftpUsername = $this->ftp_username;
            $ftpPassword = $this->ftp_password;

            // open an FTP connection
            $connId = ftp_connect($ftpHost) or die("Couldn't connect to $ftpHost");
           
            // login to FTP server
            $ftpLogin = ftp_login($connId, $ftpUsername, $ftpPassword);
             ftp_pasv($connId, true) or die("Passive mode failed");
            ftp_delete($connId, $file);

            ftp_close($connId);
            
    }

     public function upload($sourcefile, $destinationfile)
    {
        $ftpHost   = $this->ftp_host;
        $ftpUsername = $this->ftp_username;
        $ftpPassword = $this->ftp_password;

        // Membuat FTP connection
        $connId = ftp_connect($ftpHost) or die("Couldn't connect to $ftpHost");
       
        // login to FTP server
        $ftpLogin = ftp_login($connId, $ftpUsername, $ftpPassword);
         ftp_pasv($connId, true) or die("Passive mode failed");
        // try to upload file
        if(ftp_put($connId, $destinationfile, $sourcefile, FTP_BINARY, FTP_AUTORESUME)){
            ftp_close($connId);
            return array('success'=>true);
        }else{
             // close / Tutup the connection
            ftp_close($connId);
            abort(500);
        }

    }

    public function download($direktori, $file, $newName = '')
    {
        $ftpHost   = $this->ftp_host;
        $ftpUsername = $this->ftp_username;
        $ftpPassword = $this->ftp_password;

        if($newName == ''){
            $newName = $file;
        }

        // Membuat FTP connection
        $connId = ftp_connect($ftpHost) or die("Couldn't connect to $ftpHost");

        // login to FTP server
        $ftpLogin = ftp_login($connId, $ftpUsername, $ftpPassword);
        ftp_pasv($connId, true) or die("Passive mode failed");
        $destination = 'uploads/temporary/';
        if (!file_exists($destination)) {
                mkdir($destination, 0777, true);
        }

        // try to get file
        if(ftp_get($connId, $destination.$newName, $direktori.$file, FTP_BINARY)){
            ftp_close($connId);
            return array('success'=>true);
        }else{
             // close / Tutup the connection
            ftp_close($connId);
            abort(500);
        }

       
    }

    public function readFile($file){
        $ftpHost   = $this->ftp_host;
        $ftpUsername = $this->ftp_username;
        $ftpPassword = $this->ftp_password;

        // Membuat FTP connection
        $connId = ftp_connect($ftpHost) or die("Couldn't connect to $ftpHost");

        // login to FTP server
        $ftpLogin = ftp_login($connId, $ftpUsername, $ftpPassword);

        ftp_pasv($connId, true) or die("Passive mode failed");


        $h = fopen('php://temp', 'r+');
        $get = ftp_fget($connId, $h, $file, FTP_BINARY, 0);

        $fstats = fstat($h);
        fseek($h, 0);
        $contents = fread($h, $fstats['size']); 

       
        fclose($h);
        ftp_close($connId);
        return $contents;
    }

}

 ?>