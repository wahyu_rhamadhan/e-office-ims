<?php 

namespace App\Services;


class WaService extends GenericService{

	public function __construct(){
		$this->Endpoint = env('WA_SERVICE');
	}


     public function findSession($data = [])
    {
        $urlRequest =  $this->Endpoint.'/session/find/'.$data['id'];
        $hasil = $this->curlAdapterGetRequest($urlRequest);
        return $hasil;
    }

    public function createSession($data = [])
    {
        $urlRequest =  $this->Endpoint.'/session/add';
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }

    public function sendWa($data = [])
    {
        $urlRequest =  $this->Endpoint.'/chat/send?id='.$data['id'];
        $hasil = $this->curlAdapterPostRequest($urlRequest, $data);
        return $hasil;
    }
}

 ?>