<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GlobalClass\Response;
use App\Models\TblEmployee;
use App\Models\Pegawai;
use App\Models\RiwIzin;
use App\Models\Notifikasi;
use Carbon\Carbon;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\DB;
use App\Exports\IzinKonfirmasiKeluarExport;
use App\Exports\IzinExport;
use Maatwebsite\Excel\Facades\Excel;

class IzinController extends Controller
{

    public function izinByPegawaiId(Request $request){
        try{

            $data = $this->getData($request);

            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function getDetail(Request $request){
        try{

           $page = $request->input('is_mobile') ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');
           $data = RiwIzin::selectRaw("riw_izin.id, parent_id, status_id, case when jenis_izin_id = 1 then 'On Duty Leave' when jenis_izin_id = 2 then 'Off Duty Leave' end jenis_izin, case when status_id = 1 then 'Approved' when status_id = 2 then 'Rejected' else 'On Process' end status_izin, DATE_FORMAT(riw_izin.created_at,'%d/%m/%Y') tgl, DATE_FORMAT(confirmed_out_at,'%H:%i') as confirm_out, DATE_FORMAT(confirmed_in_at, '%H:%i') as confirm_in, pegawai.nama_lengkap as nama_atasan, pegawai.nip as nip_atasan, atasan_id, keperluan, tujuan, jenis_izin_id, jam_start, jam_end, riw_izin.no_doc")
            ->leftjoin('pegawai','pegawai.id','riw_izin.atasan_id')
            ->where( 'riw_izin.id', '=',  $request->input('id') );

            if(!$request->input('is_edit')){
                $data = $data->first();
                $parent_id = $data->parent_id ? $data->parent_id : $request->input('id');
                    $peserta = DB::select("
                        select
                                *
                            from
                                (
                                select
                                    pegawai_id,
                                    parent_id,
                                    nama_lengkap,
                                    nip
                                from
                                    riw_izin ri
                                left join pegawai p on
                                    p.id = ri.pegawai_id
                                where
                                    ri.id = ? and ri.deleted_at is null
                            union all
                                select
                                    pegawai_id ,
                                    parent_id,
                                    nama_lengkap,
                                    nip
                                from
                                    riw_izin ri
                                left join pegawai p on
                                    p.id = ri.pegawai_id
                                where
                                    parent_id = ? and ri.deleted_at is null
                            ) s

            ",[$parent_id, $parent_id]);

            $data->peserta = $peserta;

            }
            else{
                $data = $data->with('peserta');
                $data = $data->first();
            }
            
            $response = new Response(200,'Berhasil di dapat',$data,[],true);


            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function getSummaryPegawai(Request $request){
        try{
            $flag = $request->input('flag');

            if($flag == 'atasan'){

                $data = RiwIzin::selectRaw(' count(*) total, coalesce(sum(case when status_id is null then 1 else 0 end),0) proses, coalesce(sum(case when status_id = 1 then 1 else 0 end),0) approved, coalesce(sum(case when status_id = 2 then 1 else 0 end),0) rejected ')
                ->where('riw_izin.atasan_id', $request->input('pegawai_id'))
                ->whereNull('parent_id');
            //     if ( $request->status_id ) {

            //     if($request->status_id == 99){
            //         $data = $data->whereNull( 'riw_izin.status_id');
            //     }
            //     else{
            //          $data = $data->where( 'riw_izin.status_id', '=', $request->status_id );
            //     }
               
            // }
            
            // if ( $request->jenis_izin_id ) {
            //     $data = $data->where( 'riw_izin.jenis_izin_id', '=',  $request->jenis_izin_id );
            // }

            // if($request->keyword){
            //     $keyword = $request->keyword;
            //     $data = $data->whereRaw('no_doc like ?', '%'.$keyword.'%');
            // }

            // if ( $request->start_date && $request->end_date ) {
            //     $data = $data->whereBetween( DB::raw('date(riw_izin.created_at)'), [ $request->start_date, $request->end_date ] );
            // }


                $data = $data->first();
            }
            else{
                 $data = RiwIzin::selectRaw(' count(*) total, coalesce(sum(case when status_id is null then 1 else 0 end),0) proses, coalesce(sum(case when status_id = 1 then 1 else 0 end),0) approved, coalesce(sum(case when status_id = 2 then 1 else 0 end),0) rejected ')
                ->where('riw_izin.pegawai_id', $request->input('pegawai_id'));
                
            //      if ( $request->status_id ) {

            //     if($request->status_id == 99){
            //         $data = $data->whereNull( 'riw_izin.status_id');
            //     }
            //     else{
            //          $data = $data->where( 'riw_izin.status_id', '=', $request->status_id );
            //     }
               
            // }
            
            // if ( $request->jenis_izin_id ) {
            //     $data = $data->where( 'riw_izin.jenis_izin_id', '=',  $request->jenis_izin_id );
            // }

            // if($request->keyword){
            //     $keyword = $request->keyword;
            //     $data = $data->whereRaw('no_doc like ?', '%'.$keyword.'%');
            // }

            // if ( $request->start_date && $request->end_date ) {
            //     $data = $data->whereBetween( DB::raw('date(riw_izin.created_at)'), [ $request->start_date, $request->end_date ] );
            // }

                $data = $data->first();
            }
            

            $data = [
                'total'=>$data->total,
                'proses'=>$data->proses,
                'approved'=>$data->approved,
                'rejected'=>$data->rejected
            ];

            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

     public function izinByPegawaiIdMobile(Request $request){
        try{

            $page = $request->input('is_mobile') ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');
            $data = RiwIzin::selectRaw("id, jenis_izin_id,parent_id, status_id, case when jenis_izin_id = 1 then 'On Duty Leave' when jenis_izin_id = 2 then 'Off Duty Leave' end jenis_izin, case when status_id = 1 then 'Approved' when status_id = 2 then 'Rejected' else 'On Process' end status_izin, DATE_FORMAT(created_at,'%d/%m/%Y') tgl, DATE_FORMAT(confirmed_out_at,'%H:%i') as confirm_out, DATE_FORMAT(confirmed_in_at, '%H:%i') as confirm_in, riw_izin.no_doc")
            ->where( 'riw_izin.pegawai_id', '=',  $request->pegawai_id )
            ->orderBy('riw_izin.created_at','desc');

             if ( $request->status_id ) {

                if($request->status_id == 99){
                    $data = $data->whereNull( 'riw_izin.status_id');
                }
                else{
                     $data = $data->where( 'riw_izin.status_id', '=', $request->status_id );
                }
               
            }
            
            if ( $request->jenis_izin_id ) {
                $data = $data->where( 'riw_izin.jenis_izin_id', '=',  $request->jenis_izin_id );
            }

            if($request->keyword){

                $keyword = $request->keyword;
                $data = $data->whereRaw('no_doc like ?', '%'.$keyword.'%');
            }

            if ( $request->start_date && $request->end_date ) {
                $data = $data->whereBetween( DB::raw('date(riw_izin.created_at)'), [ $request->start_date, $request->end_date ] );
            }


            $data = $data->paginate($page);

            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function getApprovalMobile(Request $request){
        try{

            $page = $request->input('is_mobile') ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');
            $data = RiwIzin::selectRaw("riw_izin.id, jenis_izin_id,parent_id, status_id, case when jenis_izin_id = 1 then 'On Duty Leave' when jenis_izin_id = 2 then 'Off Duty Leave' end jenis_izin, case when status_id = 1 then 'Approved' when status_id = 2 then 'Rejected' else 'On Process' end status_izin, DATE_FORMAT(riw_izin.created_at,'%d/%m/%Y') tgl, DATE_FORMAT(confirmed_out_at,'%H:%i') as confirm_out, DATE_FORMAT(confirmed_in_at, '%H:%i') as confirm_in,pegawai.nama_lengkap, pegawai.nip, riw_izin.no_doc")
            ->where('atasan_id', $request->pegawai_id)
            ->leftjoin('pegawai','pegawai.id','riw_izin.pegawai_id')
            ->whereNull('parent_id')
            ->orderBy('riw_izin.created_at','desc');

            if ( $request->status_id ) {

                if($request->status_id == 99){
                    $data = $data->whereNull( 'riw_izin.status_id');
                }
                else{
                     $data = $data->where( 'riw_izin.status_id', '=', $request->status_id );
                }
               
            }
            
            if ( $request->jenis_izin_id ) {
                $data = $data->where( 'riw_izin.jenis_izin_id', '=',  $request->jenis_izin_id );
            }

            if($request->keyword){
                $keyword = $request->keyword;
                $data = $data->whereRaw('no_doc like ?', '%'.$keyword.'%');
            }

            if ( $request->start_date && $request->end_date ) {
                $data = $data->whereBetween( DB::raw('date(riw_izin.created_at)'), [ $request->start_date, $request->end_date ] );
            }

            $data = $data->paginate($page);

            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

     public function getRiwIzin(Request $request){
        try{
            
            $data = $this->_getRiwIzin($request);
            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    } 
     
     protected function _getRiwIzin($request){
        $page = $request->input('is_mobile') ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');
            $data = RiwIzin::with(['peserta','detailPegawai.unit'])
            ->selectRaw("riw_izin.*, concat(atasan.empname, ' - ', atasan.empcode) nama_atasan, ROW_NUMBER() OVER (order by created_at desc) nomor, case when jenis_izin_id = 1 then 'On Duty' when jenis_izin_id = 2 then 'Of Duty' end jenis_izin, case when status_id = 1 then 'Approved' when status_id = 2 then 'Rejected' else 'On Process' end status_izin, DATE_FORMAT(created_at,'%d/%m/%Y') tgl, tblemployee.empcode, tblemployee.empname, DATE_FORMAT(confirmed_out_at,'%d/%m/%Y %H:%i') as confirm_out, DATE_FORMAT(confirmed_in_at,'%d/%m/%Y %H:%i') as confirm_in   ")
            ->leftjoin( 'tblemployee', 'tblemployee.pegawai_id', '=', 'riw_izin.pegawai_id' )
            ->leftjoin(DB::raw('tblemployee as atasan'),'atasan.pegawai_id','riw_izin.atasan_id');

            if ( $request->status_id ) {

                if($request->status_id == 99){
                    $data = $data->whereNull( 'riw_izin.status_id');
                }
                else{
                     $data = $data->where( 'riw_izin.status_id', '=', $request->status_id );
                }
               
            }
            if ( $request->jenis_izin_id ) {
                $data = $data->where( 'riw_izin.jenis_izin_id', '=',  $request->jenis_izin_id );
            }

            if ( $request->unit_id ) {
                $data = $data->where( 'tblemployee.unit_tree_id', 'like',  $request->unit_id.'%' );
            }

            if ( $request->start_date && $request->end_date ) {
                $data = $data->whereBetween( 'riw_izin.tgl_izin', [ $request->start_date, $request->end_date ] );
            }


            
            if($request->is_export === true){
                $data = $data->get();
            }
            else{
                $data = $data->paginate( $page);
            }

            return $data;
     }

     public function exportIzin(Request $request){
        try{
            $data = $this->_getRiwIzin($request);
            return Excel::download(new IzinExport($data), 'export.xlsx');
        }
        catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
     }

     public function exportKonfirmasiKeluar(Request $request){
        try{
             $data = RiwIzin::with('peserta')->with('detailPegawai.unit')
            ->selectRaw("riw_izin.*, concat(atasan.empname, ' - ', atasan.empcode) nama_atasan, ROW_NUMBER() OVER (partition by pegawai_id order by created_at desc) nomor, DATE_FORMAT(confirmed_out_at,'%d/%m/%Y %H:%i') as confirm_out, DATE_FORMAT(confirmed_in_at,'%d/%m/%Y %H:%i') as confirm_in,case when jenis_izin_id = 1 then 'On Duty' when jenis_izin_id = 2 then 'Of Duty' end jenis_izin   ")
            ->leftjoin( 'tblemployee', 'tblemployee.pegawai_id', '=', 'riw_izin.pegawai_id' )
            ->leftjoin(DB::raw('tblemployee as atasan'),'atasan.pegawai_id','riw_izin.atasan_id');

            

            if ( $request->tgl ) {
                $data = $data->whereDate( 'riw_izin.confirmed_out_at', [ $request->tgl] );
            }

           $data = $data->get();
            return Excel::download(new IzinKonfirmasiKeluarExport($data), 'export.xlsx');
        }
        catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
     }

     public function getApproval(Request $request){
        try{
            $params = [
                'pegawai_id'=>$request->input('pegawai_id'),
                'is_mobile'=>$request->input('is_mobile'),
                'status_id'=>$request->input('status_id'),
                'jenis_izin_id'=>$request->input('jenis_izin_id'),
                'start_date'=>$request->input('start_date'),
                'end_date'=>$request->input('end_date')
            ];

            $page = $params['is_mobile'] ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');
            $data = RiwIzin::with(['peserta','detailPegawai.unit'])
            ->selectRaw("riw_izin.*, concat(atasan.empname, ' - ', atasan.empcode) nama_atasan, ROW_NUMBER() OVER (order by created_at desc) nomor, case when jenis_izin_id = 1 then 'On Duty' when jenis_izin_id = 2 then 'Of Duty' end jenis_izin, case when status_id = 1 then 'Approved' when status_id = 2 then 'Rejected' else 'On Process' end status_izin, DATE_FORMAT(created_at,'%d/%m/%Y') tgl, tblemployee.empcode, tblemployee.empname, DATE_FORMAT(confirmed_out_at,'%d/%m/%Y %H:%i') as confirm_out, DATE_FORMAT(confirmed_in_at,'%d/%m/%Y %H:%i') as confirm_in, no_doc   ")
            ->where('atasan_id',$params['pegawai_id'])
            ->whereNull('parent_id')
            ->leftjoin( 'tblemployee', 'tblemployee.pegawai_id', '=', 'riw_izin.pegawai_id' )
            ->leftjoin(DB::raw('tblemployee as atasan'),'atasan.pegawai_id','riw_izin.atasan_id');

            if ( $params['status_id'] ) {

                if($params['status_id'] == 99){
                    $data = $data->whereNull( 'riw_izin.status_id');
                }
                else{
                    $data = $data->where( 'riw_izin.status_id', '=', $params['status_id'] );
                }
                
            }
            if ( $params['jenis_izin_id'] ) {
                $data = $data->where( 'riw_izin.jenis_izin_id', '=',  $params['jenis_izin_id'] );
            }
            if ( $request['start_date'] && $request['end_date'] ) {
                $data = $data->whereBetween( 'riw_izin.tgl_izin', [ $params['start_date'], $params['end_date'] ] );
            }

            $data = $data->paginate($page);
            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function generateQrCode($id){
        $qrcode = QrCode::format('png')->size(400)->generate($id);
        return $qrcode;
    }

    public function confirmIzin(Request $request){
        try{
            $id = $request->input('id');
            $izin = RiwIzin::where('id',$id)->first();

            if($request->input('pilihan') == 'Keluar'){
                $izin->confirmed_out_at = Carbon::now();
                $izin->save();

                // RiwIzin::where('id',$id)->update(['confirmed_out_at'=>Carbon::now()]);
            }

            else{
                 $izin->confirmed_in_at = Carbon::now();
                 $izin->save();

                // RiwIzin::where('id',$id)->update(['confirmed_in_at'=>Carbon::now()]);
            }
            $response = new Response(200,'Berhasil di simpan',[],[],true);
            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

     public function delIzin(Request $request){
        DB::beginTransaction();
        try{
            $id = $request->input( 'id' );
            $rizin = RiwIzin::where( 'id', '=', $id )->first();
            $rizin->deleted_at = Carbon::now();
            $rizin->save();


            $izin2 = RiwIzin::where('parent_id','=',$id)->update([
                'deleted_at' =>Carbon::now()
            ]);

            $notif = Notifikasi::where('riwayat_id','=',$id)->first();

            if($notif){
                $notif->deleted_at = Carbon::now();
                $notif->save();
            }
           
            $response = new Response(200,'Berhasil di hapus',[],[],true);
             DB::commit();
            return $response->getResponse();

        }catch(\Exception $e){
             DB::rollback();
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function simpanIzin(Request $request){
        DB::beginTransaction();
        try {
             $params = [
                'pegawai_id'=>$request->input( 'pegawai_id' ),
                'atasan_id'=>$request->input( 'atasan_id' ),
                'jenis_izin_id'=>$request->input( 'jenis_izin_id' ),
                'keperluan'=>$request->input( 'keperluan' ),
                'tujuan'=>$request->input( 'tujuan' ),
                'jam_start'=>$request->input( 'jam_start' ),
                'jam_end'=>$request->input( 'jam_end' ),
                'tgl_izin'=>$request->input( 'tgl_izin' ),
                'pegawai_selected'=>$request->input('pegawaiSelected'),

            ];

             $newNumber = DB::select("
                select coalesce(max(urutan),0) + 1 as urutan, month(now()) bln, year(now()) th  from riw_izin where month(created_at) = month(now()) and year(created_at) = year(now())
                ");
            $noID = str_pad($newNumber[0]->urutan, 4, '0', STR_PAD_LEFT);
            $noDoc = "IMO-".$noID."/"."DUTY/".$newNumber[0]->bln."/".$newNumber[0]->th;
            $urutan = $newNumber[0]->urutan;
            $noDoc = $noDoc;
            
            $pegawai = Pegawai::selectRaw('nama_lengkap, nip')->where('id',$params['pegawai_id'])->first();
            $id = $this->uuid();
            $rizin = new RiwIzin();
            $rizin->id = $id;
            $rizin->pegawai_id = $params[ 'pegawai_id' ];
            $rizin->atasan_id = $params['atasan_id'];
            $rizin->jenis_izin_id = $params[ 'jenis_izin_id' ];
            $rizin->keperluan = $params[ 'keperluan' ];
            $rizin->tujuan = $params[ 'tujuan' ];
            $rizin->jam_start = $params[ 'jam_start' ];
            $rizin->jam_end = $params[ 'jam_end' ];
            $rizin->tgl_izin = $params[ 'tgl_izin' ];
            $rizin->urutan = $urutan;
            $rizin->no_doc = $noDoc;
            $rizin->save();           

            foreach ($params['pegawai_selected'] as $key => $value) {
                $params_insert = [
                    'id'=>$this->uuid(),
                    'pegawai_id'=>$value['pegawai_id'],
                    'atasan_id'=>$params['atasan_id'],
                    'jenis_izin_id'=>$params[ 'jenis_izin_id' ],
                    'keperluan'=>$params[ 'keperluan' ],
                    'tujuan'=>$params[ 'tujuan' ],
                    'jam_start'=>$params['jam_start'],
                    'jam_end'=>$params[ 'jam_end' ],
                    'tgl_izin'=>$params[ 'tgl_izin' ],
                    'parent_id'=>$id,
                    'urutan'=>$urutan,
                    'no_doc'=>$noDoc

                ];

                RiwIzin::create($params_insert);


             } 
            
            $izin = $params[ 'jenis_izin_id' ] == 1 ? 'On Duty' : 'Off Duty';
            $judul = $izin.' Leave Request';
            $keterangan = $izin.' Leave Requests From '.$pegawai->nama_lengkap.' - '.$pegawai->nip;
            $paramsNotif = [
                'judul'=>$judul,
                'from_pegawai_id'=>$params['pegawai_id'],
                'to_pegawai_id'=>$params['atasan_id'],
                'riwayat'=>'riw_izin',
                'riwayat_id'=>$id,
                'keterangan'=>$keterangan
            ];


            $this->sendNotifikasi($paramsNotif);
            DB::commit();
            $response = new Response( 200, 'Data Berhasil Di simpan', [], [], true );

        } catch( \Exception $e ) {
            DB::rollback();
            $errors[] = $e->getMessage();
            $response = new Response( 500, 'Terjadi Kesalahan pada server', [], $errors, false );

        }
        return $response->getResponse();
    }


    public function approval(Request $request, $id){
         DB::beginTransaction();
        try{
            $params = [
                'status_id'=>$request->input( 'status_id' ),
                'catatan_penolakan'=>$request->input('catatan_penolakan')
            ];
            $rizin = RiwIzin::find( $id );
            $rizin->status_id = $params[ 'status_id' ];
            $rizin->catatan_penolakan = $params[ 'catatan_penolakan'];
            $rizin->update();

            $izin = $rizin->jenis_izin_id == 1 ? 'On Duty' : 'Off Duty';
            $status = $params['status_id'] == 1 ? 'Approved' : 'Rejected';
            $pegawai = Pegawai::selectRaw('nama_lengkap, nip')->where('id',$rizin->pegawai_id)->first();
            $judul = $izin.' Leave Request';
            $keterangan = 'Your '.$izin.' Leave Request '.$rizin->no_doc.' Has Been '.$status;
            $paramsNotif = [
                'judul'=>$judul,
                'from_pegawai_id'=>$rizin->atasan_id,
                'to_pegawai_id'=>$rizin->pegawai_id,
                'riwayat'=>'riw_izin',
                'riwayat_id'=>$id,
                'keterangan'=>$keterangan
            ];


            $this->sendNotifikasi($paramsNotif);

            RiwIzin::where('parent_id',$id)->update($params);
             DB::commit();
            $response = new Response( 200, 'Data Berhasil Di simpan', [], [], true );
        }
        catch(\Exception $e){
            DB::rollback();
            $errors[] = $e->getMessage();
            $response = new Response( 500, 'Terjadi Kesalahan pada server', [], $errors, false );
        }
         return $response->getResponse();
    }
    public function updateIzin(Request $request, $id){
        DB::beginTransaction();
        try {

             $params = [
                'jenis_izin_id'=>$request->input( 'jenis_izin_id' ),
                'atasan_id'=>$request->input( 'atasan_id' ),
                'keperluan'=>$request->input( 'keperluan' ),
                'tujuan'=>$request->input( 'tujuan' ),
                'jam_start'=>$request->input( 'jam_start' ),
                'jam_end'=>$request->input( 'jam_end' ),
                'tgl_izin'=>$request->input( 'tgl_izin' ),
                'pegawai_selected'=>$request->input('pegawaiSelected'),
            ];


            $rizin = RiwIzin::find( $id );
            $rizin->jenis_izin_id = $params[ 'jenis_izin_id' ];
            $rizin->keperluan = $params[ 'keperluan' ];
            $rizin->tujuan = $params[ 'tujuan' ];
            $rizin->jam_start = $params[ 'jam_start' ];
            $rizin->jam_end = $params[ 'jam_end' ];
            $rizin->tgl_izin = $params[ 'tgl_izin' ];
            $rizin->atasan_id = $params['atasan_id'];
            $rizin->update();

            $pegawaiSelected = $params['pegawai_selected'];
            unset($params['pegawai_selected']);
            // update peserta
            RiwIzin::where('parent_id',$id)->update($params);

            $payloadPegawai = array_column($pegawaiSelected, 'pegawai_id');
            $existPegawai = RiwIzin::where('parent_id',$id)->get()->toArray();
            $existPegawai = array_column($existPegawai, 'pegawai_id');
            $diffDel = array_diff($existPegawai, $payloadPegawai);
            $diffIns = array_diff($payloadPegawai, $existPegawai);
            // delete peserta
            foreach ($diffDel as $key => $value) {
                RiwIzin::where('parent_id',$id)->where('pegawai_id',$value)->delete();
            }

            // tambah peserta
            foreach ($diffIns as $key => $value) {
                 $params_insert = [
                    'id'=>$this->uuid(),
                    'pegawai_id'=>$value,
                    'atasan_id'=>$params['atasan_id'],
                    'jenis_izin_id'=>$params[ 'jenis_izin_id' ],
                    'keperluan'=>$params[ 'keperluan' ],
                    'tujuan'=>$params[ 'tujuan' ],
                    'jam_start'=>$params['jam_start'],
                    'jam_end'=>$params[ 'jam_end' ],
                    'tgl_izin'=>$params[ 'tgl_izin' ],
                    'parent_id'=>$id

                ];

                RiwIzin::create($params_insert);

            }
            DB::commit();

            $response = new Response(200,'Berhasil di simpan',[],[],true);

            return $response->getResponse();

        }catch(\Exception $e){
             DB::rollback();
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function getConfirmedBySatpam(Request $request){
        $page = config('app.WEB_PER_PAGE');
        $data = RiwIzin::with('peserta')->with('detailPegawai')
            ->selectRaw("riw_izin.*, ROW_NUMBER() OVER (order by created_at desc) nomor, DATE_FORMAT(confirmed_out_at,'%d/%m/%Y %H:%i') as confirm_out, DATE_FORMAT(confirmed_in_at,'%d/%m/%Y %H:%i') as confirm_in,case when jenis_izin_id = 1 then 'On Duty' when jenis_izin_id = 2 then 'Of Duty' end jenis_izin,case when status_id = 1 then 'Approved' when status_id = 2 then 'Rejected' else 'On Process' end status_izin   ")
            ->leftjoin( 'Pegawai', 'pegawai.id', '=', 'riw_izin.pegawai_id' );
            

            if ( $request->tgl ) {
                $data = $data->whereDate( 'riw_izin.created_at', [ $request->tgl] );
            }

           $data = $data->paginate( $page);

          $response = new Response(200,'Berhasil di simpan',$data,[],true);

            return $response->getResponse();
    }
    protected function getData($request, $export = false){
        $page = $request->input('is_mobile') ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');
        $data = RiwIzin::with('peserta')
            ->selectRaw("riw_izin.*, concat(atasan.empname, ' - ', atasan.empcode) nama_atasan, ROW_NUMBER() OVER (partition by pegawai_id order by created_at desc) nomor, case when jenis_izin_id = 1 then 'On Duty' when jenis_izin_id = 2 then 'Of Duty' end jenis_izin, case when status_id = 1 then 'Approved' when status_id = 2 then 'Rejected' else 'On Process' end status_izin, DATE_FORMAT(created_at,'%d/%m/%Y') tgl, DATE_FORMAT(confirmed_out_at,'%d/%m/%Y %H:%i') as confirm_out, DATE_FORMAT(confirmed_in_at,'%d/%m/%Y %H:%i') as confirm_in   ")
            ->leftjoin( 'tblemployee', 'tblemployee.pegawai_id', '=', 'riw_izin.pegawai_id' )
            ->leftjoin(DB::raw('tblemployee as atasan'),'atasan.pegawai_id','riw_izin.atasan_id')
            ->whereNull('parent_id');

            if ( $request->status_id ) {

                if($request->status_id == 99){
                    $data = $data->whereNull( 'riw_izin.status_id');
                }
                else{
                     $data = $data->where( 'riw_izin.status_id', '=', $request->status_id );
                }
               
            }
            if ( $request->jenis_izin_id ) {
                $data = $data->where( 'riw_izin.jenis_izin_id', '=',  $request->jenis_izin_id );
            }

            if ( $request->unit_id ) {
                $data = $data->where( 'tblemployee.unit_tree_id', 'like',  $request->unit_id.'%' );
            }

            if ( $request->start_date && $request->end_date ) {
                $data = $data->whereBetween( 'riw_izin.tgl_izin', [ $request->start_date, $request->end_date ] );
            }

            // filter by pegawai id
            if ( $request->pegawai_id ) {
                $data = $data->where( 'riw_izin.pegawai_id', '=',  $request->pegawai_id );
            }

            // filter by id izin
            if ( $request->filled( 'id' ) ) {
                $data = $data->where( 'riw_izin.id', $request->id )->first();
            } else {

                $data = $export ? $data->get() : $data->paginate( $page);
            }

        return $data; 
    }

    

  
}
