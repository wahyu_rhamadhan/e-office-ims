<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{
    Pegawai,
    MutasiDraft,
    MutasiList
};
use File;
use Validator;
use App\GlobalClass\Response;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Services\FtpService;
use PhpOffice\PhpWord\TemplateProcessor;

class MutasiController extends Controller
{

    public function simpanDraftMutasi(Request $request)
    {
        try {

            $params = [
                'id' => $this->uuid(),
                'no_sk' => $request->input('no_sk'),
            ];

            MutasiDraft::create($params);
            $response = new Response(200, 'Berhasil di simpan', $params, [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function releaseDraftMutasi(Request $request)
    {
        DB::beginTransaction();
        try {

            $params = [
                'id' => $this->uuid(),
                'no_sk' => $request->input('no_sk'),
            ];

            // reset is new manjab
            $data =  MutasiDraft::query()->update(['is_new' => null]);

            $draft = MutasiDraft::where('id', $request->input('mutasi_draft_id'))->first();
            $draft->no_sk = $request->input('no_sk');
            $draft->release_date = Carbon::now();
            $draft->is_new = 1;
            $draft->save();


            // insert ke riwayat mutasi

            $insertRiw = DB::select("
            insert INTO riw_mutasi (
                id,
                no_sk,
                mutasi_draft_id ,
                pegawai_id ,
                unit_tree_id_prev ,
                unit_name_prev ,
                unit_manjab_draft_id_prev ,
                unit_tree_id_aft ,
                unit_name_aft ,
                unit_manjab_draft_id_aft )
            select
                UUID(),
                ? no_sk,
                mutasi_draft_id ,
                pegawai_id ,
                pegawai_unit_tree_id_prev ,
                pegawai_unit_name_prev ,
                pegawai_unit_manjab_draft_id_prev,
                pegawai_unit_tree_id_aft ,
                pegawai_unit_name_aft ,
                pegawai_unit_manjab_draft_id_aft
            from
                mutasi_list md
            where mutasi_draft_id = ? and md.deleted_at is null

            ", [$request->input('no_sk'), $request->input('mutasi_draft_id')]);


            // update data pegawai

            $updPgw = DB::select("
            update pegawai p
            inner join mutasi_list ml on
                ml.pegawai_id = p.id
            left join manjab_unit mu on
                mu.tree_id = ml.pegawai_unit_tree_id_aft
                and mu.manjab_draft_id = ml.pegawai_unit_manjab_draft_id_aft and mu.deleted_at is null
            SET
                p.master_unit_id = mu.id,
                p.master_unit_tree_id = ml.pegawai_unit_tree_id_aft,
                p.nama_unit = ml.pegawai_unit_name_aft,
                p.last_manjab_draft_id = ml.pegawai_unit_manjab_draft_id_aft
            where
                ml.deleted_at is null
                and ml.mutasi_draft_id = ?

            ", [$request->input('mutasi_draft_id')]);

            DB::commit();
            $response = new Response(200, 'Berhasil di simpan', $params, [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            DB::rollback();
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function simpanDraftPegawai(Request $request)
    {
        try {

            $params = [];

            foreach ($request->input('listPegawai') as $value) {
                $params[] = [
                    'id' => $this->uuid(),
                    'mutasi_draft_id' => $request->input('mutasi_draft_id'),
                    'pegawai_nama' => $value['nama_lengkap'],
                    'pegawai_nip' => $value['nip'],
                    'pegawai_id' => $value['pegawai_id'],
                    'pegawai_unit_tree_id_prev' => $value['master_unit_tree_id'],
                    'pegawai_unit_name_prev' => $value['nama_unit'],
                    'pegawai_unit_manjab_draft_id_prev' => $value['last_manjab_draft_id'],
                    'pegawai_unit_tree_id_aft' => $request->input('unit_tree_id_aft'),
                    'pegawai_unit_name_aft' => $request->input('unit_nama_aft'),
                    'pegawai_unit_manjab_draft_id_aft' => $request->input('unit_manjab_draft_id_aft'),
                    'created_at' => Carbon::now()
                ];
            }



            // insert ke DB
            MutasiList::insert($params);

            $response = new Response(200, 'Berhasil di simpan', [], [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getDraftMutasi(Request $request)
    {
        try {

            $data = MutasiDraft::all();
            $response = new Response(200, 'Berhasil di simpan', $data, [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getListMutasi(Request $request)
    {
        try {

            $data = MutasiList::where('mutasi_draft_id', $request->input('mutasi_draft_id'))
                ->paginate(config('app.WEB_PER_PAGE'));

            $response = new Response(200, 'Berhasil di simpan', $data, [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getPegawaiForMutasi(Request $request)
    {
        try {

            $params = [
                'search' => $request->input('search'),
            ];

            $page = config('app.WEB_PER_PAGE');

            $mutasi_draft_id = $request->input('selectedDraft');
            $data = Pegawai::selectRaw("nama_lengkap, nama_jabatan, nama_unit, pegawai.id as pegawai_id,nip,master_golongan_id,kelas_jabatan,master_status_pegawai_id, master_unit_tree_id, last_manjab_draft_id")
                ->whereNull('tgl_selesai')
                ->leftJoin('mutasi_list', function ($join) use ($mutasi_draft_id) {
                    $join->on('mutasi_list.pegawai_id', '=', 'pegawai.id')
                        ->where('mutasi_draft_id', $mutasi_draft_id);
                })
                ->whereNull('mutasi_list.pegawai_id')
                ->orderByRaw("case when kelas_jabatan is null then 'XXX' else kelas_jabatan end asc, master_unit_id asc");

            if ($params['search']) {
                $data = $data->whereRaw(' (pegawai.nama_lengkap like ? or pegawai.nama_jabatan like ? or pegawai.nip like ? or pegawai.nama_unit like ?) ', ['%' . $params['search'] . '%', '%' . $params['search'] . '%', '%' . $params['search'] . '%', '%' . $params['search'] . '%']);
            }


            $data = $data->paginate($page);
            $response = new Response(200, 'Berhasil di simpan', $data, [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function hapusDraftMutasi(Request $request)
    {
        try {

            $data = MutasiDraft::where('id', $request->input('id'))->delete();
            $data->deleted_at = Carbon::now();
            $data->save();

            MutasiList::where('mutasi_draft_id', $request->input('id'))->update(['deleted_at' => Carbon::now()]);

            $response = new Response(200, 'Berhasil di hapus', [], [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    // public function cetakMutasi(Request $request)
    // {
    //     try {

    //         $data = MutasiList::where('mutasi_draft_id', $request->input('mutasi_draft_id'))->get();

    //         $template = new TemplateProcessor(storage_path('app/template/mutasi.docx'));

    //         $template->setValue('no_sk', $request->input('no_sk'));
    //         $template->setValue('tgl_sk', Carbon::now()->format('d F Y'));

    //         $table = [];
    //         $no = 1;
    //         foreach ($data as $value) {
    //             $table[] = [
    //                 'no' => $no++,
    //                 'nama' => $value->pegawai_nama,
    //                 'nip' => $value->pegawai_nip,
    //                 'unit_prev' => $value->pegawai_unit_name_prev,
    //                 'unit_aft' => $value->pegawai_unit_name_aft,
    //             ];
    //         }

    //         $template->cloneRowAndSetValues('no', $table);

    //         $filename = 'Mutasi-' . $request->input('no_sk') . '.docx';
    //         $template->saveAs(storage_path('app/public/' . $filename));

    //         $response = new Response(200, 'Berhasil di simpan', ['url' => url('storage/' . $filename)], [], true);

    //         return $response->getResponse();
    //     } catch (\Exception $e) {
    //         $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
    //         return $response->getResponse();
    //     }
    // }
}