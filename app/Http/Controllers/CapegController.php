1<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GlobalClass\Response;
use App\Models\RiwCapeg;
use App\Models\Notifikasi;
use Illuminate\Support\Facades\DB;

class CapegController extends Controller
{
    public function getCapeg(Request $request){
        try{
            $params = [
              'pegawai_id'=>$request->input('pegawai_id'),
            ];
            $data = RiwCapeg::selectRaw('riw_capeg.*, date(tgl_capeg) tgl_capeg')->where('pegawai_id',$params['pegawai_id'])->first();
            $response = new Response(200,'Berhasil di dapat',$data,[],true);
        }catch(\Exception $e){
            $response = new Response(500,'Ada kesalahan server',[],$e->getMessage(),false);
        }

        return $response->getResponse();
    }
    public function simpanCapeg( Request $request ) {
        DB::beginTransaction();
        try {
            $params = [
                'pegawai_id'=>$request->input('pegawai_id'),
                'tgl_capeg'=>$request->input('tgl_capeg'),
                'tgl_selesai'=>$request->input('tgl_selesai'),
                'no_sk'=>$request->input('no_sk'),
                'nip'=>$request->input('nip'),
                'master_status_pegawai_id'=>87
            ];
            if(!empty($request->input( 'id' ))){
                // update
                $rc = RiwCapeg::where('id',$request->input( 'id' ))->first();
                $rc->update($params);
            }
            else{
                $params['id'] = $this->uuid();
                $rc = RiwCapeg::create($params);
            }
             $this->updateDataPegawai($request->input('pegawai_id'));
             DB::commit();
             $response = new Response( 200, 'Data Berhasil Di simpan', [], [] , false);
            
        } catch( \Exception $e ) {
            DB::rollback();
            $errors[] = $e->getMessage();
            $response = new Response( 500, 'Terjadi Kesalahan pada server', [], $errors, false);
        }

        return $response->getResponse();
    }
    public function delCapeg( Request $request ) {
        try {
            $id = $request->input('id');
            $spr = RiwCapeg::where( 'id' , $id );
            if($spr){
                $spr->delete();
                $response = new Response( 200, 'Data berhasil dihapus', [], [], false );
            }else{
                $response = new Response( 200, 'Data Tidak Ditemukan', [], [], false );
            }
        } catch( \Exception $e ) {
            $errors[] = $e->getMessage();
            $response = new Response( 500, 'Terjadi Kesalahan pada server', [],[], $errors, false );
        }

        return $response->getResponse();
    }
}
