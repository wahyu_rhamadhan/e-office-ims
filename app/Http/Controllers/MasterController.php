<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Agama;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\Desa;
use App\Models\Universitas;
use App\Models\{
    ManjabUnit,
    ManjabDraft, 
    MasterUnit,
    RiwJabatan,
    MasterJabatan 
};
use File;
use Validator;
use App\GlobalClass\Response;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class MasterController extends Controller
{

    public function simpanDraftUnit(Request $request){
        try{
           
            $params = [
                'id'=>$request->input('id'),
                'kelas_jabatan'=>$request->input('kelas_jabatan'),
                'parent_id'=>$request->input('parent_id'),
                'unit'=>$request->input('unit'),
                'manjab_draft_id'=>$request->input('manjab_draft_id'),

            ];

            if($request->input('id')){
                // update
                if($params['kelas_jabatan'] == 'PST000'){
                    $params['nama_jabatan'] = str_replace('DIREKTORAT', 'DIREKTUR', $params['unit']);
                }
                else{
                    $params['nama_jabatan'] = 'KEPALA '.$params['unit'];
                }
                $data = ManjabUnit::where('id',$params['id'])->update($params);
            }
            else{
                $new_id = ManjabUnit::selectRaw("
                         coalesce(max(cast(replace(SUBSTRING(tree_id , length(?)+ 1, length(tree_id)), '.', '') as unsigned)), 0) + 1 as id
                    ",[$params['parent_id']])
                ->whereRaw("
                        length(tree_id) - length(replace(tree_id,'.','')) = length(?) - length(replace(?,'.','')) + 1 and tree_id like ?
                    ",[$params['parent_id'],$params['parent_id'], $params['parent_id'].'%'])
                ->first();
    

                $tree_id = $params['parent_id'].$new_id->id.'.';
                $params['id'] = $this->uuid();
                $params['tree_id'] = $tree_id;
                if($params['kelas_jabatan'] == 'PST000'){
                    $params['nama_jabatan'] = str_replace('DIREKTORAT', 'DIREKTUR', $params['unit']);
                }
                else{
                    $params['nama_jabatan'] = 'KEPALA '.$params['unit'];
                }

                if($params['parent_id']){
                    $update = ManjabUnit::where('tree_id',$params['parent_id'])
                    ->where('manjab_draft_id',$params['manjab_draft_id'])
                    ->first();

                    $update->have_child = 1;
                    $update->save();
                }
                
                $res = ManjabUnit::create($params);

                $data = [];
                $data = $res;
                $data['name'] = $res->unit;
               
            }
            $response = new Response(200,'Berhasil di simpan',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function getUnit(Request $request){
        try{
           

            $tree_id = $request->input('tree_id');

            if($request->input('tree_id')){
                  $data =  MasterUnit::selectRaw('id,tree_id, unit as name,have_child, kelas_jabatan, parent_id, nama_jabatan, manjab_draft_id')
                ->orderBy('tree_id','asc')
                ->whereRaw(" length(tree_id) - length(replace(tree_id,'.','')) = length(?) - length(replace(?,'.','')) + 1 ",[$tree_id,$tree_id])
                    ->where('tree_id','like',$tree_id.'%')
                ->get();

                foreach ($data as $key => $value) {
                    if($value['have_child'] == 1){
                        $data[$key]['children'] = [];
                    }
                }
            }
            else{
                $unit =  MasterUnit::selectRaw('id,tree_id, unit as name,have_child, kelas_jabatan, parent_id, nama_jabatan, manjab_draft_id')
                ->orderBy('tree_id','asc')->whereRaw("length(tree_id) - length(replace(tree_id,'.','')) = 1")->get();

                foreach ($unit as $key => $value) {
                    if($value['have_child'] == 1){
                        $unit[$key]['children'] = [];
                    }
                }
                $data[]= [
                    'have_child'=>1,
                    'tree_id'=>null,
                    'id'=>null,
                    'nama_jabatan'=>'PT INKA MULTI SOLUSI',
                    'name'=>'PT INKA MULTI SOLUSI',
                    'children' => $unit
                ];
            }

            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();
        }
        catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function getJabatanNonStr(Request $request){
         try{
           
              $tree_id = $request->input('tree_id');

            if($request->input('tree_id')){
                  $data =  MasterJabatan::selectRaw('id,tree_id, jabatan as nama_jabatan, kelas_jabatan, have_children')
                ->orderBy('tree_id','asc')
                ->whereRaw(" length(tree_id) - length(replace(tree_id,'.','')) = length(?) - length(replace(?,'.','')) + 1 ",[$tree_id,$tree_id])
                    ->where('tree_id','like',$tree_id.'%')
                ->get();

                foreach ($data as $key => $value) {
                    if($value['have_children'] == 1){
                        $data[$key]['children'] = [];
                    }
                }
            }
            else{
                $data =  MasterJabatan::selectRaw('id,tree_id, jabatan as nama_jabatan, kelas_jabatan, have_children')
                ->orderBy('tree_id','asc')->whereRaw("length(tree_id) - length(replace(tree_id,'.','')) = 1")->get();

                foreach ($data as $key => $value) {
                    if($value['have_children'] == 1){
                        $data[$key]['children'] = [];
                    }
                }
            }

             $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

                
        }
        catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
             return $response->getResponse();

        }

    }

    public function hapusDraftManjab(Request $request){
        try{
           
            $data = ManjabDraft::where('id',$request->input('id'))->first();
            $data->deleted_at = Carbon::now();
            $data->save();

            ManjabUnit::where('manjab_draft_id',$request->input('id'))->update(['deleted_at'=>Carbon::now()]);

            $response = new Response(200,'Berhasil di simpan',[],[],true);

            return $response->getResponse();
        }
        catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function hapusDraftUnit(Request $request){
        DB::beginTransaction();
        try{
            
            $data = ManjabUnit::where('id', $request->input('id'))->first();

            // hapus beserta anaknya
            $delete = ManjabUnit::where('tree_id','like',$data->tree_id.'%')
            ->where('manjab_draft_id',$request->input('manjab_draft_id'))
            ->update(['deleted_at'=>Carbon::now()]);

            $parent = ManjabUnit::whereRaw("
                length(tree_id) - length(replace(tree_id,'.','')) = length(?) - length(replace(?,'.','')) + 1  and tree_id like ?
            ",[$request->input('parent_id'),$request->input('parent_id'), $request->input('parent_id').'%'])->count();

            // update children
            if($parent == 0){
               $upt = ManjabUnit::where('tree_id',$request->input('parent_id'))
               ->where('manjab_draft_id',$request->input('manjab_draft_id'))
               ->first();

               if($upt){
                $upt->have_child = null;
                $upt->save();
               }
               
            }
            DB::commit();
            $response = new Response(200,'Berhasil di hapus',[],[],true);
            return $response->getResponse();

        }catch(\Exception $e){
            DB::rollback();
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function simpanManjabJabatan(Request $request){
        DB::beginTransaction();
        try{
           
            // pengecekan apakan pada draft jabatan ini sudah ada pejabatnya / belum
            $cek = RiwJabatan::where('manjab_draft_id',$request->input('manjab_draft_id'))
            ->where('master_unit_id',$request->input('master_unit_id'))
            ->first();

            $paramsRiwJabatan = [
                'id'=>$this->uuid(),
                'is_draft'=>1,
                'pegawai_id'=>$request->input('pegawai_id'),
                'master_unit_id'=>$request->input('master_unit_id'),
                'master_unit_tree_id'=>$request->input('master_unit_tree_id'),
                'master_unit_name'=>$request->input('master_unit_name'),
                'kelas_jabatan'=>$request->input('kelas_jabatan'),
                'nama_jabatan' => $request->input('nama_jabatan'),
                'manjab_draft_id'=>$request->input('manjab_draft_id'),
                'master_jenis_jabatan_id'=>$request->input('master_jenis_jabatan_id'),
            ];

            // jika ada pejabat pada unit
            if($cek){
                // pengecekan data yang sudah ada (edit) apakah pejabatnya sama
                if($cek->pegawai_id == $paramsRiwJabatan['pegawai_id']){
                    // jika sama tinggal update datanya saja
                    $cek = $cek->update($paramsRiwJabatan);
                }
                else{
                    // jika data tidak sama, maka hapus dulu data riwayatnya kemudian insert riw jabatan baru
                    $cek->forceDelete();
                    $new_rj = RiwJabatan::create($paramsRiwJabatan);
                }
            }
            else{
                // insert ke riwayat jabatan, masih berupa draft
                $rj = RiwJabatan::create($paramsRiwJabatan);
            }
            

            
            // update pegawai_id pejabat pada unit

            $uptManjab = ManjabUnit::where('id', $paramsRiwJabatan['master_unit_id'])->first();


            $uptManjab->pegawai_id = $paramsRiwJabatan['pegawai_id'];
            $uptManjab->save();
    
            DB::commit();
            $response = new Response(200,'Berhasil di simpan',[],[],true);

            return $response->getResponse();

        }catch(\Exception $e){
             DB::rollback();
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function simpanDraftManjab(Request $request){
        try{
            $params = [
                'id'=>$this->uuid(),
                'no_sk'=>$request->input('no_sk'),
            ];

            $data = ManjabDraft::create($params);
            $response = new Response(200,'Berhasil di dapat',['id'=>$params['id']],[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

     public function releaseDraftManjab(Request $request){
        DB::beginTransaction();
        try{

            // cek no dokumen sebelumnya
            $cek = ManjabDraft::where('is_new',1)->first();
            if($cek){
                $cek->end_date = Carbon::now();
                $cek->save();
            }

            // reset is new manjab
            $data =  ManjabDraft::query()->update(['is_new'=>null]);

            $draft = ManjabDraft::where('id',$request->input('manjab_draft_id'))->first();
            $draft->no_sk = $request->input('no_sk');
            $draft->release_date = Carbon::now();
            $draft->is_new = 1;
            $draft->save();

            // reset tabel master unit
            MasterUnit::query()->forceDelete();

            // insert master unit date released terbaru
            DB::select(" 
                INSERT INTO master_unit 
                    select
                        mu.*
                    from
                        manjab_draft md
                    left join manjab_unit mu on mu.manjab_draft_id = md.id
                    where is_new = 1 mu.deleted_at is null
            ");

            DB::commit();
            $response = new Response(200,'Berhasil di simpan',[],[],true);
             
            return $response->getResponse();

        }catch(\Exception $e){
            DB::rollback();
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function releaseDraftJabatan(Request $request){
        DB::beginTransaction();
        try{
    
            // set null flag jabatan terbaru pegawai yang akan jadi pejabat
            $riw_jabatan = DB::select("
                        update
                            riw_jabatan
                        set
                            is_new = null
                        where
                            pegawai_id in (
                            select
                                pegawai_id
                            from
                                riw_jabatan rj
                            where
                                is_draft = 1
                            and manjab_draft_id = ?
                        )
            ",[$request->input('manjab_draft_id')]);
            // 

            // set no_sk , tgl_sk dan update flag jabatan terbaru;
            $new_jab = RiwJabatan::where('is_draft',1)
            ->where('manjab_draft_id',$request->input('manjab_draft_id'))
            ->update([
                'is_draft' => null,
                'is_new' => 1,
                'tgl_sk' => $request->input('release_date'),
                'tgl_mulai' => $request->input('release_date'),
                'no_sk' => $request->input('no_sk'),

            ]); 

            // update_pegawai
            $upd_pegawai = DB::select("
                update
                    pegawai p
                left join riw_jabatan rj on
                    rj.pegawai_id = p.id
                    and rj.is_new = 1
                set
                    p.last_jabatan_id = rj.id ,
                    p.nama_unit = rj.master_unit_name,
                    p.nama_jabatan = rj.nama_jabatan ,
                    p.master_jenis_jabatan_id = rj.master_jenis_jabatan_id,
                    p.kelas_jabatan = rj.kelas_jabatan, 
                    p.master_unit_id = rj.master_unit_id,
                    p.master_unit_tree_id = rj.master_unit_tree_id, 
                    p.last_manjab_draft_id = rj.manjab_draft_id
                where
                    rj.manjab_draft_id = ?
                ",[$request->input('manjab_draft_id')]);

            // update pejabat pada master_unit
            $upd_masterUnit = MasterUnit::select("
                    update 
                        master_unit mu
                    inner join manjab_unit mu2 on
                        mu2.id = mu.id
                        and mu.manjab_draft_id = mu.manjab_draft_id
                    set mu.pegawai_id = mu2.pegawai_id  
                    where
                        mu.manjab_draft_id = ?
                ",[$request->input('manjab_draft_id')]);

            // update manjab draft

            $uptManjab = ManjabDraft::where('id',$request->input('manjab_draft_id'))->first();
            $uptManjab->release_date_jabatan = Carbon::now();
            $uptManjab->no_dokumen_jabatan = $request->input('no_sk');
            $uptManjab->save();

             DB::commit();
            $response = new Response(200,'Berhasil di simpan',[],[],true); 
            return $response->getResponse();
           
        }
        catch(\Exception $e){
            DB::rollback();
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }

    }

    public function getManjabStruktural(Request $request){
        try{

            $manjab_draft_id = $request->input('manjab_draft_id');
            $data =  ManjabUnit::where('manjab_unit.manjab_draft_id',$manjab_draft_id)
            ->selectRaw('manjab_unit.id,tree_id, pegawai.nama_lengkap,manjab_unit.manjab_draft_id,manjab_unit.nama_jabatan as name,have_child, manjab_unit.kelas_jabatan, parent_id, unit as unit_name,riw_jabatan.master_jenis_jabatan_id, riw_jabatan.pegawai_id')
            ->leftJoin('riw_jabatan', function($query){
                $query->on('riw_jabatan.manjab_draft_id','manjab_unit.manjab_draft_id')->on('riw_jabatan.master_unit_id', 'manjab_unit.id')
                ->whereNull('riw_jabatan.deleted_at');
                // ->where('is_draft',1);
            })
            ->leftJoin('pegawai','pegawai.id','riw_jabatan.pegawai_id')
            ->orderBy('tree_id','asc');

            $tree_id = $request->input('tree_id');

            if($request->input('tree_id')){
                $data = $data->whereRaw(" length(tree_id) - length(replace(tree_id,'.','')) = length(?) - length(replace(?,'.','')) + 1 ",[$tree_id,$tree_id])
                ->where('tree_id','like',$tree_id.'%');
            }
            else{
                $data = $data->whereRaw("length(tree_id) - length(replace(tree_id,'.','')) = 1");
            }

            $data = $data->get();

             foreach ($data as $key => $value) {
                    if($value['have_child'] == 1){
                        $data[$key]['children'] = [];
                    }
                }
            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function getDraftUnit(Request $request){
        try{
            $manjab_draft_id = $request->input('manjab_draft_id');
            $data =  ManjabUnit::where('manjab_draft_id',$manjab_draft_id)
            ->selectRaw('id,tree_id, unit as name,have_child, kelas_jabatan, parent_id')
            ->orderBy('tree_id','asc');

            $tree_id = $request->input('tree_id');

            if($request->input('tree_id')){
                $data = $data->whereRaw(" length(tree_id) - length(replace(tree_id,'.','')) = length(?) - length(replace(?,'.','')) + 1 ",[$tree_id,$tree_id])
                ->where('tree_id','like',$tree_id.'%');
            }
            else{
                $data = $data->whereRaw("length(tree_id) - length(replace(tree_id,'.','')) = 1");
            }

            $data = $data->get();

             foreach ($data as $key => $value) {
                    if($value['have_child'] == 1){
                        $data[$key]['children'] = [];
                    }
                }
            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function getDraftManjab(Request $request){
        try{

            $data =  ManjabDraft::selectRaw("id, no_sk,date_format(release_date,'%d/%m/%Y') as release_date,is_new,no_dokumen_jabatan,date_format(release_date_jabatan,'%d/%m/%Y') as release_date_jabatan ");

            if($request->input('is_new')){
                $data = $data->where('is_new',1)->first();
            }
            else{
                $data = $data->get();
            }

            
            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }
    public function getBank(){
        try{
            $data = DB::table('tblbank')->get();
            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function getMasterKabupaten(Request $request){
        try{
            $data = Kabupaten::where('province_id',$request->input('provinsi_id'))->get();
            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function getMasterKecamatan(Request $request){
        try{
            $data = Kecamatan::where('regency_id',$request->input('kabupaten_id'))->get();
            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function getMasterDesa(Request $request){
        try{
            $data = Desa::where('district_id',$request->input('desa_id'))->get();
            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }


    public function getUniversitas(Request $request){
        try{
            $data = Universitas::all();
            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

	public function getAgama(Request $request){
        try{
        	$agama = Agama::all();
        	$response = new Response(200,'Berhasil di dapat',$agama,[]);

        	return $response->getResponse();

        }catch(\Exception $e){
        	$response = new Response(200,'Ada kesalahan server',[],$e->getMessage());
            return $response->getResponse();
        }
        
    }

    public function getHubungan(Request $request){
        try{
        	$hubungan = Hubungan::all();
        	$response = new Response(200,'Berhasil di dapat',$hubungan,[]);

        	return $response->getResponse();

        }catch(\Exception $e){
        	$response = new Response(200,'Ada kesalahan server',[],$e->getMessage());
            return $response->getResponse();
        }
        
    }
}

?>