<?php

namespace App\Http\Controllers;

use App\Models\{PersuratanSurat};
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Webklex\PDFMerger\Facades\PDFMergerFacade as PDFMerger;
use App\Models\MutasiDraft;
use App\Models\MutasiList;
use PDF;

class PDFController extends Controller
{
    //
    public function index2()
    {
        //menampilkan halaman laporan
        return view('viewpdf');
    }

    // public function index(Request $request)
    // {
    //     $data = PersuratanSurat::with(['mengetahui', 'penerima', 'pengirim', 'drafter', 'unit', 'disposisi'])
    //         ->selectRaw('*, date(created_at) as _date ')
    //         ->where('id', $request->input('id'))->first();
    //     $data->tgl_indo = $this->tgl_indo($data->_date);
    //     $params = [
    //         'id' => $data->id,
    //         'flag' => "surat"
    //     ];
    //     $generateQrcode = Config('app.URL_VALIDATE_SIGN_FE') . '/' . base64_encode(json_encode($params));
    //     $qrcode = base64_encode(QrCode::format('png')->size(200)->errorCorrection('H')->merge('https://inkamultisolusi.co.id/images/starter-logo.jpg', .4, true)->generate($generateQrcode));

    //     // return view('laporan_pdf', ['data' => $data, 'qrcode'=>$qrcode]);
    //     $direktori = 'persuratan/';
    //     $pdf = PDF::loadview('laporan_pdf', ['data' => $data, 'qrcode' => $qrcode]);
    //     $pdf->save("persuratan/" . date('YmdHis') . '_' . str_replace('/', '_', $data->nomor_surat) . '.pdf');

    //     if ($data->file_surat_generated && file_exists($direktori . $data->file_surat_generated)) {
    //         $oMerger = PDFMerger::init();
    //         $oMerger->addPDF("persuratan/" . str_replace('/', '_', $data->nomor_surat) . '.pdf', 'all');
    //         $oMerger->addPDF("persuratan/" . $data->file_surat_generated, 'all');

    //         $oMerger->merge();
    //         return $oMerger->stream();
    //     } else {
    //         return $pdf->stream();
    //     }
    // }

    public function index(Request $request)
    {
        $data = PersuratanSurat::with(['mengetahui', 'penerima', 'pengirim', 'drafter', 'unit', 'disposisi'])
            ->selectRaw('*, date(created_at) as _date ')
            ->where('id', $request->input('id'))->first();

        if ($data) {
            $data->tgl_indo = $this->tgl_indo($data->_date);
            $params = [
                'id' => $data->id,
                'flag' => "surat"
            ];
            $generateQrcode = Config('app.URL_VALIDATE_SIGN_FE') . '/' . base64_encode(json_encode($params));
            $qrcode = base64_encode(QrCode::size(200)->errorCorrection('H')->merge('https://inkamultisolusi.co.id/images/starter-logo.jpg', .4, true)->generate($generateQrcode));

            $direktori = 'persuratan/';
            $pdf = PDF::loadView('laporan_pdf', ['data' => $data, 'qrcode' => $qrcode]);
            $pdf->save("persuratan/" . date('YmdHis') . '_' . str_replace('/', '_', $data->nomor_surat) . '.pdf');

            if ($data->file_surat_generated && file_exists($direktori . $data->file_surat_generated)) {
                $oMerger = PDFMerger::init();
                $oMerger->addPDF("persuratan/" . str_replace('/', '_', $data->nomor_surat) . '.pdf', 'all');
                $oMerger->addPDF("persuratan/" . $data->file_surat_generated, 'all');

                $oMerger->merge();
                return $oMerger->stream();
            } else {
                return $pdf->stream();
            }
        } else {
            // Handle the case where $data is null
            return response()->json(['message' => 'Data not found.'], 404);
        }
    }

    public function cetakDisposisi(Request $request)
    {
        $path = public_path('/images/ims.png');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

        $pdf = PDF::loadView('cetak_disposisi', ['sppd' => [], 'logo' => $base64]);
        return $pdf->stream();
    }

    public function cetakMutasi(Request $request)
    {
        $data = MutasiDraft::selectRaw('*, date(release_date) as _date ')
            ->where('id', $request->input('id'))
            ->first();

        if ($data) {

            $data->tgl_indo = $this->tgl_indo($data->_date);
            $params = [
                'id' => $data->id,
                'flag' => "mutasi"
            ];
            $generateQrcode = Config('app.URL_VALIDATE_SIGN_FE') . '/' . base64_encode(json_encode($params));
            $qrcode = base64_encode(QrCode::size(200)->errorCorrection('H')->merge('https://inkamultisolusi.co.id/images/starter-logo.jpg', .4, true)->generate($generateQrcode));

            $direktori = 'mutasi/';
            $pdf = PDF::loadView('cetak_mutasi', ['data' => $data, 'qrcode' => $qrcode]);
            $pdf->save("mutasi/" . date('YmdHis') . '_' . str_replace('/', '_', $data->no_sk) . '.pdf');

            if ($data->release_date && file_exists($direktori . $data->release_date)) {
                $oMerger = PDFMerger::init();
                $oMerger->addPDF("mutasi/" . str_replace('/', '_', $data->no_sk) . '.pdf', 'all');
                $oMerger->addPDF("mutasi/" . $data->release_date, 'all');

                $oMerger->merge();
                return $oMerger->stream();
            } else {
                return $pdf->stream();
            }
        } else {
            // Handle the case where $data is null
            return response()->json(['message' => 'Data not found.'], 404);
        }
    }
}
