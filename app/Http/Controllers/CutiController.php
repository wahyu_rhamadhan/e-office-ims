<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GlobalClass\Response;
use App\Models\TblEmployee;
use App\Models\SaldoCuti;
use App\Models\RiwCuti;
use App\Models\Notifikasi;
use Carbon\Carbon;
use App\Services\FtpService;
use Illuminate\Support\Facades\DB;
use App\Exports\CutiExport;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpWord\TemplateProcessor;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class CutiController extends Controller
{

    public function cetakCuti(Request $request){
        $data = RiwCuti::with('detailPegawai.unit')->selectRaw("riw_cuti.*, m_cuti.jenis,ROW_NUMBER() OVER (partition by pegawai_id order by created_at) nomor,
                case when status_cuti_id = 1 then 'Disetujui Atasan 1' when status_cuti_id = 3 then 'Disetujui Atasan 2' when status_cuti_id = 2 then 'Ditolak Atasan 1' when status_cuti_id = 4 then 'Ditolak Atasan 2' else 'Awaiting' end status_cuti,date(created_at) as created_at_,
                    atasan1.empname as nama_atasan, atasan2.empname as nama_atasan2,
                    DATE_FORMAT(tgl_mulai,'%Y-%m-%d') tgl_mulai_,   DATE_FORMAT(tgl_selesai,'%Y-%m-%d') tgl_selesai_, tblemployee.empname as nama, TblEmployee.empcode as nip, pos1.posname as posname1,pos2.posname as posname2, date(tgl_mulai) as start_date, date(tgl_selesai) as end_date,concat(coalesce(durasi, DATEDIFF(date(tgl_selesai), date(tgl_mulai)) + 1), ' Hari') as durasi
            ")
            ->leftjoin( 'tblemployee', 'tblemployee.pegawai_id', '=', 'riw_cuti.pegawai_id' )
            ->leftjoin('m_cuti','m_cuti.id','riw_cuti.jenis_cuti_id')
            ->leftjoin(DB::raw('tblemployee as atasan1'),'atasan1.pegawai_id','riw_cuti.atasan_id')
            ->leftjoin(DB::raw('tblposition as pos1'),'pos1.poscode','atasan1.poscode')
            ->leftjoin(DB::raw('tblemployee as atasan2'),'atasan2.pegawai_id','riw_cuti.atasan2_id')
            ->leftjoin(DB::raw('tblposition as pos2'),'pos2.poscode','atasan2.poscode')
            ->where('riw_cuti.id',$request->input('id'))->first();

        
        $rules = $data->jenis_cuti_id.'_'.$data->status_cuti_id;
        $isExistAtasan2 = $data->atasan2_id ? true : false;
   
     
       $templateCuti = $this->_generateTemplateCuti($rules, $isExistAtasan2);
       $templateProcessor = new TemplateProcessor(public_path('template/'.$templateCuti));
       

       // generate qr code
       $nama = $data->detailPegawai->empname;
       $nama = explode(' ', $nama);
       

       $url = config('app.FE_URL').'cuti/'.$data->id.'?pegawai_id='.$data->pegawai_id.'&flag=u&signed='.base64_encode($data->created_at);
       $namaGenerate = $url;

       // generate qr code untuk pengaju
       QrCode::format('png')->size(1000)->generate($namaGenerate, public_path('qrcode/'.$data->pegawai_id.'.png'));

       $templateProcessor->setImageValue('ttd_user', array('path' => public_path('qrcode/'.$data->pegawai_id.'.png'), 'width' => 50, 'height' => 50, 'ratio' => false));



       if($data->status_cuti_id){
           $urlAtasan1 = config('app.FE_URL').'cuti/'.$data->id.'?pegawai_id='.$data->atasan_id.'&flag=a1&signed='.base64_encode($data->atasan_approved_date);

           // generate qr code untuk pengaju
           QrCode::format('png')->size(1000)->generate($urlAtasan1, public_path('qrcode/'.$data->atasan_id.'.png'));

           $urlAtasan2 = config('app.FE_URL').'cuti/'.$data->id.'?pegawai_id='.$data->atasan2_id.'&flag=a2&signed='.base64_encode($data->atasan2_approved_date);

           // generate qr code untuk pengaju
           QrCode::format('png')->size(1000)->generate($urlAtasan2, public_path('qrcode/'.$data->atasan2_id.'.png'));
       }


       switch ($data->status_cuti_id) {
           // distejui atasan langsung
           case '1':
              $templateProcessor->setImageValue('ttd_atasan1', array('path' => public_path('qrcode/'.$data->atasan_id.'.png'), 'width' => 50, 'height' => 50, 'ratio' => false));
               $templateProcessor->setValue('ttd_atasan2', '');
               break;
           case '2':
               $templateProcessor->setImageValue('ttd_atasan1', array('path' => public_path('qrcode/'.$data->atasan_id.'.png'), 'width' => 50, 'height' => 50, 'ratio' => false));
               $templateProcessor->setValue('ttd_atasan2', '');
               break;
            case '3':
                $templateProcessor->setImageValue('ttd_atasan1', array('path' => public_path('qrcode/'.$data->atasan_id.'.png'), 'width' => 50, 'height' => 50, 'ratio' => false));
                 $templateProcessor->setImageValue('ttd_atasan2', array('path' => public_path('qrcode/'.$data->atasan2_id.'.png'), 'width' => 50, 'height' => 50, 'ratio' => false));
               break;
            case '4':
               $templateProcessor->setImageValue('ttd_atasan1', array('path' => public_path('qrcode/'.$data->atasan_id.'.png'), 'width' => 50, 'height' => 50, 'ratio' => false));
                 $templateProcessor->setImageValue('ttd_atasan2', array('path' => public_path('qrcode/'.$data->atasan2_id.'.png'), 'width' => 50, 'height' => 50, 'ratio' => false));
               break;
           default:
               $templateProcessor->setValue('ttd_atasan1', '');
               $templateProcessor->setValue('ttd_atasan2', '');
               break;
       }

      
        
        $start_date = tgl_indo($data->start_date);
        $end_date = tgl_indo($data->end_date);
        $tgl_cuti = $start_date == $end_date ? $start_date : $start_date.' s/d '.$end_date;
       $values = [
            'nama'=>$data->detailPegawai->empname,
            'durasi'=>$data->durasi,
            'nip'=>$data->detailPegawai->empcode,
            'unit'=>$data->detailPegawai->unit->nama,
            'jabatan'=>$data->detailPegawai->posname,
            'tgl_created'=>tgl_indo($data->created_at_),
            'keperluan'=>$data->keperluan,
            'tgl_cuti'=>$tgl_cuti,
            'hp'=>$data->no_hp,
            'alamat'=>$data->alamat_cuti,
            'nama_atasan'=>$data->nama_atasan,
            'nama_jabatan'=>$data->posname1,
            'nama_atasan2'=>$data->nama_atasan2,
            'nama_jabatan2'=>$data->posname2,
       ];

       $templateProcessor->setValues($values);
       $templateProcessor->saveAs(public_path('word/cuti.docx'));
       return response()->download(public_path('word/cuti.docx'));
       
    }


    public function validateSignCuti(Request $request){
         try{
            $riwCuti = RiwCuti::find($request->input('id'));
            $signed = base64_decode($request->input('signed'));
            $flag = $request->input('flag');
            $pegawai = TblEmployee::selectRaw('empname,empcode')->where('pegawai_id',$request->input('pegawai_id'))->first();
             $data = [
                    'nama'=>$pegawai ? $pegawai->empname.' - '.$pegawai->empcode : '-',
                    'signedDate'=> $signed
                ];
            if($flag=='u'){
                $status= $riwCuti->pegawai_id == $request->input('pegawai_id') && $riwCuti->created_at == $signed ? true : false;
               
            }
            elseif ($flag=='a1') {
                 $status= $riwCuti->atasan_id == $request->input('pegawai_id') && $riwCuti->atasan_approved_date == $signed ? true : false;
            }
             elseif ($flag=='a2') {
                 $status= $riwCuti->atasan2_id == $request->input('pegawai_id') && $riwCuti->atasan2_approved_date == $signed ? true : false;
            }
            $response = new Response(200,'Berhasil di dapat',['valid'=>$status,'data'=>$data],[],true);
            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }


    /**
     *  $rules = 1_1
     *  $rules = "id_cuti"_"status_cuti"
     *
     *  Jenis Cuti
     *  ID Nama Cuti
     *  1 Cuti Tahunan
     *  2 Cuti Besar
     *  3 Cuti Sakit
     *  4 Cuti Dispensasi
     *  5 Cuti Melahirkan / Gugur Kandungan
     *  6 Cuti Penting
     *  7 Cuti Ibadah
     *  8 Cuti Ijin Diri
     *  9 Cuti DT Perusahaan
     *
     * Status Cuti
     * ID STATUS CUTI
     * 1 Disetuji Atasan 1 / Langsung
     * 2 Ditolak Atasan Langsung
     * 3 Disetujui Atasan 2
     * 4 Ditolak Atasan 2
     *
     *
     */
    protected function _generateTemplateCuti($rules, $isExistAtasan2){
        // jika tdk ada atasan 2
        if(!$isExistAtasan2){    
            switch ($rules) {
                case '1_':
                    $template = 'CUTI_TAHUNAN/CUTI_TAHUNAN_DRAFT_ATASAN1';
                    break;
                case '1_1':
                    $template = 'CUTI_TAHUNAN/CUTI_TAHUNAN_APROVE_ATASAN1';
                    break;
                case '1_2':
                    $template = 'CUTI_TAHUNAN/CUTI_TAHUNAN_DECLINE_ATASAN1';
                    break;
                case '2_':
                    $template = 'CUTI_BESAR/CUTI_DRAFT_ATASAN1';
                    break;
                case '2_1':
                    $template = 'CUTI_BESAR/CUTI_APROVE_ATASAN1';
                    break;
                case '2_2':
                    $template = 'CUTI_BESAR/CUTI_DECLINE_ATASAN1';
                    break;
                case '3_':
                    $template = 'CUTI_SAKIT/CUTI_DRAFT_ATASAN1';
                    break;
                case '3_1':
                    $template = 'CUTI_SAKIT/CUTI_APROVE_ATASAN1';
                    break;
                case '3_2':
                    $template = 'CUTI_SAKIT/CUTI_DECLINE_ATASAN1';
                    break;
                case '4_':
                    $template = 'CUTI_DISPENSASI/CUTI_DRAFT_ATASAN1';
                    break;
                case '4_1':
                    $template = 'CUTI_DISPENSASI/CUTI_APROVE_ATASAN1';
                    break;
                case '4_2':
                    $template = 'CUTI_DISPENSASI/CUTI_DECLINE_ATASAN1';
                    break;
                 case '5_':
                    $template = 'CUTI_MELAHIRKAN/CUTI_DRAFT_ATASAN1';
                    break;
                case '5_1':
                    $template = 'CUTI_MELAHIRKAN/CUTI_APROVE_ATASAN1';
                    break;
                case '5_2':
                    $template = 'CUTI_MELAHIRKAN/CUTI_DECLINE_ATASAN1';
                    break;
                case '6_':
                    $template = 'CUTI_PENTING/CUTI_DRAFT_ATASAN1';
                    break;
                case '6_1':
                    $template = 'CUTI_PENTING/CUTI_APROVE_ATASAN1';
                    break;
                case '6_2':
                    $template = 'CUTI_PENTING/CUTI_DECLINE_ATASAN1';
                    break;
                case '7_':
                    $template = 'CUTI_IBADAH/CUTI_DRAFT_ATASAN1';
                    break;
                case '7_1':
                    $template = 'CUTI_IBADAH/CUTI_APROVE_ATASAN1';
                    break;
                case '7_2':
                    $template = 'CUTI_IBADAH/CUTI_DECLINE_ATASAN1';
                    break;
                case '8_':
                    $template = 'CUTI_IJINDIRI/CUTI_DRAFT_ATASAN1';
                    break;
                case '8_1':
                    $template = 'CUTI_IJINDIRI/CUTI_APROVE_ATASAN1';
                    break;
                case '8_2':
                    $template = 'CUTI_IJINDIRI/CUTI_DECLINE_ATASAN1';
                    break;
                case '9_':
                    $template = 'CUTI_DT/CUTI_DRAFT_ATASAN1';
                    break;
                case '9_1':
                    $template = 'CUTI_DT/CUTI_APROVE_ATASAN1';
                    break;
                case '9_2':
                    $template = 'CUTI_DT/CUTI_DECLINE_ATASAN1';
                    break;
                default:
                    # code...
                    break;
            }
        }
        else{
            // jika approval cuti hanya atasan langsung
             switch ($rules) {
                case '1_':
                    $template = 'CUTI_TAHUNAN/CUTI_TAHUNAN_DRAFT_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '1_1':
                    $template = 'CUTI_TAHUNAN/CUTI_TAHUNAN_APROVE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '1_2':
                    $template = 'CUTI_TAHUNAN/CUTI_TAHUNAN_DECLINE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '1_3':
                    $template = 'CUTI_TAHUNAN/CUTI_TAHUNAN_APROVE_ATASAN1_APROVE_ATASAN2';
                    break;
                case '1_4':
                    $template = 'CUTI_TAHUNAN/CUTI_TAHUNAN_APROVE_ATASAN1_DECLINE_ATASAN2';
                    break;
                case '2_':
                    $template = 'CUTI_BESAR/CUTI_DRAFT_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '2_1':
                    $template = 'CUTI_BESAR/CUTI_APROVE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '2_2':
                    $template = 'CUTI_BESAR/CUTI_DECLINE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '2_3':
                    $template = 'CUTI_BESAR/CUTI_APROVE_ATASAN1_APROVE_ATASAN2';
                    break;
                case '2_4':
                    $template = 'CUTI_BESAR/CUTI_APROVE_ATASAN1_DECLINE_ATASAN2';
                    break;
                case '3_':
                    $template = 'CUTI_SAKIT/CUTI_DRAFT_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '3_1':
                    $template = 'CUTI_SAKIT/CUTI_APROVE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '3_2':
                    $template = 'CUTI_SAKIT/CUTI_DECLINE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '3_3':
                    $template = 'CUTI_SAKIT/CUTI_APROVE_ATASAN1_APROVE_ATASAN2';
                    break;
                case '3_4':
                    $template = 'CUTI_SAKIT/CUTI_APROVE_ATASAN1_DECLINE_ATASAN2';
                    break;
                case '4_':
                    $template = 'CUTI_DISPENSASI/CUTI_DRAFT_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '4_1':
                    $template = 'CUTI_DISPENSASI/CUTI_APROVE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '4_2':
                    $template = 'CUTI_DISPENSASI/CUTI_DECLINE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '4_3':
                    $template = 'CUTI_DISPENSASI/CUTI_APROVE_ATASAN1_APROVE_ATASAN2';
                    break;
                case '4_4':
                    $template = 'CUTI_DISPENSASI/CUTI_APROVE_ATASAN1_DECLINE_ATASAN2';
                    break;
                case '5_':
                    $template = 'CUTI_MELAHIRKAN/CUTI_DRAFT_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '5_1':
                    $template = 'CUTI_MELAHIRKAN/CUTI_APROVE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '5_2':
                    $template = 'CUTI_MELAHIRKAN/CUTI_DECLINE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '5_3':
                    $template = 'CUTI_MELAHIRKAN/CUTI_APROVE_ATASAN1_APROVE_ATASAN2';
                    break;
                case '5_4':
                    $template = 'CUTI_MELAHIRKAN/CUTI_APROVE_ATASAN1_DECLINE_ATASAN2';
                    break;
                case '6_':
                    $template = 'CUTI_PENTING/CUTI_DRAFT_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '6_1':
                    $template = 'CUTI_PENTING/CUTI_APROVE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '6_2':
                    $template = 'CUTI_PENTING/CUTI_DECLINE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '6_3':
                    $template = 'CUTI_PENTING/CUTI_APROVE_ATASAN1_APROVE_ATASAN2';
                    break;
                case '6_4':
                    $template = 'CUTI_PENTING/CUTI_APROVE_ATASAN1_DECLINE_ATASAN2';
                    break;
                case '7_':
                    $template = 'CUTI_IBADAH/CUTI_DRAFT_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '7_1':
                    $template = 'CUTI_IBADAH/CUTI_APROVE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '7_2':
                    $template = 'CUTI_IBADAH/CUTI_DECLINE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '7_3':
                    $template = 'CUTI_IBADAH/CUTI_APROVE_ATASAN1_APROVE_ATASAN2';
                    break;
                case '7_4':
                    $template = 'CUTI_IBADAH/CUTI_APROVE_ATASAN1_DECLINE_ATASAN2';
                    break;
                case '8_':
                    $template = 'CUTI_IJINDIRI/CUTI_DRAFT_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '8_1':
                    $template = 'CUTI_IJINDIRI/CUTI_APROVE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '8_2':
                    $template = 'CUTI_IJINDIRI/CUTI_DECLINE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '8_3':
                    $template = 'CUTI_IJINDIRI/CUTI_APROVE_ATASAN1_APROVE_ATASAN2';
                    break;
                case '8_4':
                    $template = 'CUTI_IJINDIRI/CUTI_APROVE_ATASAN1_DECLINE_ATASAN2';
                    break;
                 case '9_':
                    $template = 'CUTI_DT/CUTI_DRAFT_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '9_1':
                    $template = 'CUTI_DT/CUTI_APROVE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '9_2':
                    $template = 'CUTI_DT/CUTI_DECLINE_ATASAN1_DRAFT_ATASAN2';
                    break;
                case '9_3':
                    $template = 'CUTI_DT/CUTI_APROVE_ATASAN1_APROVE_ATASAN2';
                    break;
                case '9_4':
                    $template = 'CUTI_DT/CUTI_APROVE_ATASAN1_DECLINE_ATASAN2';
                    break;
                default:
                    # code...
                    break;
            }
        }


        return $template.'.docx';
    }

    protected function _getData($params, $export = false){
        $page = $params->input('is_mobile') ? config('app.MOBILE_PER_PAGE') : config('app.MOBILE_PER_PAGE');
           $data = RiwCuti::selectRaw("riw_cuti.*, m_cuti.jenis,ROW_NUMBER() OVER (partition by pegawai_id order by created_at desc) nomor,
                case when status_cuti_id = 1 then 'Disetujui Atasan 1' when status_cuti_id = 3 then 'Disetujui Atasan 2' when status_cuti_id = 2 then 'Ditolak Atasan 1' when status_cuti_id = 4 then 'Ditolak Atasan 2' else 'Awaiting' end status_cuti, case when tgl_mulai = tgl_selesai then  DATE_FORMAT(tgl_mulai,'%d/%m/%Y') else concat(DATE_FORMAT(tgl_mulai,'%d/%m/%Y'),' s.d. ', DATE_FORMAT(tgl_selesai,'%d/%m/%Y')) end as tgl_cuti,
                    concat(atasan1.empname, ' - ', atasan1.empcode) as nama_atasan, concat(atasan2.empname, ' - ', atasan2.empcode) as nama_atasan2,
                    DATE_FORMAT(tgl_mulai,'%Y-%m-%d') tgl_mulai_,   DATE_FORMAT(tgl_selesai,'%Y-%m-%d') tgl_selesai_, tblemployee.empname as nama, TblEmployee.empcode as nip
            ")
            ->leftjoin( 'tblemployee', 'tblemployee.pegawai_id', '=', 'riw_cuti.pegawai_id' )
            ->leftjoin('m_cuti','m_cuti.id','riw_cuti.jenis_cuti_id')
            ->leftjoin(DB::raw('tblemployee as atasan1'),'atasan1.pegawai_id','riw_cuti.atasan_id')
            ->leftjoin(DB::raw('tblemployee as atasan2'),'atasan2.pegawai_id','riw_cuti.atasan2_id');

            if ( $params->jenis_cuti_id ) {
                $data = $data->where( 'riw_cuti.jenis_cuti_id', '=', $params->jenis_cuti_id );
            }

            if ( $params->status_cuti_id ) {
                $data = $data->where( 'riw_cuti.status_cuti_id', '=', $params->status_cuti_id );
            }
            if ( $params->tgl_mulai ) {
                $data = $data->where( 'riw_cuti.tgl_mulai', '=',  $params->tgl_mulai );
            }
            if ( $params->tgl_selesai ) {
                $data = $data->whereBetween( 'riw_cuti.tgl_selesai', [ $params->tgl_selesai ] );
            }

             // filter by pegawai id
            if ( $params->pegawai_id ) {
                $data = $data->where( 'riw_cuti.pegawai_id', '=',  $params->pegawai_id );
            }

            if ( $params->filled( 'id' ) ) {
                $data = $data->where( 'riw_cuti.id', $params->id )->first();
            } else {

                $data = $export ? $data->get() : $data->paginate($page);
            }

            return $data;
    }

    public function getPegawaiSaldoCuti(Request $request){
        try{
              $params = [
                  'tahun'=>$request->input('tahun'),
                  'unit_id'=>$request->input('unit_id')
                ];
             $data = TblEmployee::with('unit')
            ->selectRaw("empcode as nip, empname as nama, unit_tree_id, tblposition.posname, emp_status.status, tblemployee.pegawai_id, saldo_cuti.saldo, false as is_editing")
            ->leftJoin('tblposition', 'tblposition.poscode', 'TblEmployee.poscode')
            ->leftJoin('emp_status','emp_status.kode','TblEmployee.employmentStatus')
            ->leftjoin('saldo_cuti', function($join) use ($params)  {
                $join->on('tblemployee.pegawai_id','saldo_cuti.pegawai_id')
                ->where('tahun',$params['tahun']);
            })
            ->whereNull('resigndt')
            ->orderBy(DB::raw(' case when positionStatusCode is null then "PST005" else positionStatusCode end '),'asc');

            if($params['unit_id']){
                $data = $data->where('tblemployee.unit_tree_id','like',$params['unit_id'].'%');
            }

            $data = $data->paginate(config('app.WEB_PER_PAGE'));
            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function getStatusSaldoCuti(Request $request){
        try{
           
            $data = DB::table('lock_saldo_cuti')->where('tahun', $request->input('tahun'))->first();
            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

	public function getCuti(Request $request, $export = false){
        try{
           
            $data = $this->_getData($request);
            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function cutiByPegawaiId(Request $request){
        try{

            $data = $this->_getData($request);

            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

     public function export(Request $request){
        try{
            $data = $this->_getData($request, true);
            return Excel::download(new CutiExport($data), 'export.xlsx');
        }
        catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
     }

    public function approval(Request $request){
        DB::beginTransaction();
        try{
           $params = [
            'id'=>$request->input('id'),
            'status_cuti_id'=>$request->input( 'status_cuti_id' ),
            'catatan_penolakan'=>$request->input('catatan_penolakan'),
            'atasan_id'=>$request->input('atasan_id'),
            'atasan2_id'=>$request->input('atasan2_id'),
        ];

            $rcuti = RiwCuti::find($params['id']);
            $rcuti->status_cuti_id = $params[ 'status_cuti_id' ];
            $rcuti->catatan_penolakan = $params[ 'catatan_penolakan'];

            if($rcuti->jenis_cuti_id == 1 || $rcuti->jenis_cuti_id == 2){
                $rcuti->atasan_approved_date = Carbon::now();
            }

            if($rcuti->jenis_cuti_id == 3 || $rcuti->jenis_cuti_id == 4){
                $rcuti->atasan2_approved_date = Carbon::now();
            }
            $rcuti->update();


            // jika cuti tahunan ditolak atasan 1 | atasan 2 akan mengembalikan saldo cuti

            if($rcuti->jenis_cuti_id == 1 && ($params['status_cuti_id'] == 2 || $params['status_cuti_id'] == 4)){
                 $thCuti = substr($rcuti->created_at,0,4);
                 $saldo = SaldoCuti::where('tahun',$thCuti)->where('pegawai_id',$rcuti->pegawai_id)->first();
                 $saldo->saldo = $saldo->saldo + $rcuti->durasi;
                 $saldo->save();
             
            }

            $masterCuti = DB::table('m_cuti')->where('id',$rcuti->jenis_cuti_id)->first();
            $judulNotif = 'Pengajuan '.$masterCuti->jenis;

            // jika ada atasan 2 kirim notifikasi ke atasan 2
            if($params['atasan2_id'] && $params['atasan2_id'] != null && $params['status_cuti_id'] != 3 && $params['status_cuti_id'] != 4 ){
               
                
                $pegawai = TblEmployee::selectRaw('empname, empcode')->where('pegawai_id',$rcuti->pegawai_id)->first();
                $keterangan = 'Pengajuan Atas Nama '.$pegawai->empname.' - '.$pegawai->empcode;
                $paramsNotifAtasan2 = [
                    'judul'=>$judulNotif,
                    'from_pegawai_id'=>$rcuti->pegawai_id,
                    'to_pegawai_id'=>$params['atasan2_id'],
                    'riwayat'=>'riw_cuti',
                    'riwayat_id'=>$params['id'],
                    'keterangan'=>$keterangan
                ];

                $this->sendNotifikasi($paramsNotifAtasan2);
            }

            // notif ke pegawai
                if($params['status_cuti_id'] == 1 || $params['status_cuti_id'] == 2){
                    $from_atasan = $params['atasan_id'];
                }
                else{
                    $from_atasan = $params['atasan2_id'];
                }


                switch ($params['status_cuti_id']) {
                    case '1':
                        $status = 'Disetujui Atasan 1';
                        break;
                    case '2':
                        $status = 'Ditolak Atasan 1';
                        break;
                    case '3':
                        $status = 'Disetujui Atasan 2';
                        break;
                    case '4':
                        $status = 'Ditolak Atasan 2';
                        break;
                    default:
                        # code...
                        break;
                }
                $pegawai = TblEmployee::selectRaw('empname, empcode')->where('pegawai_id',$rcuti->pegawai_id)->first();
                $keterangan = 'Pengajuan '.$masterCuti->jenis.' '.$status;
                $paramsNotif = [
                    'judul'=>$judulNotif,
                    'from_pegawai_id'=>$from_atasan,
                    'to_pegawai_id'=>$rcuti->pegawai_id,
                    'riwayat'=>'riw_izin',
                    'riwayat_id'=>$rcuti->id,
                    'keterangan'=>$keterangan
                ];
                 $this->sendNotifikasi($paramsNotif);
             DB::commit();
            $response = new Response(200,'Berhasil di simpan',[],[],true);

            return $response->getResponse();
        }
        catch(\Exception $e){
            DB::rollback();
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
        
    }

    public function getSaldoCuti(Request $request){
        try{

           $currentYear = date('Y');
           $pegawai_id = $request->input('pegawai_id');
           $saldo = SaldoCuti::selectRaw(' coalesce(saldo,0) saldo ')->where('tahun',$currentYear)->where('pegawai_id',$pegawai_id)->first();
           
            $response = new Response(200,'Berhasil di dapat',$saldo,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function updateSaldoCuti(Request $request){
        try{
            $cek = SaldoCuti::where('tahun',$request->input('tahun'))->where('pegawai_id',$request->input('pegawai_id'))->first();

            if($cek){
                // update
                $cek->saldo = $request->input('saldo');
                $cek->save();
            }
            else{
                // insert
                SaldoCuti::create([
                    'pegawai_id'=>$request->input('pegawai_id'),
                    'tahun'=>$request->input('tahun'),
                    'saldo'=>$request->input('saldo')
                ]);
            }

            $response = new Response(200,'Berhasil di simpan',[],[],true);
            return $response->getResponse();
        }
        catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function delCuti(Request $request){
         DB::beginTransaction();
        try{
            $id = $request->input( 'id' );
            $riw = RiwCuti::where( 'id', '=', $id )->first();
            $riw->deleted_at = Carbon::now();
            $riw->save();


            $notif = Notifikasi::where('riwayat_id','=',$id)->first();
            $notif->deleted_at = Carbon::now();
            $notif->save();
            $response = new Response(200,'Berhasil di hapus',[],[],true);
             DB::commit();
            return $response->getResponse();

        }catch(\Exception $e){
             DB::rollback();
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

     public function updateStatusSaldoCuti(Request $request){
        try{
            $cek =   $data = DB::table('lock_saldo_cuti')->where('tahun', $request->input('tahun'))->first();

            if($cek){
                // update
                $cek->is_lock = 1;
                $cek->save();
            }
            else{
                // insert
                 DB::table('lock_saldo_cuti')->insert([
                    'tahun'=>$request->input('tahun'),
                    'is_lock'=>1
                 ]);
            }

            $response = new Response(200,'Berhasil di simpan',[],[],true);
            return $response->getResponse();
        }
        catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }
    public function simpanCuti(Request $request){
    	DB::beginTransaction();
        try{
             $params = [
             	'id'=>$this->uuid(),
	            'jenis_cuti_id'=>$request->input('jenis_cuti_id'),
	            'atasan_id'=>$request->input( 'atasan_id' ),
	            'atasan2_id'=>$request->input( 'atasan2_id' ),
	            'pegawai_id'=>$request->input( 'pegawai_id' ),
	            'tgl_mulai'=>$request->input( 'tgl_mulai' ),
	            'tgl_selesai'=>$request->input( 'tgl_selesai' ),
	            'alamat_cuti'=>$request->input( 'alamat_cuti' ),
	            'no_hp'=>$request->input( 'no_hp' ),
	            'keperluan'=>$request->input( 'keperluan' ),
            // 'file_cuti'=>$request->file( 'file_cuti' ),
        	];


        	// jika cuti tahunan
        	if($params['jenis_cuti_id'] == 1){
        		$durasi = 0;
        		$earlier = new \DateTime(date("Y-m-d", strtotime($params['tgl_mulai']) ));
				$later = new \DateTime(date("Y-m-d", strtotime($params['tgl_selesai']) ));
				
				$abs_diff = $later->diff($earlier)->format("%a"); //3

				for($i = $earlier; $i <= $later; $i->modify('+1 day')){
				   !isWeekend( $i->format("Y-m-d")) ? $durasi ++ : null;
				}
				$params['durasi'] = $durasi;

                // cek Saldo Cuti
                 $saldo = SaldoCuti::selectRaw(' coalesce(saldo,0) saldo ')->where('tahun',date('Y'))->where('pegawai_id',$params['pegawai_id'])->first();

                
                 if($durasi > $saldo->saldo){
                      $response = new Response(412,'Saldo Cuti Tidak Cukup',[],[],false);
                      return $response->getResponse();
                 }

                 // update saldo
                 $newSaldo = $saldo->saldo - $durasi;
                 SaldoCuti::where('tahun',date('Y'))->where('pegawai_id',$params['pegawai_id'])->first()->update(['saldo'=>$newSaldo]);
        	}
        	
        	// jika ada file upload
        	if($request->hasFile('file')){
        		$direktori = config('app.FTP_FOLDER').'ess/cuti/';
        		$upload = uploadFileFtp($request->file('file'), $direktori);
           		if(!$upload['status']){
					$response = new ResponseCreator(500, $upload['message'], [], $upload);
					return $response->getResponse();
				}
					$params['path'] = $direktori;
					$params['file_original'] = $upload['file_asli']; 
					$params['file_generated'] = $upload['file_generated']; 
           	}

    
			

        	$atasan = TblEmployee::where('empcode',$params['atasan_id'])->first();
        	if($atasan){
        		$params['atasan_id'] = $atasan->pegawai_id;
        	}
        	$atasan2 = TblEmployee::where('empcode',$params['atasan2_id'])->first();
        	if($atasan2){
        		$params['atasan2_id'] = $atasan2->pegawai_id;
        	}

        	$riwCuti = RiwCuti::create($params);

        	// sendNotifikasi
        	$masterCuti = DB::table('m_cuti')->where('id',$params['jenis_cuti_id'])->first();
            $judul = 'Pengajuan Cuti '.$masterCuti->jenis;
            $pegawai = TblEmployee::selectRaw('empname, empcode')->where('pegawai_id',$params['pegawai_id'])->first();
            $keterangan = 'Pengajuan Atas Nama '.$pegawai->empname.' - '.$pegawai->empcode;

            if($params['atasan_id']){
            	$paramsNotifAtasan = [
	                'judul'=>$judul,
	                'from_pegawai_id'=>$params['pegawai_id'],
	                'to_pegawai_id'=>$params['atasan_id'],
	                'riwayat'=>'riw_cuti',
	                'riwayat_id'=>$params['id'],
	                'keterangan'=>$keterangan
	            ];

	            $this->sendNotifikasi($paramsNotifAtasan);
            }


            // if($params['atasan2_id'] && $params['atasan2_id'] != 'null'){
            // 	$paramsNotifAtasan2 = [
	           //      'judul'=>$judul,
	           //      'from_pegawai_id'=>$params['pegawai_id'],
	           //      'to_pegawai_id'=>$params['atasan2_id'],
	           //      'riwayat'=>'riw_cuti',
	           //      'riwayat_id'=>$params['id'],
	           //      'keterangan'=>$keterangan
	           //  ];

	           //  $this->sendNotifikasi($paramsNotifAtasan2);
            // }
        	

            $response = new Response(200,'Berhasil di simpan',[],[],true);
            DB::commit();
            return $response->getResponse();

        }catch(\Exception $e){
        	 DB::rollback();
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

     public function getApproval(Request $request){
        try{
             $params = [
                'pegawai_id'=>$request->input('pegawai_id'),
                'is_mobile'=>$request->input('is_mobile'),
            ];

            $page = $params['is_mobile'] ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');
            $data = RiwCuti::with('detailPegawai.unit')->selectRaw("riw_cuti.*, m_cuti.jenis,ROW_NUMBER() OVER (order by created_at desc) nomor,
                case when status_cuti_id = 1 then 'Disetujui Atasan 1' when status_cuti_id = 3 then 'Disetujui Atasan 2' when status_cuti_id = 2 then 'Ditolak Atasan 1' when status_cuti_id = 4 then 'Ditolak Atasan 2' else 'Awaiting' end status_cuti,date(created_at) as created_at_,
                    atasan1.empname as nama_atasan, atasan2.empname as nama_atasan2,
                    DATE_FORMAT(tgl_mulai,'%Y-%m-%d') tgl_mulai_,   DATE_FORMAT(tgl_selesai,'%Y-%m-%d') tgl_selesai_, tblemployee.empname as nama, TblEmployee.empcode as nip, pos1.posname as posname1,pos2.posname as posname2, date(tgl_mulai) as start_date, date(tgl_selesai) as end_date,concat(coalesce(durasi, DATEDIFF(date(tgl_selesai), date(tgl_mulai)) + 1), ' Hari') as durasi
            ")
            ->leftjoin( 'tblemployee', 'tblemployee.pegawai_id', '=', 'riw_cuti.pegawai_id' )
            ->leftjoin('m_cuti','m_cuti.id','riw_cuti.jenis_cuti_id')
            ->leftjoin(DB::raw('tblemployee as atasan1'),'atasan1.pegawai_id','riw_cuti.atasan_id')
            ->leftjoin(DB::raw('tblposition as pos1'),'pos1.poscode','atasan1.poscode')
            ->leftjoin(DB::raw('tblemployee as atasan2'),'atasan2.pegawai_id','riw_cuti.atasan2_id')
            ->leftjoin(DB::raw('tblposition as pos2'),'pos2.poscode','atasan2.poscode')
            ->whereRaw(' riw_cuti.atasan_id = ? or (riw_cuti.atasan2_id = ? and riw_cuti.status_cuti_id in (1,3,4) ) ', [$params['pegawai_id'],$params['pegawai_id']]);


            $data = $data->paginate($page);
            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(200,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }
}

?>