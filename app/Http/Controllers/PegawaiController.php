<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GlobalClass\Response;
use App\Models\{Pegawai, RiwJabatan, RiwPkwt, RiwPkwtt, RiwPerbantuan, MasterUnit};
use App\Models\{PersuratanSurat};
use App\Models\Notifikasi;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PegawaiController extends Controller
{

    public function tes()
    {
        $fields['include_external_user_ids'] = ['3a242032-9705-11ed-a8fc-0242ac120002'];
        $fields['contents'] = array(
            "en" => 'Pengajuan atas nama Dendi Madisanto - 652100045',
        );
        $fields['headings'] = array(
            "en" => 'Pengajuan Izin Dinas'
        );

        \OneSignal::sendPush($fields);
    }

    protected function _mobilePegawaiFilter($query, $request)
    {

        if ($request->input('master_unit_tree_id') && $request->input('last_manjab_draft_id')) {

            $query = $query->whereRaw('master_unit_tree_id like ?', [$request->input('master_unit_tree_id') . '%'])
                ->where('last_manjab_draft_id', $request->input('last_manjab_draft_id'));
        }

        $query = $query->where('id', '<>', $request->input('pegawai_id'));

        return $query;
    }

    public function getAllApproval(Request $request)
    {
        try {

            $params = [
                'pegawai_id' => $request->input('pegawai_id'),
                'is_mobile' => $request->input('is_mobile'),

            ];

            $page = $params['is_mobile'] ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');
            $data = DB::table('vw_list_approval_atasan')
                ->where('atasan_id', $params['pegawai_id'])
                ->selectRaw("vw_list_approval_atasan.*, pegawai.nama_lengkap as nama_pengaju, pegawai.nip as nip_pengaju")
                ->leftJoin('pegawai', 'pegawai.id', 'vw_list_approval_atasan.pengaju_id')
                ->orderBy('created_at', 'desc')
                ->paginate($page);

            $response = new Response(200, 'Berhasil di dapat', $data, [], true);
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
        }

        return $response->getResponse();
    }


    public function getPegawaiMobile(Request $request)
    {
        try {
            $params = [
                'search' => $request->input('search'),
            ];

            $page = config('app.MOBILE_PER_PAGE');

            $data = Pegawai::selectRaw("nama_lengkap, nama_jabatan, nama_unit, id as pegawai_id,nip,master_golongan_id,kelas_jabatan,master_status_pegawai_id, master_unit_tree_id, last_manjab_draft_id")
                ->whereNull('tgl_selesai')
                ->orderByRaw("case when kelas_jabatan is null then 'XXX' else kelas_jabatan end asc, master_unit_id asc");

            if ($request->input('flag_filter')) {

                switch ($request->input('flag_filter')) {
                    case 'izin':
                        $data = $this->_mobilePegawaiFilter($data, $request);

                        break;

                    default:
                        # code...
                        break;
                }
            }



            if ($params['search']) {
                $data = $data->whereRaw(' (pegawai.nama_lengkap like ? or pegawai.nama_jabatan like ? or pegawai.nip like ?) ', ['%' . $params['search'] . '%', '%' . $params['search'] . '%', '%' . $params['search'] . '%']);
            }


            $data = $data->paginate($page);


            $result = $data->toArray();

            $pegawai = [];
            foreach ($result['data'] as $key => $value) {
                # code...
                $pegawai[$key] = $value;
                $pegawai[$key]['photo'] = 'https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png';
            }

            $result['data'] = $pegawai;

            $response = new Response(200, 'Berhasil di dapat', $result, [], true);
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
        }

        return $response->getResponse();
    }


    public function getPegawai(Request $request)
    {
        try {
            $params = [
                'search' => $request->input('search'),
                'unit_id' => $request->input('unit_id'),
                'is_mobile' => $request->input('is_mobile'),
                'unit_id_divisi_in' => $request->input('unit_id_divisi_in'),
                'position_status_in' => $request->input('position_status_in'),
                'pegawai_id_not_in' => $request->input('pegawai_id_not_in'),
                'alfabet' => $request->input('alfabet')
            ];

            $page = $params['is_mobile'] ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');

            $data = Pegawai::selectRaw("pegawai.*, nama_lengkap as nama, master_unit_tree_id as unit_tree_id, DATE_FORMAT(tgl_lahir ,'%d/%m/%Y') as tgl_lahir, pegawai.nama_jabatan as posname, master_status_pegawai.status, pegawai.id, pegawai.kelas_jabatan as position_status_code,
                case when tgl_selesai is null then 'Aktif' else 'Tidak Aktif' end ket, master_unit.unit, case when pejabat.id is not null then 1 else null end is_pejabat")
                ->leftJoin('master_status_pegawai', 'master_status_pegawai.id', 'pegawai.master_status_pegawai_id')
                ->leftJoin('master_unit', 'master_unit.id', 'pegawai.master_unit_id')
                ->leftJoin(DB::raw('master_unit as pejabat'), function ($query) use ($params) {
                    $query->on('pejabat.pegawai_id', 'pegawai.id')
                        ->on('pejabat.id', 'pegawai.master_unit_id')
                        ->where('pejabat.tree_id', $params['unit_id']);
                })
                ->whereNull('tgl_selesai')
                ->orderBy(DB::raw('case when master_unit.pegawai_id = pegawai.id then 1 else 99 end, master_unit.tree_id asc,  case when pegawai.kelas_jabatan is null then "PST005" else pegawai.kelas_jabatan end '), 'asc');

            if ($params['alfabet']) {
                $data = $data->where('pegawai.nama_lengkap', 'like', $params['alfabet'] . '%');
            }

            if ($params['unit_id']) {
                $data = $data->where('pegawai.master_unit_tree_id', 'like', $params['unit_id'] . '%');
            }

            if ($params['unit_id_divisi_in']) {
                $explode = explode('.', $params['unit_id_divisi_in']);
                $id = $explode[0] . '.' . $explode[1] . '.';
                $data = $data->where('pgawai.master_unit_tree_id', 'like', $id . '%');
            }

            if ($params['pegawai_id_not_in']) {
                $val = base64_decode($params['pegawai_id_not_in']);
                $data = $data->whereNotIn('pegawai.pegawai_id', json_decode($val));
            }

            if ($params['position_status_in']) {
                $val = base64_decode($params['position_status_in']);
                $data = $data->whereIn('pegawai.kelas_jabatan', json_decode($val));
            }

            if ($params['search']) {
                $data = $data->whereRaw(' (pegawai.nama_jabatan like ? or pegawai.nip like ?) ', ['%' . $params['search'] . '%', '%' . $params['search'] . '%']);
            }


            $data = $data->paginate($page);

            $response = new Response(200, 'Berhasil di dapat', $data, [], true);
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
        }

        return $response->getResponse();
    }

    public function getDetailPegawai(Request $request)
    {
        try {
            $params = [
                'pegawai_id' => $request->input('pegawai_id'),
            ];

            $data = TblEmployee::where('pegawai_id', $params['pegawai_id'])->first();

            $response = new Response(200, 'Berhasil di dapat', $data, [], true);
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
        }

        return $response->getResponse();
    }

    public function deleteAllNotifikasi(Request $request)
    {
        try {

            $data = Notifikasi::where('to_pegawai_id', $request->input('pegawai_id'))->delete();
            $response = new Response(200, 'Berhasil di hapus', [], [], true);
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
        }
        return $response->getResponse();
    }

    public function getNotifikasi(Request $request)
    {
        try {

            $data = Notifikasi::where('to_pegawai_id', $request->input('pegawai_id'))->whereNull('is_read')->count();
            $response = new Response(200, 'Berhasil di dapat', $data, [], true);
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
        }
        return $response->getResponse();
    }

    public function getNotifikasiDetail(Request $request)
    {
        try {
            $page = $request->input('is_mobile') ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');
            $data = Notifikasi::where('to_pegawai_id', $request->input('pegawai_id'))->orderBy('created_at', 'desc')->paginate($page);
            $response = new Response(200, 'Berhasil di dapat', $data, [], true);
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
        }
        return $response->getResponse();
    }

    public function updateStatusNotifikasi(Request $request)
    {
        try {
            $id = $request->input('id');
            $data = Notifikasi::where('id', $id)->first();
            $data->is_read = 1;
            $data->save();

            $response = new Response(200, 'Berhasil di dapat', [], [], true);
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
        }
        return $response->getResponse();
    }

    public function updatePegawai(Request $request)
    {
        try {
            $params = [
                'nama_lengkap' => $request->input('nama_lengkap'),
                'nik' => $request->input('nik'),
                'nip' => $request->input('nip'),
                'kk' => $request->input('no_kk'),
                'bpjs_kes' => $request->input('bpjs_kes'),
                'bpjs_kej' => $request->input('bpjs_kej'),
                'tempat_lahir' => $request->input('tempat_lahir'),
                'tgl_lahir' => $request->input('tgl_lahir'),
                'hp' => $request->input('hp'),
                'agama_id' => $request->input('agama_id'),
                'jenis_kelamin' => $request->input('jenis_kelamin'),
                'status_kawin_id' => $request->input('status_kawin_id'),
                'bank_code' => $request->input('bank_code'),
                'tgl_join' => $request->input('tgl_join'),
                'ukuran_baju' => $request->input('ukuran_baju'),
                'ukuran_sepatu' => $request->input('ukuran_sepatu'),
                'alamat_ktp' => $request->input('alamat_ktp'),
                'alamat_domisili' => $request->input('alamat_domisili'),
                'nip' => $request->input('nip'),
                'email' => $request->input('email'),
                'no_rekening' => $request->input('no_rekening')
            ];

            $response = new Response(200, 'Berhasil di simpan', [], [], true);
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
        }

        return $response->getResponse();
    }

    public function simpanPegawai(Request $request)
    {
        DB::beginTransaction();
        try {

            $id_pegawai = $this->uuid();
            $id_jabatan = $this->uuid();

            // insert pegawai

            $peg = Pegawai::create([
                'id' => $id_pegawai,
                'nama_lengkap' => $request->input('nama_lengkap'),
                'nik' => $request->input('nik'),
                'nip' => $request->input('nip'),
                'kk' => $request->input('no_kk'),
                'bpjs_kes' => $request->input('bpjs_kes'),
                'bpjs_kej' => $request->input('bpjs_kej'),
                'tempat_lahir' => $request->input('tempat_lahir'),
                'tgl_lahir' => $request->input('tgl_lahir'),
                'hp' => $request->input('hp'),
                'agama_id' => $request->input('agama_id'),
                'jenis_kelamin' => $request->input('jenis_kelamin'),
                'status_kawin_id' => $request->input('status_kawin_id'),
                'bank_code' => $request->input('bank_code'),
                'master_status_pegawai_id' => $request->input('master_status_pegawai_id'),
                'tgl_join' => $request->input('tgl_join'),
                'ukuran_baju' => $request->input('ukuran_baju'),
                'ukuran_sepatu' => $request->input('ukuran_sepatu'),
                'alamat_ktp' => $request->input('alamat_ktp'),
                'alamat_domisili' => $request->input('alamat_domisili'),
                'nip' => $request->input('nip'),
                'last_jabatan_id' => $id_jabatan,
                'kelas_jabatan' => $request->input('kelas_jabatan'),
                'email' => $request->input('email'),
                'no_rekening' => $request->input('no_rekening'),
                'master_unit_id' => $request->input('master_unit_id'),
                'master_unit_tree_id' => $request->input('master_unit_tree_id'),
                'nama_jabatan' => $request->input('nama_jabatan'),
                'nama_unit' => $request->input('nama_unit'),
                'last_manjab_draft_id' => $request->input('last_manjab_draft_id')

            ]);


            // input ke riw jabatan
            $paramsJabatan = [
                'id' => $id_jabatan,
                'pegawai_id' => $id_pegawai,
                'master_unit_id' => $request->input('master_unit_id'),
                'master_unit_tree_id' => $request->input('master_unit_tree_id'),
                'master_unit_name' => $request->input('nama_unit'),
                'manjab_draft_id' => $request->input('last_manjab_draft_id'),
                'tgl_sk' => Carbon::now(),
                'tgl_mulai' => Carbon::now(),
                'kelas_jabatan' => $request->input('kelas_jabatan'),
                'is_new' => 1,
                'nama_jabatan' => $request->input('nama_jabatan'),
                'master_jabatan_id' => $request->input('master_jabatan_id')
            ];

            // create riw jabatan
            $rj = Riwjabatan::create($paramsJabatan);

            // input ke riwayat pegawai
            switch ($request->input('master_status_pegawai_id')) {
                    // riw pkwt
                case '65':
                    $rpkwt = RiwPkwt::create([
                        'id' => $this->uuid(),
                        'pegawai_id' => $id_pegawai,
                        'tgl_join' => $request->input('tgl_join'),
                        'nip' => $request->input('nip'),
                        'master_status_pegawai_id' => $request->input('master_status_pegawai_id'),
                        'is_new' => 1
                    ]);
                    break;
                case '87':
                    $rpkwt = RiwPkwt::create([
                        'id' => $this->uuid(),
                        'pegawai_id' => $id_pegawai,
                        'tgl_capeg' => $request->input('tgl_join'),
                        'nip' => $request->input('nip'),
                        'master_status_pegawai_id' => $request->input('master_status_pegawai_id'),
                        'is_new' => 1
                    ]);
                    break;
                case '98':
                    $rpkwtt = RiwPkwtt::create([
                        'id' => $this->uuid(),
                        'pegawai_id' => $id_pegawai,
                        'tgl_peg' => $request->input('tgl_join'),
                        'nip' => $request->input('nip'),
                        'master_status_pegawai_id' => $request->input('master_status_pegawai_id'),
                        'is_new' => 1
                    ]);
                    break;
                default:
                    // selain di atas/ perbantuan
                    $rperbantuan = RiwPerbantuan::create([
                        'id' => $this->uuid(),
                        'pegawai_id' => $id_pegawai,
                        'tgl_peg' => $request->input('tgl_join'),
                        'nip' => $request->input('nip'),
                        'master_status_pegawai_id' => $request->input('master_status_pegawai_id'),
                        'is_new' => 1
                    ]);
                    break;
            }




            DB::commit();
            $response = new Response(200, 'Berhasil di simpan', ['pegawai_id' => $id_pegawai], [], true);
        } catch (\Exception $e) {
            DB::rollback();
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
        }
        return $response->getResponse();
    }

    public function getPegawaiStrukturalMobile(Request $request)
    {
        try {
            $params = [
                'search' => $request->input('search'),
            ];


            $page = config('app.MOBILE_PER_PAGE');

            $data = Pegawai::selectRaw("nama_lengkap, nama_jabatan, nama_unit, id as pegawai_id,nip")
                ->whereIn('pegawai.kelas_jabatan', ['PST000', 'PST001', 'PST002', 'PST0003'])
                ->whereNull('tgl_selesai')
                ->orderByRaw("case when kelas_jabatan is null then 'XXX' else kelas_jabatan end asc, master_unit_id asc");



            if ($params['search']) {
                $data = $data->whereRaw(' (pegawai.nama_lengkap like ? or pegawai.nama_jabatan like ? or pegawai.nip like ?) ', ['%' . $params['search'] . '%', '%' . $params['search'] . '%', '%' . $params['search'] . '%']);
            }


            $data = $data->paginate($page);


            $result = $data->toArray();

            $pegawai = [];
            foreach ($result['data'] as $key => $value) {
                # code...
                $pegawai[$key] = $value;
                $pegawai[$key]['photo'] = 'https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png';
            }

            $result['data'] = $pegawai;

            $response = new Response(200, 'Berhasil di dapat', $result, [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getPegawaiStruktural(Request $request)
    {
        try {
            $params = [
                'search' => $request->input('search'),
            ];


            $page = config('app.MOBILE_PER_PAGE');

            $data = Pegawai::selectRaw("nama_lengkap, nama_jabatan, nama_unit, id as pegawai_id,nip")
                ->whereIn('pegawai.kelas_jabatan', ['PST000', 'PST001', 'PST002', 'PST0003'])
                ->whereNull('tgl_selesai')
                ->orderByRaw("case when kelas_jabatan is null then 'XXX' else kelas_jabatan end asc, master_unit_id asc");



            if ($params['search']) {
                $data = $data->whereRaw(' (pegawai.nama_lengkap like ? or pegawai.nama_jabatan like ? or pegawai.nip like ?) ', ['%' . $params['search'] . '%', '%' . $params['search'] . '%', '%' . $params['search'] . '%']);
            }


            $data = $data->paginate($page);


            $result = $data->toArray();

            $pegawai = [];
            foreach ($result['data'] as $key => $value) {
                # code...
                $pegawai[$key] = $value;
                $pegawai[$key]['photo'] = 'https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png';
            }

            $result['data'] = $pegawai;

            $response = new Response(200, 'Berhasil di dapat', $result, [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    protected function _recursiveGetStruktural($params)
    {
        $explode = explode('.', $params['master_unit_tree_id']);
        $id = '';
        $iterate = (count($explode) - 1) - 1 == 0 ? 1 : (count($explode) - 1) - 1;
        $data = [];
        $filter = ['PST001', 'PST002', 'PST003'];
        if ($params['filter_jabatan']) {
            $filter = explode(',', $params['filter_jabatan']);
        }
        $id = '';
        for ($i = 0; $i <= $iterate; $i++) {
            $id .= $explode[$i] . '.';
            $pegawai = MasterUnit::selectRaw("master_unit.*, nama_lengkap, pegawai.nama_jabatan, nama_unit, pegawai.id as pegawai_id,nip, pegawai.master_unit_tree_id, pegawai.last_manjab_draft_id,pegawai.nama_unit")
                ->where('tree_id', $id)
                ->where('manjab_draft_id', $params['last_manjab_draft_id'])
                ->where('pegawai_id', '<>', $params['pegawai_id'])
                ->leftJoin('pegawai', 'pegawai.id', 'master_unit.pegawai_id')
                ->first();

            if ($pegawai) {
                $pegawai->photo = 'https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png';
                if (in_array($pegawai->kelas_jabatan, $filter)) {
                    $data[] = $pegawai;
                }
            }
        }

        return $data;
    }

    public function digiSignInfo(Request $request)
    {
        try {
            $decode = base64_decode($request->params);
            $params = json_decode($decode);

            if ($params->flag == "surat") {
                $data = PersuratanSurat::selectRaw(" id,date_format(persuratan_surat.created_at,'%d/%m/%Y') tgl_dokumen, nomor_surat as nomor_dokumen, perihal ")
                    ->with(['pengirim', 'penerima', 'mengetahui', 'unit'])
                    ->with('disposisi.detail')
                    ->where('id', $params->id)
                    ->first();
            }
            $response = new Response(200, 'Berhasil di dapat', $data, [], true);
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
        }
        return $response->getResponse();
    }

    public function getPegawaiStrukturalSpecial(Request $request)
    {

        $params = [
            'master_unit_tree_id' => $request->master_unit_tree_id,
            'last_manjab_draft_id' => $request->last_manjab_draft_id,
            'pegawai_id' => $request->pegawai_id,
            'filter_jabatan' => $request->filter_jabatan
        ];
        $atasan = $this->_recursiveGetStruktural($params);

        $result['data'] = $atasan;
        $response = new Response(200, 'Berhasil di dapat', $result, [], true);

        return $response->getResponse();
    }

    public function masterStatusPegawai(Request $request)
    {
        try {

            $data = DB::table('emp_status')->get();
            $response = new Response(200, 'Berhasil di dapat', $data, [], true);
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
        }
        return $response->getResponse();
    }

    public function getPegawaiById(Request $request)
    {
        try {
            $params = [
                'pegawai_id' => $request->input('pegawai_id')
            ];

            $data = Pegawai::selectRaw("pegawai.*, nama_lengkap as nama, master_unit_tree_id as unit_tree_id, DATE_FORMAT(tgl_lahir ,'%d/%m/%Y') as tgl_lahir, pegawai.nama_jabatan as posname, master_status_pegawai.status, pegawai.id, pegawai.kelas_jabatan as position_status_code,
                case when tgl_selesai is null then 'Aktif' else 'Tidak Aktif' end ket, master_unit.unit, date(tgl_lahir) as tgl_lahir,date(tgl_join) as tgl_join,date(tgl_selesai) as tgl_selesai")
                ->leftJoin('master_status_pegawai', 'master_status_pegawai.id', 'pegawai.master_status_pegawai_id')
                ->leftJoin('master_unit', 'master_unit.id', 'pegawai.master_unit_id')
                ->where('pegawai.id', $params['pegawai_id'])
                ->first();

            // $data->atasan_langsung = $this->atasanLangsung(['pegawai_id'=>$data->pegawai_id,'position_status'=>$data->PositionStatusCode, 'unit_id'=>$data->unit_tree_id]);

            // $data->full_unit = $this->getFullUnit($data->unit_tree_id);
            $response = new Response(200, 'Berhasil di dapat', $data, [], true);
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
        }
        return $response->getResponse();
    }
}
