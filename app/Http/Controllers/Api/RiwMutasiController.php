<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RiwMutasiRequest;
use App\Models\RiwMutasi;

class RiwMutasiController extends Controller
{
    //
    public function get($id=null)
    {
    	if (isset($id)) {
            $riwmutasi = RiwMutasi::findOrFail($id);
            return response()->json(['msg' => 'Data retrieved', 'data' => $riwmutasi], 200);
        } else {
            $riwmutasis = RiwMutasi::get();
            return response()->json(['msg' => 'Data retrieved', 'data' => $riwmutasis], 200);
        }
    }
    public function store(RiwMutasiRequest $request)
    {
    	$riwmutasi = RiwMutasi::create($request->all());
    	return response()->json(['msg' => 'data received', 'data'=>$riwmutasi], 200);
    }
    public function update( $id, RiwMutasiRequest $request)
    {

       

        if (RiwMutasi::where('id', $id)->exists()) {
        $riwmutasi = RiwMutasi::find($id);
        $riwmutasi->pegawai_id = is_null($request->pegawai_id) ? $riwmutasi->pegawai_id : $request->pegawai_id;
        $riwmutasi->unit_name_prev = is_null($request->unit_name_prev) ? $riwmutasi->unit_name_prev : $request->unit_name_prev;
        $riwmutasi->unit_tree_id_prev = is_null($request->unit_tree_id_prev) ? $riwmutasi->unit_tree_id_prev : $request->unit_tree_id_prev;
        $riwmutasi->unit_manjab_draft_id_prev = is_null($request->unit_manjab_draft_id_prev) ? $riwmutasi->unit_manjab_draft_id_prev : $request->unit_manjab_draft_id_prev;
        $riwmutasi->unit_name_aft = is_null($request->unit_name_aft) ? $riwmutasi->unit_name_aft : $request->unit_name_aft;
        $riwmutasi->unit_tree_id_aft = is_null($request->unit_tree_id_aft) ? $riwmutasi->unit_tree_id_aft : $request->unit_tree_id_aft;
        $riwmutasi->unit_manjab_draft_id_aft = is_null($request->unit_manjab_draft_id_aft) ? $riwmutasi->unit_manjab_draft_id_aft: $request->unit_manjab_draft_id_aft;
        $riwmutasi->no_sk = is_null($request->no_sk) ? $riwmutasi->no_sk : $request->no_sk;
        $riwmutasi->tgl_mulai = is_null($request->tgl_mulai) ? $riwmutasi->tgl_mulai : $request->tgl_mulai;
        $riwmutasi->save();

        return response()->json([
            "message" => "records updated successfully"
        ], 200);
        } else {
        return response()->json([
            "message" => "RiwMutasi not found"
        ], 404);
        
    }


    }
  
    function destroy($id)
    {
        $riwmutasi = RiwMutasi::findOrFail($id);
        $riwmutasi->delete();
        return response()->json(['msg' => 'Data deleted'], 200);
    }
    
}
