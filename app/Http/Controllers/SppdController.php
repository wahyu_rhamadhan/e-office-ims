<?php

namespace App\Http\Controllers;

use App\GlobalClass\Response;
use App\Models\Notifikasi;
use App\Models\Pegawai;
use App\Models\RiwIzin;

use App\Models\{ Sppd, SppdParticipant, WorkflowStage, WorkflowStagePrerequisite, WorkflowTransaction };
use Illuminate\Http\Request;
// use Barryvdh\DomPDF\PDF;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
// use Barryvdh\DomPDF\Facade as PDF;

class SppdController extends Controller
{
    //
    public function index(Request $request, Sppd $sppd)
    {
        $data = Sppd::all();
        // $request = ['sppd'];
        return view('view_sppd', ['data' => $data]);
        // return dd($data);

    }

    public function getTahapan(Request $request)
    {
        try{

            $result = WorkflowTransaction::where('transaction_id', $request->input('transaction_id'))
            ->leftJoin('workflow_stages','workflow_stages.id','workflow_transactions.workflow_stage_id')
            ->orderBy('sequence','asc')
            ->selectRaw("workflow_transactions.*, workflow_stages.name as stage_name, DATE_FORMAT(workflow_transactions.approved_at, '%d/%m/%Y') as tgl, DATE_FORMAT(workflow_transactions.approved_at, '%H:%i:%s') as time ")
            ->whereNull("workflow_stages.only_notif")
            ->get();


            $response = new Response(200, 'Berhasil di dapat', $result, [], true);
            return $response->getResponse();
        }
        catch(\Exception $e){
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }

    }

    public function getMasterLumpsump()
    {
        $data = DB::table('lumsump')->get();
        $response = new Response(200, 'Berhasil Di Dapat', $data, [], false);
        return $response->getResponse();
    }

    public function indexSppd(Request $request, Sppd $sppd)
    {
        $data = Sppd::all();
        // $request = ['sppd'];
        return view('view_sppd', ['data' => $data]);
        // return dd($data);
    }

    public function export(Request $request)
    {
        $data = Sppd::all();
        // $request = ['sppd'];
        return view('export_sppd', ['data' => $data]);
        // return dd($data);
    }

    protected function _cek($pegawai, $condition)
    {
        switch ($condition->operator) {
            case '=':
                $value = $pegawai->{$condition->prerequisite} == $condition->min_value ? 1 : 0;
                break;
             case 'IN':
                $value = in_array($pegawai->{$condition->prerequisite} , explode(',', $condition->min_value)) ? 1 : 0;
                break;
            default:
                # code...
                break;
        }

        return $value;
    }

    protected function _generateWhereDb($pegawai, $condition)
    {
        switch ($condition->operator) {
            case '=':
                $value = " AND '" . $pegawai->{$condition->prerequisite} . "' = " . $condition->min_value;
                break;

            default:
                # code...
                break;
        }

        return $value;
    }

    protected function _generateWhereDb2($condition)
    {
        switch ($condition->operator) {
            case '=':
                $value = " AND " . $condition->prerequisite . " = '" . $condition->min_value."'";
                break;
            case 'IN':
                $value = " AND " . $condition->prerequisite . " IN ('" . implode( "', '", explode(',', $condition->min_value))."')";
                break;
            default:
                # code...
                break;
        }

        return $value;
    }

    protected function _getNextWorkflow($currentSequence, $transaction_id){
        $data = WorkflowTransaction::where('transaction_id', $transaction_id)
                ->where('workflow_transactions.sequence', (int)$currentSequence + 1)
                ->leftJoin('workflow_stages','workflow_stages.id', 'workflow_transactions.workflow_stage_id')
                ->selectRaw('workflow_transactions.*, workflow_stages.only_notif')
                ->first();

        if($data){
            $sppd = Sppd::where('id',$transaction_id)->first();
        
            $workflow_stage_prerequisites = WorkflowStagePrerequisite::where('workflow_stage_id',$data->workflow_stage_id)->get();
            $where = ' 1=1 ';

            // generate where 
            foreach ($workflow_stage_prerequisites as $key => $value) {
                $where.=$this->_generateWhereDb2($value);
            }

            $pengaju = DB::table('vw_pegawai')->where('pegawai_id',$sppd->pegawai_id)->first(); 
            $pgw = DB::table('vw_pegawai')->whereRaw($where)->get();

            // notif pegawai 
            foreach ($pgw as $key => $value) {
                $judul = 'Bussiness Trip Application Letter';
                $keterangan = 'Bussiness Trip Application Letter '.$sppd->no_doc.' From '.$pengaju->nama_lengkap.' - '.$pengaju->nip;
                $paramsNotif = [
                    'judul'=>$judul,
                    'from_pegawai_id'=>$sppd->pegawai_id,
                    'to_pegawai_id'=>$value->pegawai_id,
                    'riwayat'=>'sppd',
                    'riwayat_id'=>$transaction_id,
                    'keterangan'=>$keterangan
                ];

                $this->sendNotifikasi($paramsNotif);
            }

            // kalau prasyart only notif langsung tahapan dianggap selesai
            if($data->only_notif == 1){
                $data->approved_at = Carbon::now();
                $data->status_id = 1;
                $data->save();
                return $this->_getNextWorkflow($data->sequence, $transaction_id);
            }
            
            return $data;
        }

        

    }

    public function approvalSppd(Request $request)
    {
        DB::beginTransaction();
        try {
            // atasan
            $pegawai = Pegawai::where('id', $request->input('pegawai_id'))->first();

            $workflow_transactions = WorkflowTransaction::where('id', $request->input('workflow_transaction_id'))->first();
            // update status workflow
            $workflow_transactions->status_id = $request->input('status_id');
            $workflow_transactions->catatan_penolakan = $request->input('catatan_penolakan');
            $workflow_transactions->pegawai_id = $pegawai->id;
            $workflow_transactions->nama_pegawai = $pegawai->nama_lengkap;
            $workflow_transactions->nama_jabatan_pegawai = $pegawai->nama_jabatan;
            $workflow_transactions->nama_unit_pegawai = $pegawai->nama_unit;
            $workflow_transactions->nip_pegawai = $pegawai->nip;
            $workflow_transactions->approved_at = Carbon::now();
            $workflow_transactions->save();

            $currentSequence = $workflow_transactions->sequence;
            $sppd = Sppd::where('id', $request->input('id'))->first();

            // cek apakah workflow saat ini disetujui
            if ($request->input('status_id') == 1) {

                // lanjut tahapan selanjutnya
                $nextWorkflow = $this->_getNextWorkflow($currentSequence, $request->input('id'));

                if ($nextWorkflow) {
                    // jika masih ada tahapan selanjutnya update kolom workflow_transaction_id pada sppd
                    $sppd->workflow_transaction_id = $nextWorkflow->id;
                } else {
                    // jika tidak update kolom status pada sppd
                    $sppd->status_id = $request->input('status_id');
                }
            } else {
                // kalau tahapan ditolak langsung update sppd
                $sppd->status_id = $request->input('status_id');
                $sppd->catatan_penolakan = $request->input('catatan_penolakan');
            }

            // send notifikasi approval
            $status = $request->input('status_id') == 1 ? 'Approved' : 'Rejected';
            $judul = 'Bussiness Trip Application Letter';
            $keterangan = 'Your Bussiness Trip Application '.$sppd->no_doc.' Has Been '.$status.' By '.$pegawai->nama_lengkap;
            $paramsNotif = [
                'judul'=>$judul,
                'from_pegawai_id'=>$pegawai->id,
                'to_pegawai_id'=>$sppd->pegawai_id,
                'riwayat'=>'sppd',
                'riwayat_id'=>$sppd->id,
                'keterangan'=>$keterangan
            ];

            $this->sendNotifikasi($paramsNotif);

            // update sppd
            $sppd->save();

            DB::commit();
            $response = new Response(200, 'Berhasil Disimpan', [], [], false);
            return $response->getResponse();
        } catch (\Exception $e) {
            DB::rollback();
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getApproval(Request $request)
    {
        try {
            $params = [
                'pegawai_id' => $request->pegawai_id
            ];

            $pegawai_access = DB::table('vw_pegawai')->where('pegawai_id', $params['pegawai_id'])->first();

            $workflow_stage_prerequisite =  WorkflowStage::where('workflow_type_id', 1)
                ->leftJoin('workflow_stage_prerequisites', 'workflow_stages.id', 'workflow_stage_prerequisites.workflow_stage_id')
                ->selectRaw('workflow_stage_prerequisites.*')
                ->orderBy('workflow_stage_id', 'asc')
                ->whereNotNull('workflow_stage_prerequisites.id')
                ->get();

            $data = [];
            $where = '';
            $total = array_count_values(array_column($workflow_stage_prerequisite->toArray(), 'workflow_stage_id'));
            $resultPrerequisite = [];


            // pengecekan pertama prasyarat workflow sppd
            foreach ($workflow_stage_prerequisite as $key => $value) {
                // prasyarat approval sppd (tidak mengambil dari tabel sppd hanya workflow_prerequisites)

                if ($value->from_db != 1) {
                    if (!isset($data[$value->workflow_stage_id])) {
                        $data[$value->workflow_stage_id]['total'] = $total[$value->workflow_stage_id];
                        $data[$value->workflow_stage_id]['sum'] = 0;
                    }
                    $data[$value->workflow_stage_id]['sum'] += $this->_cek($pegawai_access, $value);
                } else {
                    // prasyarat dari tabel sppd
                    $where .= $this->_generateWhereDb($pegawai_access, $value);
                }
            }

            // prasyarat workflow yang diizinkan
            foreach ($data as $key => $vv) {
                if ($data[$key]['sum'] / $data[$key]['total'] == 1) {
                    $resultPrerequisite[] = $key;
                }
            }

            // $sppd = SPPD::leftJoin('workflow_transactions','workflow_transactions.id','sppd.workflow_transaction_id')
            // ->selectRaw('sppd.*, workflow_transactions.workflow_stage_id')
            // ->whereIn('workflow_transactions.workflow_stage_id', $resultPrerequisite);

            // if($where){
            //     $sppd = $sppd->orWhereRaw($where);    
            // }

            $resultPrerequisite = array_map(function ($a) {
                return sprintf("'%s'", $a);
            }, $resultPrerequisite);


            $resultPrerequisite = join(",", $resultPrerequisite);

            if (strlen($resultPrerequisite) == 0) {
                $resultPrerequisite = '9999999999999';
            }

            $per_page = config('app.MOBILE_PER_PAGE');

            $page = $request->input('page') ? $request->input('page') : 1;
            $start_page = $page > 1 ? ($page * $per_page) - $per_page : 0;


            $sql  = "
                    select *,  ROW_NUMBER() OVER (order by created_at DESC) AS no from (
                        select
                            DATE_FORMAT(s.tgl_mulai, '%d/%m/%Y') as tgl_mulai, DATE_FORMAT(s.tgl_selesai, '%d/%m/%Y') as tgl_selesai, DATE_FORMAT(s.created_at, '%d/%m/%Y') as tgl_created, s.id, s.pegawai_id, case when s.status_id = 1 then 'Approved' when s.status_id = 2 then 'Rejected' else 'Need Approval' end status_sppd, s.status_id, s.proyek, s.no_doc, s.created_at, s.workflow_transaction_id, s.jenis_sppd
                        from
                            sppd s
                        inner join workflow_transactions wt2 on
                            wt2.id = s.workflow_transaction_id
                        where
                            wt2.workflow_stage_id in ($resultPrerequisite) and s.status_id is null
                    union all
                        select
                             DATE_FORMAT(sppd.tgl_mulai, '%d/%m/%Y') as tgl_mulai, DATE_FORMAT(sppd.tgl_selesai, '%d/%m/%Y') as tgl_selesai, DATE_FORMAT(sppd.created_at, '%d/%m/%Y') as tgl_created, sppd.id, sppd.pegawai_id, case when sppd.status_id = 1 then 'Approved' when sppd.status_id = 2 then 'Rejected' else 'Need Approval' end status_sppd, sppd.status_id, sppd.proyek, sppd.no_doc, sppd.created_at, sppd.workflow_transaction_id,sppd.jenis_sppd
                        from
                            sppd
                        inner join workflow_transactions wt on
                            wt.id = sppd.workflow_transaction_id
                        inner join workflow_stages ws on
                            ws.id = wt.workflow_stage_id
                        inner join workflow_stage_prerequisites wsp on
                            wsp.workflow_stage_id = ws.id
                            and from_db = 1
                        where
                            1 = 1 $where
                    union all
                    select
                        DATE_FORMAT(s.tgl_mulai, '%d/%m/%Y') as tgl_mulai,
                        DATE_FORMAT(s.tgl_selesai, '%d/%m/%Y') as tgl_selesai,
                        DATE_FORMAT(s.created_at, '%d/%m/%Y') as tgl_created,
                        s.id,
                        s.pegawai_id,
                        case
                            when wt.status_id = 1 then 'Approved'
                            when wt.status_id = 2 then 'Rejected'
                            else 'Need Approval'
                        end status_sppd,
                        wt.status_id,
                        s.proyek,
                        s.no_doc,
                        s.created_at,
                        s.workflow_transaction_id,
                        s.jenis_sppd
                    from
                        sppd s
                    inner join workflow_transactions wt on
                        wt.transaction_id = s.id
                    where
                        wt.pegawai_id = '".$params['pegawai_id']."' and wt.`sequence`  <> 1
                ) as sppd order By created_at desc
            ";

            $whereRaw = ' 1 = 1 ';

             if($request->input('type')){
                $whereRaw.= " and jenis_sppd = ".$request->input('type');
              }

             if($request->input('start_date') && $request->input('end_date')){
                   $whereRaw.= " and date(created_at) between '".$request->input('start_date')."' and '".$request->input('end_date')."'  ";
              }

              if($request->input('status_id')){
                     if($request->status_id == 99){
                        $whereRaw.= " and status_id is null";
                    }
                    else{
                          $whereRaw.= " and status_id = ".$request->input('status_id');
                    }
                }

                if($request->keyword){
                    $keyword = $request->keyword;
                    $whereRaw.= " and ( no_doc like '%".$keyword."%' ) ";
                }

            $sppd = DB::select("
                    select * from (
                        $sql
                    ) as data
                    where $whereRaw
                    limit $start_page, $per_page
                ");

            $total = DB::select("
                    select count(*) total from (
                        $sql
                    ) as  a ");

            $needApproval = DB::select("  select count(*) total from (
                        $sql
                    ) as  a where status_id is null");

            $Approved = DB::select("  select count(*) total from (
                        $sql
                    ) as  a where status_id = 1");

            $Rejected = DB::select("  select count(*) total from (
                        $sql
                    ) as  a where status_id = 2");
            $result = [
                'data' => $sppd,
                'current_page' => $page,
                'per_page' => $per_page,
                'last_page'=>ceil($total[0]->total / $per_page),
                'total' => $total[0]->total,
                'needApproval'=>$needApproval[0]->total,
                'approved'=>$Approved[0]->total,
                'rejected'=>$Rejected[0]->total

            ];

            // $string = trim(preg_replace('/\s+/', ' ', $sql));
            // echo $string;
            // exit;
            $response = new Response(200, 'Berhasil di dapat', $result, [], true);
            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }


    public function sppdByJenis(Request $request)
    {
        try {
            $jenisSppd = $request->input('jenis_sppd');
            // dd($jenisSppd);
            // exit;
            $data = DB::table('sppd')
                ->selectRaw("*, DATE_FORMAT(tgl_mulai, '%Y-%m-%d') as tgl_mulai, DATE_FORMAT(tgl_selesai, '%Y-%m-%d') as tgl_selesai, ROW_NUMBER() OVER(ORDER BY created_at DESC) AS row_num")
                ->where('jenis_sppd', $jenisSppd)
                ->orderBy('created_at', 'DESC')
                ->paginate(10);

            $response = new Response(200, 'Berhasil di dapat', $data, [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function sppdByPegawaiId(Request $request)
    {
        try {

            $data = $this->getData($request);

            $response = new Response(200, 'Berhasil di dapat', $data, [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

     public function sppdByPegawaiIdMobile(Request $request)
    {
        try {
            $page = config('app.MOBILE_PER_PAGE');
            $data = Sppd::selectRaw(" DATE_FORMAT(tgl_mulai, '%d/%m/%Y') as tgl_mulai, DATE_FORMAT(tgl_selesai, '%d/%m/%Y') as tgl_selesai, DATE_FORMAT(created_at, '%d/%m/%Y') as tgl_created, id, pegawai_id, proyek, case when status_id = 1 then 'Approved' when status_id = 2 then 'Rejected' else 'On Process' end status_sppd, status_id, proyek, no_doc ")
            ->where('pegawai_id', $request->input('pegawai_id'))
            ->orderBy('created_at','desc');


            if($request->input('type')){
                $data = $data->where('jenis_sppd',$request->input('type'));
            }

            if($request->input('start_date') && $request->input('end_date')){
                $data = $data->whereBetween(DB::raw('date(sppd.created_at)'), [$request->input('start_date'), $request->input('end_date')]);
            }

            if($request->input('status_id')){
                 if($request->status_id == 99){
                    $data = $data->whereNull( 'sppd.status_id');
                }
                else{
                     $data = $data->where( 'sppd.status_id', '=', $request->input('status_id') );
                }
            }

            if($request->keyword){
                $keyword = $request->keyword;
                $data = $data->whereRaw('( sppd.no_doc like ? or sppd.proyek like ? or sppd.destinasi like ? or sppd.deskripsi like ? or sppd.bank like ?) ', ['%'.$keyword.'%', '%'.$keyword.'%', '%'.$keyword.'%', '%'.$keyword.'%', '%'.$keyword.'%']);
            }

            $data = $data->paginate(10);

            $response = new Response(200, 'Berhasil di dapat', $data, [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getDetail(Request $request){
        try {
            $data = Sppd::with(['partisipan'=> function($q){
                $q->selectRaw('sppd_participant.id, sppd_participant.sppd_id, pegawai_id, pegawai_nama as nama_lengkap, pegawai_unit as nama_unit, pegawai_jabatan as nama_jabatan, pegawai_nip as nip, pegawai.kelas_jabatan, pegawai.master_golongan_id')
                ->leftJoin('pegawai','pegawai.id','sppd_participant.pegawai_id');
            }])->selectRaw(" sppd.*, DATE_FORMAT(sppd.tgl_mulai, '%d/%m/%Y') as tgl_mulai, DATE_FORMAT(sppd.tgl_selesai, '%d/%m/%Y') as tgl_selesai, DATE_FORMAT(sppd.created_at, '%d/%m/%Y') as tgl_created, pegawai_id, proyek, case when status_id = 1 then 'Approved' when status_id = 2 then 'Rejected' else 'On Process' end status_sppd, pegawai.nama_lengkap as person_in_charge, catatan_penolakan, status_id ")
            ->where('sppd.id', $request->input('id'))
            ->leftJoin('pegawai','pegawai.id','sppd.pegawai_id')
            ->first();

            $response = new Response(200, 'Berhasil di dapat', $data, [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }   
    }

    protected function getData($request, $export = false)
    {
        // $page = $request->input('is_mobile') ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');
        // $data = Sppd::with('peserta')
        //     ->selectRaw("sppd.*, ROW_NUMBER() 
        //     OVER (partition by pegawai_id order by created_at desc) nomor, case when status_id = 1 
        //     then 'Approved' when status_id = 2 then 'Declined' else 'Awaiting' end status_izin, 
        //     DATE_FORMAT(created_at,'%d/%m/%Y') tgl, DATE_FORMAT(confirmed_out_at,'%d/%m/%Y %H:%i') as confirm_out, 
        //     DATE_FORMAT(confirmed_in_at,'%d/%m/%Y %H:%i') as confirm_in   ");
        // ->leftjoin('pegawai', 'pegawai.id', '=', 'sppd.pegawai_id')
        // ->whereNull('parent_id');
        $page = $request->input('is_mobile') ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');
        $data = Sppd::with(['peserta'])
            ->selectRaw("*");
        // ->leftjoin('pegawai', 'pegawai.id', '=', 'sppd.pegawai_id');
        // ->leftjoin(DB::raw('tblemployee as atasan'), 'atasan.pegawai_id', 'riw_izin.atasan_id');

        if ($request->status_id) {

            if ($request->status_id == 99) {
                $data = $data->whereNull('sppd.status_id');
            } else {
                $data = $data->where('sppd.status_id', '=', $request->status_id);
            }
        }
        if ($request->jenis_sppd) {
            $data = $data->where('sppd.jenis_sppd', '=',  $request->jenis_sppd);
        }

        // if ($request->unit_id) {
        //     $data = $data->where('tblemployee.unit_tree_id', 'like',  $request->unit_id . '%');
        // }

        if ($request->start_date && $request->end_date) {
            $data = $data->whereBetween('sppd.pengajuan', [$request->start_date, $request->end_date]);
        }

        // filter by pegawai id
        if ($request->pegawai_id) {
            $data = $data->where('sppd.pegawai_id', '=',  $request->pegawai_id);
        }

        // filter by id izin
        if ($request->filled('id')) {
            $data = $data->where('sppd.id', $request->id)->first();
        } else {

            $data = $export ? $data->get() : $data->paginate($page);
        }

        return $data;
    }


    public function getSummaryPegawai(Request $request){
        try{
                $data = Sppd::selectRaw('count(*) total, sum(case when sppd.status_id = 1 then 1 else 0 end) approved, sum(case when sppd.status_id = 2 then 1 else 0 end) rejected, sum(case when sppd.status_id is null then 1 else 0 end) proses')
                ->where('sppd.pegawai_id','=', $request->input('pegawai_id'));

                $data = $data->first();
            

            $data = [
                'total'=>$data->total,
                'proses'=>$data->proses,
                'approved'=>$data->approved,
                'rejected'=>$data->rejected
            ];

            $response = new Response(200,'Berhasil di dapat',$data,[],true);

            return $response->getResponse();

        }catch(\Exception $e){
            $response = new Response(500,'Ada kesalahan server',[],$e->getMessage(),false);
            return $response->getResponse();
        }
    }

    public function getSppd(Request $request)
    {
        try {

            $data = Sppd::with(['partisipan'=> function($q){
                $q->selectRaw('sppd_participant.id, sppd_participant.sppd_id, pegawai_id, pegawai_nama as nama_lengkap, pegawai_unit as nama_unit, pegawai_jabatan as nama_jabatan, pegawai_nip as nip, pegawai.kelas_jabatan, pegawai.master_golongan_id')
                ->leftJoin('pegawai','pegawai.id','sppd_participant.pegawai_id');
            }])->selectRaw("*, DATE_FORMAT(tgl_mulai, '%Y-%m-%d') as tgl_mulai, DATE_FORMAT(tgl_selesai, '%Y-%m-%d') as tgl_selesai, case when jenis_sppd = 1 then 'Domestic' when jenis_sppd = 2 then 'Abroad' else null end as jenis_sppd_text,
            case when status_id = 1 then 'Approved' when status_id = 2 then 'Rejected' else 'On Process' end status_sppd
            ")
                ->orderBy('created_at', 'DESC')
                ->paginate(10);
            $response = new Response(200, 'Berhasil di dapat', $data, [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getAllSppd(Request $request)
    {
        try {

            $data = Sppd::selectRaw("*, DATE_FORMAT(tgl_mulai, '%Y-%m-%d') as tgl_mulai, DATE_FORMAT(tgl_selesai, '%Y-%m-%d') as tgl_selesai, ROW_NUMBER() OVER(ORDER BY created_at DESC) AS row_num")
                // ->orderBy('created_at', 'DESC')
                ->select('pegawai.id', 'sppd.pegawai_id', 'pegawai.nama_lengkap')
                ->leftJoin('pegawai', 'sppd.pegawai_id', '=', 'pegawai.id')
                ->paginate(10);
            // $data = Sppd::selectRaw("*, date_format(tgl_mulai, '%d/%m/%Y') as tgl_mulai, date_format(tgl_selesai, '%d/%m/%Y') as tgl_selesai, ROW_NUMBER() OVER(ORDER BY created_at DESC) AS row_num  ")
            //     ->orderBy('created_at', 'DESC')
            //     ->paginate(10);
            $response = new Response(200, 'Berhasil di dapat', $data, [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function simpanSppd(Request $request)
    {
        DB::beginTransaction();
        try {
            $params = [
                'id' => $this->uuid(),
                'bank'=>$request->input('bank'),
                'jenis_sppd' => $request->input('jenis_sppd'),
                'tgl_mulai' => $request->input('tgl_mulai'),
                'tgl_selesai' => $request->input('tgl_selesai'),
                'durasi' => $request->input('durasi'),
                'proyek' => $request->input('proyek'),
                'destinasi' => $request->input('destinasi'),
                'deskripsi' => $request->input('deskripsi'),
                'pegawai_id' => $request->input('pegawai_id'),
                'transportasi' => $request->input('transportasi'),
                'lumsump' => $request->input('lumpsump'),
                'biaya_transportasi' => $request->input('biaya_transportasi'),
                'biaya_hotel' => $request->input('biaya_hotel'),
                'pengajuan' => $request->input('pengajuan'),
                'status_id' => $request->input('status_id'),
                'total_lumpsump'=>$request->input('total_lumpsump'),
                'total_biaya_transportasi'=>$request->input('total_biaya_transportasi'),
                'total_biaya_hotel'=>$request->input('total_biaya_hotel'),
                'pegawai_selected' => $request->input('pegawai_selected'),
                'pejabat_penugasan_id'=>$request->input('pejabat_penugasan_id'),
                'pejabat_penugasan_nama'=>$request->input('pejabat_penugasan_nama'),
                'pejabat_penugasan_jabatan'=>$request->input('pejabat_penugasan_jabatan')
            ];

            $newNumber = DB::select("
                select coalesce(max(urutan),0) + 1 as urutan, month(now()) bln, year(now()) th  from sppd where month(created_at) = month(now()) and year(created_at) = year(now())
                ");
            $noID = str_pad($newNumber[0]->urutan, 4, '0', STR_PAD_LEFT);
            $noDoc = "IMO-".$noID."/"."BTRIP/".$newNumber[0]->bln."/".$newNumber[0]->th;
            $params['urutan'] = $newNumber[0]->urutan;
            $params['no_doc'] = $noDoc;

            $pegawai = Pegawai::where('id', $params['pegawai_id'])->first();

            $params['pengaju_nama'] = $pegawai->nama_lengkap;
            $params['pengaju_nip'] = $pegawai->nip;
            $params['pengaju_jabatan'] = $pegawai->nama_jabatan;
            $params['pengaju_unit'] = $pegawai->nama_unit;

            // workflow type pengajuan sppd = 1
            $tahapan = WorkflowStage::where('workflow_type_id', 1)->orderBy('sequence', 'asc')->get();

            // memasukkan master tahapan ke dalam workflow transaction
            foreach ($tahapan as $key => $value) {
                $insertWorkflowTransaction[$key] = [
                    'id' => $this->uuid(),
                    'workflow_stage_id' => $value->id,
                    'transaction_id' => $params['id'],
                    'sequence' => $value->sequence,
                    'pegawai_id' => null,
                    'nama_pegawai' => null,
                    'nama_jabatan_pegawai' => null,
                    'nama_unit_pegawai' => null,
                    'nip_pegawai' => null,
                    'approved_at'=> null,
                    'created_at'=>Carbon::now()
                ];

                // tahapan pertama
                if ($key == 0) {
                    // tahapan pertama selesai saat simpan sppd pertama kali, tahapan pertama adalah pengajuan
                    $insertWorkflowTransaction[$key]['pegawai_id'] = $params['pegawai_id'];
                    $insertWorkflowTransaction[$key]['nama_pegawai'] = $pegawai->nama_lengkap;
                    $insertWorkflowTransaction[$key]['nama_jabatan_pegawai'] = $pegawai->nama_jabatan;
                    $insertWorkflowTransaction[$key]['nama_unit_pegawai'] = $pegawai->nama_unit;
                    $insertWorkflowTransaction[$key]['nip_pegawai'] = $pegawai->nip;
                    $insertWorkflowTransaction[$key]['approved_at'] = Carbon::now();
                }

                if ($key == 1) {
                    // merupakan tahapan selanjutnya setelah pengajuan
                    $params['workflow_transaction_id'] = $insertWorkflowTransaction[$key]['id'];
                }
            }


            // insert partisipan
            $paramsPegawaiSelected = [];
            foreach ($params['pegawai_selected'] as $key => $value) {
                $paramsPegawaiSelected[]=[  
                    'id'=>$this->uuid(),
                    'sppd_id'=>$params['id'],
                    'pegawai_nama'=>$value['nama_lengkap'],
                    'pegawai_nip'=>$value['nip'],
                    'pegawai_unit'=>$value['nama_unit'],
                    'pegawai_id'=>$value['pegawai_id'],
                    'pegawai_jabatan'=>$value['nama_jabatan'],
                    'lumpsump'=>$value['lumpsump'],
                    'created_at'=>Carbon::now()
                ];
            }

            unset($params['pegawai_selected']);
            $res = DB::table('sppd_participant')->insert($paramsPegawaiSelected);



            $WorkflowTransaction = WorkflowTransaction::insert($insertWorkflowTransaction);
            $sppd = Sppd::create($params);



            $response = new Response(200, 'Berhasil di simpan', [], [], true);
            DB::commit();
            return $response->getResponse();
        } catch (\Exception $e) {
            DB::rollback();
            $errors[] = $e->getMessage();
            $response = new Response(500, 'Terjadi Kesalahan pada server', [], $errors, false);
        }
        return $response->getResponse();
    }

    public function UpdateSppd($id, Request $request)
    {
        DB::beginTransaction();
        try {

            $params = [
                'id' => $id,
                'bank'=>$request->input('bank'),
                'jenis_sppd' => $request->input('jenis_sppd'),
                'tgl_mulai' => $request->input('tgl_mulai'),
                'tgl_selesai' => $request->input('tgl_selesai'),
                'durasi' => $request->input('durasi'),
                'proyek' => $request->input('proyek'),
                'destinasi' => $request->input('destinasi'),
                'deskripsi' => $request->input('deskripsi'),
                'pegawai_id' => $request->input('pegawai_id'),
                'transportasi' => $request->input('transportasi'),
                'lumsump' => $request->input('lumpsump'),
                'biaya_transportasi' => $request->input('biaya_transportasi'),
                'biaya_hotel' => $request->input('biaya_hotel'),
                'pengajuan' => $request->input('pengajuan'),
                'status_id' => $request->input('status_id'),
                'total_lumpsump'=>$request->input('total_lumpsump'),
                'total_biaya_transportasi'=>$request->input('total_biaya_transportasi'),
                'total_biaya_hotel'=>$request->input('total_biaya_hotel'),
                'pegawai_selected' => $request->input('pegawai_selected'),
                'pejabat_penugasan_id'=>$request->input('pejabat_penugasan_id'),
                'pejabat_penugasan_nama'=>$request->input('pejabat_penugasan_nama'),
                'pejabat_penugasan_jabatan'=>$request->input('pejabat_penugasan_jabatan')

            ];

            $payloadPegawai = array_column($params['pegawai_selected'], 'pegawai_id');
            $existPegawai = SppdParticipant::where('sppd_id',$params['id'])->get()->toArray();
            $existPegawai = array_column($existPegawai, 'pegawai_id');
            $diffDel = array_diff($existPegawai, $payloadPegawai);
            $diffIns = array_diff($payloadPegawai, $existPegawai);

             // delete peserta
            foreach ($diffDel as $key => $value) {
                SppdParticipant::where('sppd_id',$params['id'])->where('pegawai_id',$value)->delete();
            }

            // tambah peserta
            foreach ($params['pegawai_selected'] as $key => $value) {
                 if(in_array($value['pegawai_id'], $diffIns)){
                    $params_insert = [
                        'id'=>$this->uuid(),
                        'sppd_id'=>$params['id'],
                        'pegawai_nama'=>$value['nama_lengkap'],
                        'pegawai_nip'=>$value['nip'],
                        'pegawai_unit'=>$value['nama_unit'],
                        'pegawai_id'=>$value['pegawai_id'],
                        'pegawai_jabatan'=>$value['nama_jabatan'],
                        'lumpsump'=>$value['lumpsump'],
                        'created_at'=>Carbon::now()

                    ];

                    SppdParticipant::create($params_insert);
                 }
                 

            }

             unset($params['pegawai_selected']);
            Sppd::where('id', $id)->update($params);

            $response = new Response(200, 'Data SPPD berhasil diperbarui', [], [], true);
            DB::commit();
            return $response->getResponse();
        } catch (\Exception $e) {
            DB::rollback();
            $errors[] = $e->getMessage();
            $response = new Response(500, 'Terjadi Kesalahan pada server', [], $errors, false);
        }
        return $response->getResponse();
    }


    public function delSppd(Request $request)
    {
        DB::beginTransaction();
        try {
            $id = $request->input('id');
            $rsppd = Sppd::where('id', '=', $id)->first();
            $rsppd->deleted_at = Carbon::now();
            $rsppd->save();


            // $sppd2 = Sppd::where('parent_id', '=', $id)->update([
            //     'deleted_at' => Carbon::now()
            // ]);

            // $notif = Notifikasi::where('riwayat_id', '=', $id)->first();

            // if ($notif) {
            //     $notif->deleted_at = Carbon::now();
            //     $notif->save();
            // }

            $response = new Response(200, 'Berhasil di hapus', [], [], true);
            DB::commit();
            return $response->getResponse();
        } catch (\Exception $e) {
            DB::rollback();
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }


    public function cetak_sppd(Request $request)
    {
        $sppd = Sppd::with('partisipan')
        ->selectRaw("sppd.*,pegawai.nip as pejabat_penugasan_nip, date_format(tgl_mulai, '%d/%m/%Y') _tgl_mulai, date_format(tgl_mulai, '%d/%m/%Y') _tgl_selesai, date_format(sppd.created_at, '%d/%m/%Y') _tgl_created ")
        ->where('sppd.id',$request->input('id'))
        ->leftJoin('pegawai','pegawai.id','sppd.pejabat_penugasan_id')
        ->first();

        $path = public_path('/images/ims.png');
        $type = pathinfo( $path, PATHINFO_EXTENSION );
        $data = file_get_contents( $path );
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode( $data );
        //dikomen nanti kalo dah jadi di buka lagi
        $pdf = Pdf::loadview('cetak_sppd', ['sppd' => $sppd,'logo'=>$base64]);
        // return $pdf->download('sppd.pdf');
        return $pdf->stream();
        return view('cetak_sppd', ['sppd' => $sppd,'logo'=>$base64]);
    }

    public function rincianBiayaSppd()
    {
        $sppd = Sppd::all();
        // $details = ['title' => 'test'];

        //dikomen nanti kalo dah jadi di buka lagi
        $pdf = Pdf::loadview('rincian_biaya_sppd', ['sppd' => $sppd]);
        return $pdf->download('Rincian-Biaya.pdf');
        return view('rincian_biaya_sppd', ['sppd' => $sppd]);
    }
}
