<?php

namespace App\Http\Controllers;

use Ramsey\Uuid\Uuid;
use App\Models\LogActivity;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

class logActivityController extends Controller
{
    public function logActivityWrite($user, $modul, $keterangan = null, $status = null, $dataSebelum = null, $dataSesudah = null, $fitur = null)
    {
        $data  = [
            'data_sebelum' => json_encode($dataSebelum),
            'data_sesudah' => json_encode($dataSesudah),
        ];
        // set default user
        if ($user ==  null) {
            $user = (object) [
                'id' => null,
                'ip_address' => null,
                'user_nip' => null,
                'user_deptcode' => null,
                'user_deptname' => null,
                'user_login' => null,
                'user_nama' => 'anymouse'
            ];
        }
        $log = LogActivity::create([
            'id' => Uuid::uuid4()->toString(),
            'modul' => $modul,
            'keterangan' => $keterangan,
            'data_sebelum' => $data['data_sebelum'],
            'data_sesudah' =>  $data['data_sesudah'],
            'status' =>  $status,
            'ip_address' =>  $user['ip'],
            'user_nama' =>  $user['name'],
            'user_nip' => $user['nip'],
            'user_deptcode' => $user['id_department'],
            'user_deptname' => $user['job'],
            'user_login' =>  $user['id'],
            'fitur' =>  $fitur
        ]);
    }

    public function logActivityUpdate($user, $modul, $keterangan = null, $status = null, $dataSebelum = null, $dataSesudah = null, $fitur = null): void
    {
        $data  = [
            'data_sebelum' => json_encode($dataSebelum),
            'data_sesudah' => json_encode($dataSesudah),
        ];
        // set default user
        if ($user ==  null) {
            $user = (object) [
                'id' => null,
                'ip_address' => null,
                'user_nip' => null,
                'user_deptcode' => null,
                'user_deptname' => null,
                'user_login' => null,
                'user_nama' => 'anymouse'
            ];
        }
        $log = LogActivity::where('id', $user->id)->first();
        $log->update([
            'id' => Uuid::uuid4()->toString(),
            'modul' => $modul ?? $log->modul,
            'keterangan' => $keterangan ?? $log->keterangan,
            'data_sebelum' => $data['data_sebelum'] ?? $log->data_sebelum,
            'data_sesudah' =>  $data['data_sesudah'] ?? $log->data_sesudah,
            'status' =>  $status ?? $log->status,
            'ip_address' =>  $user->ip_address ?? $log->ip_address,
            'user_nama' =>  $user->user_nama ?? $log->user_nama,
            'user_nip' => $user->user_nip ?? $log->user_nip,
            'user_deptcode' => $user->user_deptcode ?? $log->user_deptcode,
            'user_deptname' => $user->user_deptname ?? $log->user_deptname,
            'user_login' =>  $user->id ?? $log->user_login,
            'fitur' =>  $fitur ?? $log->fitur
        ]);
    }

    public function logActivityDelete($id): void
    {
        LogActivity::where('id', $id)->delete();
    }
}
