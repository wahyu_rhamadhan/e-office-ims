<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GlobalClass\Response;
use App\Models\{Pegawai, RiwPkwt};
use App\Models\Notifikasi;
use Illuminate\Support\Facades\DB;

class PkwtController extends Controller
{
    public function getPkwt(Request $request){
        try{
            $params = [
              'pegawai_id'=>$request->input('pegawai_id'),
            ];
            $data = RiwPkwt::selectRaw("
                    riw_pkwt.*, date(tgl_join) as tgl_join, case when is_new = 1 then 'Terbaru' else null end status
                ")->where('pegawai_id',$params['pegawai_id'])->get();
            $response = new Response(200,'Berhasil di dapat',$data,[],true);
        }catch(\Exception $e){
            $response = new Response(500,'Ada kesalahan server',[],$e->getMessage(),false);
        }
        return $response->getResponse();
    }
    public function simpanPkwt( Request $request ) {
        DB::beginTransaction();
        try {
            
            $params = [
                'nip'=>$request->input('nip'),
                'pegawai_id'=>$request->input( 'pegawai_id' ),
                'tgl_join'=>$request->input( 'tgl_join' ),
                'tgl_selesai'=>$request->input( 'tgl_resign' ),
                'is_new'=>$request->input('is_new'),
                'master_status_pegawai_id'=>65
            ];


            if(!empty($request->input( 'id' ))){
                $rc = RiwPkwt::where( 'id', $request->input( 'id' ) )->first();
                $rc->update($params);
               
            }else{
                $params['id'] = $this->uuid();
                $rc = RiwPkwt::create( $params );
               
            }

            
            // update pegawai
            $this->updateDataPegawai($params['pegawai_id']);

            $response = new Response( 200, 'Data Berhasil Di simpan', [], [] , false);
            DB::commit();
            
        } catch( \Exception $e ) {
             DB::rollback();
            $errors[] = $e->getMessage();
            $response = new Response( 500, 'Terjadi Kesalahan pada server', [], $errors, false);
        }

        return $response->getResponse();
    }
    public function delPkwt( Request $request ) {
        try {
            $id = $request->input('id');
            $spr = RiwPkwt::where( 'id' , $id );
            if($spr){
                $spr->delete();
                $response = new Response( 200, 'Data berhasil dihapus', [], [], false );
            }else{
                $response = new Response( 200, 'Data Tidak Ditemukan', [], [], false );
            }

            $this->updateDataPegawai($request->input('pegawai_id'));
        } catch( \Exception $e ) {
            $errors[] = $e->getMessage();
            $response = new Response( 500, 'Terjadi Kesalahan pada server', [],[], $errors, false );
        }

        return $response->getResponse();
    }
}
