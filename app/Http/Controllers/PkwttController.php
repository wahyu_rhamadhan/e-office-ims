<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GlobalClass\Response;
use App\Models\RiwPkwtt;
use App\Models\Notifikasi;
use Illuminate\Support\Facades\DB;

class PkwttController extends Controller
{
    public function getPkwtt(Request $request){
        try{
            $params = [
              'pegawai_id'=>$request->input('pegawai_id'),
            ];
            $data = RiwPkwtt::selectRaw('riw_pkwtt.*,DATE(tgl_peg) AS tgl_peg')->where('pegawai_id',$params['pegawai_id'])->first();
            $response = new Response(200,'Berhasil di dapat',$data,[],true);
        }catch(\Exception $e){
            $response = new Response(500,'Ada kesalahan server',[],$e->getMessage(),false);
        }
        return $response->getResponse();
    }
    public function simpanPkwtt( Request $request ) {
        DB::beginTransaction();
        try {
            $params = [
                    'pegawai_id'=>$request->input('pegawai_id'),
                    'tgl_peg'=>$request->input('tgl_peg'),
                    'no_sk'=>$request->input('no_sk'),
                    'nip'=>$request->input('nip'),
                    'master_status_pegawai_id'=>98
            ];
            if(!empty($request->input( 'id' ))){
                $rc = RiwPkwtt::where( 'id', $request->input( 'id' ) )->first();
                $rc->update($params);
            }else{
                $params['id'] = $this->uuid();
                $rc = RiwPkwtt::create( $params );
                
            }
             $this->updateDataPegawai($request->input('pegawai_id'));
             $response = new Response( 200, 'Data Berhasil Di simpan', [], [] , false);
             DB::commit();
        } catch( \Exception $e ) {
             DB::rollback();
            $errors[] = $e->getMessage();
            $response = new Response( 500, 'Terjadi Kesalahan pada server', [], $errors, false);
        }

        return $response->getResponse();
    }
    public function delPkwtt( Request $request ) {
        try {
            $id = $request->input('id');
            $spr = RiwPkwtt::where( 'id' , $id );
            if($spr){
                $spr->delete();
                $response = new Response( 200, 'Data berhasil dihapus', [], [], false );
            }else{
                $response = new Response( 200, 'Data Tidak Ditemukan', [], [], false );
            }
        } catch( \Exception $e ) {
            $errors[] = $e->getMessage();
            $response = new Response( 500, 'Terjadi Kesalahan pada server', [],[], $errors, false );
        }

        return $response->getResponse();
    }
}
