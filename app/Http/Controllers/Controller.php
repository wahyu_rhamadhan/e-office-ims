<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Models\{ Unit, Pegawai };
use App\Models\Notifikasi;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    protected function tgl_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
     
    }

    protected function updateDataPegawai($pegawai_id){
        $upt = DB::table('vw_riwayat_pegawai_last')->where('pegawai_id',$pegawai_id)->first();

        if($upt){
            $peg = Pegawai::where('id',$pegawai_id)->first();
            $peg->update([
                'nip'=>$upt->nip,
                'tgl_join'=>$upt->tgl_mulai,
                'tgl_selesai'=>$upt->tgl_selesai,
                'master_status_pegawai_id'=>$upt->master_status_pegawai_id
            ]);
        }
        
    }

     protected function uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

       protected function sendNotifikasi($params){
        $params['id'] = $this->uuid();
        Notifikasi::create($params);

        if(config('app.PUSH_NOTIFICATION')){
            $fields['include_external_user_ids'] = [$params['to_pegawai_id']];
            $fields['contents'] = array(
                              "en" => $params['keterangan'],
                          );
            $fields['headings'] = array(
                "en"=>$params['judul']
            );

           \OneSignal::sendPush($fields);     
        }
       
    }

    protected function atasanLangsung($params){
        /**
         * ambil atasan langsung
         * @param Array $params terdiri dari unit_id, pegawai_id, position_status
         * @return string
         */

        if($params['position_status']=='PST005'){

            $explode = explode('.', $params['unit_id']);

            $id = '';
            foreach ($explode as $key => $value) {
                
            }
            $unit = Unit::with('pegawai')->where('tree_id',$params['unit_id'])->first();
            
            if(empty($unit->pegawai)){
              $atasan = $this->recursiveGetAtasan($unit->tree_id);
            }
            else{
                $atasan = $unit->pegawai->EmpName.' ('.$unit->pegawai->EmpCode.')';
            }
            return $atasan;
        }
        else{
            
            $explode = explode('.', $params['unit_id']);
            $id = '';
            $iterate = (count($explode)-1) - 1 == 0 ? 1 : (count($explode)-1) - 1;

            $id = '';
            for ($i=0; $i <$iterate; $i++) { 
                $id.= $explode[$i].'.';
            }

            $unit = Unit::with('pegawai')->where('tree_id',$id)->first();
            $atasan = $unit->pegawai->EmpName.' ('.$unit->pegawai->EmpCode.')';
            return $atasan;

        }

    }

    protected function recursiveGetAtasan($unit_id){
        $parent = $this->getParent($unit_id);

      
        $unit =  Unit::with('pegawai')->where('tree_id',$parent)->first();
      
        if(empty($unit->pegawai)){
            $this->recursiveGetAtasan($unit->tree_id);
        }else{
            return $unit->pegawai->EmpName.' - '.$unit->pegawai->EmpCode;
        }
    }

    protected function getParent($unit_id){
        $explode = explode('.', $unit_id);
        if(count($explode) > 2){
            $parent_id = '';
            for ($i=0; $i <count($explode)-2 ; $i++) { 
                $parent_id.=$explode[$i].'.';
            }

            return $parent_id;
        }

        return $unit_id;
        
    }

     protected function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

     protected function getFullUnit($unit_id){
        $explode = explode(".", $unit_id);

        $id='';
        $unit='';
        foreach ($explode as $key => $value) {
           if(strlen($value) > 0){
                $id.=$value.'.';
                $res = Unit::where('tree_id',$id)->first();
                if($key > 0){
                     $unit.= ' - '.$res->nama;
                }
                else{
                    $unit.=$res->nama;
                }
               
           }
        }
        
        return $unit;
    }
}
