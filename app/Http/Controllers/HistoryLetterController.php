<?php

namespace App\Http\Controllers;

use App\GlobalClass\Response;
use App\Models\HistoryPersuratan;
use Illuminate\Http\Request;

class HistoryLetterController extends Controller
{
    public function HistoryWriter($data)
    {
        // created history


        try {
            $data = HistoryPersuratan::create([
                'from' => $data['form'],
                'departemen_from' => $data['departemen_from'],
                'to' => $data['to'],
                'departemen_to' => $data['departemen_to'],
                'letter_id' => $data['letter_id'],
                'reviewer' => $data['reviewer'],
                'comment' => $data['comment'],
                'keterangan' => $data['keterangan']
            ]);
            return $data;
        } catch (\Exception $e) {
            $response =  new Response(500, 'ada masalah server', $e->getMessage(), [], true);
            return $response->getResponse();
        }
    }

    public function HistoryUpdate($data)
    {
        try {
            $history = HistoryPersuratan::where('id', $data->id)->first();
            if ($history == null) {
                $response = new Response(404, 'data tidak ditemukan', [], [], true);
                return $response->getResponse();
            }
            $history->data['keterangan'] ?? $history->keterangan;
            $history->saved();
            return true;
        } catch (\Exception $e) {
            $response = new Response(500, 'ada masalah server', $e->getMessage(), [], true);
            return $response->getResponse();
        }
    }

    public function HistoryDelete(Request $request)
    {
        $history = HistoryPersuratan::where('id', $request->id)->first();
        if ($history == null) {
            $response = new Response(404, 'data tidak ditemukan', [], [], true);
            return $response->getResponse();
        }
        $history->delete();

        return true;
    }

    public function addKeterangan($message, $whoReject = null): String
    {
        $mainKeterangan =  $message . ' ' . $whoReject;
        return $mainKeterangan;
    }
}