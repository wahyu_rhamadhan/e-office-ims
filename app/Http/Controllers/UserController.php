<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Notifikasi;
use App\GlobalClass\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Services\PengembanganSdmService;
use App\Mail\SendEmail;
use Mail;
use App\Models\PasswordReset;
use App\Models\ResendVerifikasi;


class UserController extends Controller
{

    public function sendEmail(Request $request){

       $paramsEmail = [
        'email_tujuan'=>$request->input('email'),
        'name'=>$request->input('nama'),
        'view'=>'notifikasi',
        'pegawai_id'=>$request->input('pegawai_id'),
        'template'=>$request->input('template')
      ];
      
      $paramsNotifikasi = [
        'id'=>$this->uuid(),
        'template'=>$request->input('template'),
        'pegawai_id'=>$request->input('pegawai_id'),
        'keterangan'=>$request->input('judul')
      ];

      Notifikasi::create($paramsNotifikasi);
      dispatch(new \App\Jobs\SendEmailJob($paramsEmail));

    }

    public function resendVerifikasi(Request $request){
        $user = new User();
        $nik = $request->input('ktp');
        $email = $request->input('email');
        $cek_nik = $user->where('nik',$nik)->first();
        if(!$cek_nik){
          return response()->json(['message'=>'NIK belum terdaftar ke sistem, silahkan registrasi dahulu','success'=>false],200);
        }

        $cek_email = $user->where('email',$email)->where('nik',$nik)->first();
        if(!$cek_email){
          return response()->json(['message'=>'Email dan NIK tidak sama seperti yang didaftarkan ke sistem','success'=>false],200);
        }
        $token = $this->uuid();
        $currentDate = Carbon::now();
        $valid_token_until = Carbon::now()->addMinute(30);
        $params = [
          'id'=>$this->uuid(),
          'token'=> $token,
          'ktp'=>$nik,
          'email'=>$email,
          'valid_until'=>$valid_token_until
        ];
        
        $data = ResendVerifikasi::create($params);

        $paramsNotifikasi = [
          'email_tujuan'=> $email,
          'name'=>$cek_email->name,
          'view'=>'resend_verifikasi',
          'token'=>$params['token']
        ];

        dispatch(new \App\Jobs\SendEmailJob($paramsNotifikasi));
        return response()->json(['message'=>'Berhasil di kirim','success'=>true],200);
    }

    public function cekTokenPassword(Request $request){
      $token = $request->input('token');
      $cek = PasswordReset::where('token',$token)->orderBy('valid_until','desc')->first();
      $now = Carbon::now();
      $valid_token = $cek->valid_token_until;
      
      // cek valid
      if($now > $valid_token){
        return response()->json(['message'=>'Token tidak valid','success'=>false],200);
      }

       return response()->json(['message'=>'Token valid','success'=>true],200);
      
    }

    public function cekTokenVerifikasi(Request $request){
      $token = $request->input('token');
      $cek = ResendVerifikasi::where('token',$token)->first();
      $now = Carbon::now();
      $valid_token = $cek->valid_until;
      
      // cek valid
      if($now > $valid_token){
        return response()->json(['message'=>'Token tidak valid','success'=>false],200);
      }

      $user = User::where('nik',$cek->ktp)->first();
      $user->verified = Carbon::now();
      $user->save();

       return response()->json(['message'=>'Akun anda sudah terverifikasi','success'=>true],200);
      
    }

    public function ubahPassword(Request $request){
    
      $data = PasswordReset::where('token',$request->input('token'))->first();
      $user = User::where('email',$data->email)->first();
      
     $password = bcrypt($request->password);
     $user->password = $password;
     $user->save();
      
      return response()->json(['message'=>'Berhasil di ubah','success'=>true],200);
      
    }

    public function forgotPassword(Request $request){
      try {
        $currentDate = Carbon::now();
        $valid_token_until = Carbon::now()->addMinute(30);
        $params = [
          'token'=>$this->uuid(),
          'email'=>$request->email,
          'valid_token_until'=>$valid_token_until
        ];
        PasswordReset::create($params);
         $paramsEmail = [
          'email_tujuan'=>$request->email,
          'name'=>$params['email'],
          'view'=>'forgot_password',
          'token'=>$params['token']
        ];

         dispatch(new \App\Jobs\SendEmailJob($paramsEmail));
         $response = new Response(200, 'Verifikasi lupa password dikirimkan melalui email',[],[], true);
         $response = $response->getResponse();
      } catch (Exception $e) {
        $error[] = $e->getMessage();
        $response = new Response(500, 'Ada kesalahan server',[],$error,false);
        $response = $response->getResponse();
      }
      
       return response()->json($response, $response['code']);
    }

    public function verifikasi(Request $request){
      DB::beginTransaction();
      try{
        $id = $request->input('id');
        $otp = $request->input('otp');

        $user = User::where('uuid','=',$id)->where('kode_otp','=',$otp)->first();
        
        if(!$user){
          $response = new Response(200, 'Kode OTP yang dimasukkan tidak sesuai',[],[], false);
          $response = $response->getResponse();
          return response()->json($response, $response['code']);
        }

         $user->verified = Carbon::now();

         $service = new PengembanganSdmService();

         $params = [
            'email'=>$user->email,
            'nama'=>$user->name,
            'nik'=>$user->nik,
            'hp'=>$user->nomor
         ];
         $sdm = $service->updatePegawai($params);
         $hasil = json_decode($sdm);

         if($hasil->code === 200){
             $user->pegawai_id = $hasil->data->id;
             $user->kode_otp = null;
             $user->save();
             DB::commit();
         } 
         else{
             DB::rollBack();
             $error[] = $e->getMessage();
             $response = new Response(500, 'Gagal di simpan',[],$error,false);
             $response = $response->getResponse();
              return response()->json($response, $response['code']);
         } 
     
        
         $response = new Response(200, 'Verifikasi berhasil',[],[], true);
         $response = $response->getResponse();
          

       
        }catch(\Exception $e){
           DB::rollBack();
           $error[] = $e->getMessage();
           $response = new Response(500, 'Ada kesalahan server',[],$error,false);
           $response = $response->getResponse();
        }

        return response()->json($response, $response['code']);
    }
    //index
    public function index(Request $request){
        try{

            if($request->input('id')){
              $res['data'] = Warga::where('WARGA_ID','=',$request->input('id'))->first();
              return json_encode($res);
            }
            else{
              $search = $request->input('search');
              $warga = new Warga();

              if($search){
                $warga = $warga->orWhere('NIK','like','%'.$search.'%')->orWhere('NAMA','like','%'.$search.'%');
              }

              $data = $warga->paginate(10);
              return $data;
            }
            

        }catch(\Exception $e){
            return "Sedang ada Perbaikan";
        }
        
    }

    //tambah data warga
    public function create(request $request, Warga $warga){
        try {


            $valid = $this->validate($request, [
                'FOTO' => 'nullable|sometimes|mimes:jpeg,png,jpg|max:2048',
                'NIK'  => 'unique:warga,NIK|digits_between:2,20'
            ]);
              // menyimpan data file yang diupload ke variabel $file
              $file = $request->file('FOTO');
              $nama_file = null;
              if ($file){
                  $nama_file = date('yymd')."_".$file->hashName();
                  // isi dengan nama folder tempat kemana file diupload
                  $tujuan_upload = 'foto';
                  $file->move($tujuan_upload,$nama_file);
              }
              $warga->insert([
                  'NIK'             => $request->NIK,
                  'NO_KK'           => $request->NO_KK,
                  'NAMA'            => $request->NAMA,
                  'JENIS_KELAMIN'   => $request->JENIS_KELAMIN,
                  'TEMPAT_LAHIR'    => $request->TEMPAT_LAHIR,
                  'TANGGAL_LAHIR'   => $request->TANGGAL_LAHIR,
                  'AGAMA_ID'        => $request->AGAMA_ID,
                  'HUB_ID'          => $request->HUB_ID,
                  'PEKERJAAN'       => $request->PEKERJAAN,
                  'ALAMAT'          => $request->ALAMAT,
                  'RT_ID'           => $request->RT_ID,
                  'RW_ID'           => $request->RW_ID,
                  'FOTO'            => $nama_file,
                  'CLIENT_ID'       => $request->CLIENT_ID
              ]);
              return "Data Berhasil Disimpan";    

        }catch(\Exception $e){
            dd($e);
            //return response()->json(['a' => $e]);
        }
    
    }
    // edit data warga
    public function update(request $request,Warga $warga,$WARGA_ID){
        $this->validate($request, [
           'FOTO' => 'nullable|sometimes|mimes:jpeg,png,jpg|max:2048',
           'NIK'  => 'required'
    ]);
        // foto old di hapus
        $foto = $warga->where('WARGA_ID',$WARGA_ID)->first();
    File::delete('foto/'.$foto->FOTO);
    // foto new
    $file = $request->file('FOTO');
        $nama_file = null;
        if ($file){
            $nama_file = date('yymd')."_".$file->hashName();
            // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'foto';
        $file->move($tujuan_upload,$nama_file);
        }
        $warga->where('WARGA_ID',$request->WARGA_ID)->update([
            'NIK'             => $request->NIK,
            'NO_KK'           => $request->NO_KK,
            'NAMA'            => $request->NAMA,
            'JENIS_KELAMIN'   => $request->JENIS_KELAMIN,
            'TEMPAT_LAHIR'    => $request->TEMPAT_LAHIR,
            'TANGGAL_LAHIR'   => $request->TANGGAL_LAHIR,
            'AGAMA_ID'        => $request->AGAMA_ID,
            'HUB_ID'          => $request->HUB_ID,
            'PEKERJAAN'       => $request->PEKERJAAN,
            'ALAMAT'          => $request->ALAMAT,
            'RT_ID'           => $request->RT_ID,
            'RW_ID'           => $request->RW_ID,
            'FOTO'            => $nama_file,
            'CLIENT_ID'       => $request->CLIENT_ID
        ]);

        return "Data Berhasil Diubah";

    }
    
    //hapus data warga
    public function delete(Warga $warga,$WARGA_ID){
        // hapus FOTO
        $foto = $warga->where('WARGA_ID',$WARGA_ID)->first();
    File::delete('foto/'.$foto->FOTO);

        // hapus Data    
        $warga->where('WARGA_ID',$WARGA_ID)->delete();

        return "Data Berhasil Di Hapus";
    }    

}
