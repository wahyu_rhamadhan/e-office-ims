<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Jobs\LogWriter;
use App\Models\Pegawai;
use App\Models\MasterUnit;
use App\Models\Notifikasi;
use App\Models\TblEmployee;
use App\Services\FtpService;
use Illuminate\Http\Request;
use App\GlobalClass\Response;
use App\Models\PersuratanSurat;
use App\Jobs\SendNotificationIms;
use App\Models\PersuratanSuratDtl;
use Illuminate\Support\Facades\DB;
use App\Models\PersuratanDisposisi;
use App\Models\PersuratanNomorSurat;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\PersuratanDisposisiDtl;
use PhpOffice\PhpWord\TemplateProcessor;
use Illuminate\Support\Facades\Validator;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class PersuratanController extends Controller
{
    public function previewFile(Request $request)
    {
        try {
            $persuratan = PersuratanSurat::with('pengirim')
                ->with('penerima', function ($query) {
                    $query->where('position_status_code', 'PST001');
                })
                ->selectRaw("persuratan_surat.*, DATE_FORMAT(persuratan_surat.created_at,'%d/%m/%Y') tgl_surat")->where('id', $request->input('id'))->first();



            $service = new FtpService();
            $path = config('app.FTP_FOLDER') . 'persuratan/';
            // $filetype=pathinfo($file, PATHINFO_EXTENSION );
            $content = $service->readFile($path . $persuratan->file_surat_generated);
            echo $content;
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function deleteSurat(Request $request)
    {
        try {

            $res = PersuratanSurat::where('id', $request->id)->delete();
            $dtl = PersuratanSuratDtl::where('persuratan_surat_id', $request->id)->delete();

            $response = new Response(200, 'Berhasil di hapus.', [], [], true);
            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getSuratKeluarById(Request $request)
    {
        try {
            // dd($request->all());
            // exit;
            $persuratan = PersuratanSurat::selectRaw("persuratan_surat.*, case when klasifikasi_surat_id = 1 then 'Memo' else 'Surat Dinas' end klasifikasi ")
                ->with(['pengirim', 'penerima', 'mengetahui', 'unit'])
                ->with('disposisi.detail')
                ->where('id', $request->input('id'))
                ->first();
            $response = new Response(200, 'Berhasil di dapat', $persuratan, [], true);
            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function approveSurat(Request $request)
    {
        $user = (array) $request['auth_user'];
        $user['ip'] =  $request['ip-address'];


        $surat =  PersuratanSurat::where('id', $request->id)->first();
        //validation aprroved

        if ($surat->is_approve == 1) {

            //balikan konfirmasi bahwa udah di approved

            //mencatat log Activity 
            $log =  new LogWriter($user, 'Surat has Approvef Before', 'Surat sudah pernah di aproved', 'failed', $request->all(), null, 'approvedSurat');
            $this->dispatch($log);

            $response = new Response(400, 'Surat ini Sudah di Approved', [], [], true);
            return $response->getResponse();
        }
        //to get unitName in PersuratanNomorsurat 
        $persuratanNomor =  PersuratanNomorSurat::where('unit_id', $surat->unit_id)->first();

        if ($persuratanNomor == null) {
            $log =  new LogWriter($user, 'Not Found', 'surat tidak ditemukan', 'failed', $request->all(), null, 'approvedSurat');
            $this->dispatch($log);

            $response = new Response(404, 'Surat ini Sudah di Approved', [], [], true);
            return $response->getResponse();
        }

        // to generate Nomor Surat after approved
        $nomorSurat = $this->_generateNomorSurat(
            [
                "unit_id" => $surat->unit_id,
                "unit_name" => $persuratanNomor->unit_name,
                "kode_unit" => $surat->kode_unit,
                "klasifikasi_surat_id" => $surat->klasifikasi_surat_id,
                "manjab_draft_id" => $persuratanNomor->manjab_draft_id, //
            ]
        );

        //to Update Generate Nomor Surat in Persuratan
        $surat->nomor_surat = $nomorSurat['full_nomor'];
        $surat->is_approve = 1;
        $surat->save();

        $_record = [
            'data' =>  $surat
        ];

        // melakukan notifikasi 
        $pengirim = $surat->pengirim()->first();
        $penerima = $surat->penerima()->first();

        $judul = 'Letter has Aprroved';
        $keterangan = 'Letter Has approve by ' . $penerima->nama . ' - ' . $penerima->nip;
        $riwayat = null;

        $params = [
            'judul' =>  $judul,
            'form_pegawai_id' =>  $pengirim->pegawai_id,
            'is_read' => null,
            'keterangan' =>  $keterangan,
            'to_pegawai_id' => $penerima->pegawai_id,
            'riwayat' => $riwayat,
            'riwayat_id' => $surat->id,
            'catatan_penolakan' => null
        ];

        $notif =  new SendNotificationIms($params);

        //mencatat log Activity 
        $log =  new LogWriter($user, 'The Letter is Success Aproved', 'Surat sudah di approved', 'failed', $request->all(), null, 'approvedSurat');
        $this->dispatch($log);
        $response = new Response(200, 'Berhasil di simpan', [], [], true);
        return $response->getResponse();
    }

    public function unapproveSurat(Request $request)
    {

        $user = (array)$request['auth_user'];
        $user['ip'] =  $request['ip-address'];
        //get id form persuratanmodel

        if ($request->comment ==  null && $request->id) {
            $log =  new LogWriter($user, 'comment is required dan id is required', 'Kesalahan User', 'failed', $request->all(), null, 'unapproveSurat');
            $this->dispatch($log);
            $response = new Response(404, 'comment is required dan id is required', [], [], true);
            return $response->getResponse();
        }

        $surat = PersuratanSurat::where('id', $request->id)->first();

        if ($surat == null) {
            $log =  new LogWriter($user, 'Not Found', 'surat tidak ditemukan', 'failed', $request->all(), null, 'unapproveSurat');
            $this->dispatch($log);
            $response = new Response(404, 'comment is required dan id is required', [], [], true);
            return $response->getResponse();
        }
        // menegambil siapa yang melakukan reject dan mencata comment
        // mengambil id surat
        $id = $surat->id;
        //mengambil penerima 

        $dataPenerima =  $surat->penerima()->first();

        $penerima = $dataPenerima->nama;
        $departemenTo = $dataPenerima->unit_kerja;

        // mengambil pengirim

        $dataPengirim = $surat->pengirim()->first();
        $pengirim =  $dataPengirim->nama;
        $departemenFrom = $dataPengirim->unit_kerja;

        //mengambil keterangan penolakan
        $addKeterangan = new HistoryLetterController();
        $keterangan = $addKeterangan->addKeterangan('surat anda telah direject oleh', $pengirim);

        //menyatukan jadi satu
        $data =  [
            'letter_id' => $id,
            'form' => $pengirim,
            'departemen_from' => $departemenFrom,
            'to' => $penerima,
            'departemen_to' => $departemenTo,
            'reviewer' => $pengirim,
            'comment' =>  $request->comment,
            'keterangan' => $keterangan

        ];
        //menambahkan surat pada history surat dikarenakan direject

        $history = new HistoryLetterController();
        $rejectDetail =  $history->HistoryWriter($data);

        // melanjutkan update pada persuratanSurat

        $surat->is_approve = 2;
        $surat->save();

        //record
        $record = [
            'reject_detail' =>  $rejectDetail,
            'surat' => $surat

        ];

        $notify =  new Notifikasi();
        $riwayat = $notify->riw_izin()->first();

        // get penerima 

        $pengirim = $surat->penerima()->first();
        $penerima = $surat->pengirim()->first();

        $judul = 'Avenger';
        $keterangan = 'Keep Of Vorever from ' . $pengirim->nama . ' - ' . $pengirim->nip;
        $params = [
            'judul' =>  $judul,
            'form_pegawai_id' =>  $pengirim->pegawai_id,
            'is_read' => null,
            'keterangan' =>  $keterangan,
            'to_pegawai_id' => $penerima->pegawai_id,
            'riwayat' => $riwayat,
            'riwayat_id' => $surat->id,
            'catatan_penolakan' => null
        ];


        $notify =  new SendNotificationIms($params);
        $this->dispatch($notify);

        //mencatat log activity;
        $log =  new LogWriter($user, 'Rejected Letter', 'Surat telah di reject', 'success', $request->all(), $record, 'unapproveSurat');
        $this->dispatch($log);
        $response = new Response(200, 'Maaf Surat di reject dan berhasil di simpan di history', ['data' => $history], [], true);
        return $response->getResponse();
    }

    public function cetakSurat(Request $request)
    {
        $persuratan = PersuratanSurat::with('pengirim')
            ->with('penerima', function ($query) {
                $query->where('position_status_code', 'PST001');
            })
            ->selectRaw("persuratan_surat.*, DATE_FORMAT(persuratan_surat.created_at,'%d/%m/%Y') tgl_surat")->where('id', $request->input('id'))->first();



        $service = new FtpService();
        $path = config('app.FTP_FOLDER') . 'persuratan/';
        // $filetype=pathinfo($file, PATHINFO_EXTENSION );
        $content = $service->download($path, $persuratan->file_surat_generated, 'memo.docx');
        $templateProcessor = new TemplateProcessor(public_path('uploads/temporary/memo.docx'));


        $values = [
            'nomor_surat' => $persuratan->nomor_surat,
            'perihal' => $persuratan->perihal,
            'tgl_surat' => $persuratan->tgl_surat,
            'pengirim' => ucwords(strtolower(($persuratan->pengirim->jabatan))),
            'nama_pengirim' => ucwords(strtolower(($persuratan->pengirim->nama)))
        ];

        $penerima = [];
        foreach ($persuratan->penerima as $key => $value) {
            array_push($penerima, ['jabatan' => ucwords(strtolower($value->jabatan))]);
        }


        if ($persuratan->is_approve == 1) {
            $urlAtasan = config('app.FE_URL') . 'surat/' . $persuratan->id . '?pegawai_id=' . $persuratan->pengirim->pegawai_id . '&flag=a2&signed=' . base64_encode($persuratan->created_at);
            QrCode::format('png')->size(1000)->generate($urlAtasan, public_path('qrcode/' . $persuratan->pengirim->pegawai_id . '.png'));

            $templateProcessor->setImageValue('ttd_atasan', array('path' => public_path('qrcode/' . $persuratan->pengirim->pegawai_id . '.png'), 'width' => 150, 'height' => 150, 'ratio' => false));
        } else {
            $templateProcessor->setValue('ttd_atasan', '');
        }


        $templateProcessor->cloneRowAndSetValues('jabatan', $penerima);

        $templateProcessor->setValues($values);
        $templateProcessor->saveAs(public_path('uploads/temporary/memo.docx'));
        return response()->download(public_path('uploads/temporary/memo.docx'));
    }

    public function simpanDisposisi(Request $request)
    {
        DB::beginTransaction();
        try {
            $id_disposisi = $this->uuid();
            $catatan = $request->input('catatan');
            $pegawaiSelected = $request->input('pegawaiSelected');
            $pegawaiSelected = json_decode($pegawaiSelected);

            foreach ($pegawaiSelected as $key => $value) {
                $id = $this->uuid();
                $paramsDisposisiDtl = [
                    'id' => $id,
                    'persuratan_disposisi_id' => $id_disposisi,
                    'pegawai_id' => $value->pegawai_id
                ];

                PersuratanDisposisiDtl::create($paramsDisposisiDtl);
            }

            PersuratanDisposisi::create([
                'id' => $id_disposisi,
                'persuratan_surat_id' => $request->input('persuratan_surat_id'),
                'catatan' => $catatan

            ]);
            $response = new Response(200, 'Berhasil di simpan', [], [], true);
            DB::commit();
            return $response->getResponse();
        } catch (\Exception $e) {
            DB::rollback();
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }
    public function simpanSuratKeluar(Request $request)
    {
        $user = (array) $request['auth_user'];
        $user['ip'] = $request['ip-address'];


        DB::beginTransaction();
        try {

            // update
            if ($request->input('id')) {
                // jika ada file upload

                $persuratan = PersuratanSurat::where('id', $request->input('id'))->first();
                $direktori = 'persuratan/';
                $persuratan->konten = $request->input('konten');
                $persuratan->ket_lampiran = $request->input('ket_lampiran');
                $generated = date('YmdHis') . '_' . str_replace("/", "_", $persuratan->nomor_surat) . '_lampiran.pdf';

                // jika ada file upload
                if ($request->hasFile('file_lampiran')) {

                    $upload = uploadFile($direktori, $request->file('file_lampiran'), $generated);
                    if (!$upload['status']) {
                        $response = new ResponseCreator(500, $upload['message'], [], $upload);

                        $data = [
                            'data_sebelum' => json_encode($request->all()),
                            'data_sesudah' => json_encode($upload),
                        ];
                        //melakukan pencatatan logActivity
                        //mengunakan queue 
                        $log =  new LogWriter($user, 'Update Surat Masuk', 'gagal file tidak ada', 'gagal', $request->all(), null, 'update inbox letter');

                        return $response->getResponse();
                    }


                    $fileDeleted = $persuratan->file_surat_generated;
                    // update ke database
                    $persuratan->path_file = $direktori;
                    $persuratan->file_surat_original = $upload['file_asli'];
                    $persuratan->file_surat_generated = $upload['file_generated'];
                    if (file_exists($direktori . $fileDeleted)) {
                        unlink($direktori . $fileDeleted);
                    }
                }

                $persuratan->save();
                //mencata log activity
                $log =  new LogWriter($user, 'Update Surat Masuk', 'berhasil update Surat masuk', 'success', $request->all(), null, 'simpanSuratKeluar');
                $this->dispatch($log);
            } else {

                // insert
                $id_surat = $this->uuid();
                $pengirimSelected = $request->input('pengirimSelected');

                $paramsDrafter = [
                    'id' => $this->uuid(),
                    'pegawai_id' => $request->input('pegawai_id'),
                    'nama' => $request->input('nama'),
                    'nip' => $request->input('nip'),
                    'position_status_code' => $request->input('position_status_code'),
                    'unit_id' => $request->input('unit_pegawai_id'),
                    'unit_kerja' => $request->input('unit_pegawai'),
                    'jabatan' => $request->input('jabatan'),
                    'persuratan_surat_id' => $id_surat,
                    'flag' => 1
                ];


                // input drafter
                PersuratanSuratDtl::create($paramsDrafter);

                // input pengirim
                if ($pengirimSelected) {
                    $pengirimSelected = json_decode($pengirimSelected);
                    foreach ($pengirimSelected as $key => $val) {
                        $paramsPengirim = [
                            'id' => $this->uuid(),
                            'pegawai_id' => $val->pegawai_id,
                            'nama' => $val->nama_lengkap,
                            'nip' => $val->nip,
                            'position_status_code' => $val->kelas_jabatan,
                            'unit_id' => $val->master_unit_tree_id,
                            'manjab_draft_id' => $val->last_manjab_draft_id,
                            'unit_kerja' => $val->nama_unit,
                            'jabatan' => $val->nama_jabatan,
                            'persuratan_surat_id' => $id_surat,
                            'flag' => 2
                        ];

                        PersuratanSuratDtl::create($paramsPengirim);
                    }
                }

                // input penerima
                $penerimaSelected = $request->input('penerimaSelected');
                if ($penerimaSelected) {
                    $penerimaSelected = json_decode($penerimaSelected);
                    foreach ($penerimaSelected as $key => $val) {
                        $paramsPenerima = [
                            'id' => $this->uuid(),
                            'pegawai_id' => $val->pegawai_id,
                            'nama' => $val->nama_lengkap,
                            'nip' => $val->nip,
                            'position_status_code' => $val->kelas_jabatan,
                            'unit_id' => $val->master_unit_tree_id,
                            'manjab_draft_id' => $val->last_manjab_draft_id,
                            'unit_kerja' => $val->nama_unit,
                            'jabatan' => $val->nama_jabatan,
                            'persuratan_surat_id' => $id_surat,
                            'flag' => 3
                        ];

                        PersuratanSuratDtl::create($paramsPenerima);
                    }
                }

                // input mengetahui
                $mengetahuiSelected = $request->input('mengetahuiSelected');
                if ($mengetahuiSelected) {
                    $mengetahuiSelected = json_decode($mengetahuiSelected);
                    foreach ($mengetahuiSelected as $key => $val) {
                        $paramsMengetahui = [
                            'id' => $this->uuid(),
                            'pegawai_id' => $val->pegawai_id,
                            'nama' => $val->nama_lengkap,
                            'nip' => $val->nip,
                            'position_status_code' => $val->kelas_jabatan,
                            'unit_id' => $val->master_unit_tree_id,
                            'manjab_draft_id' => $val->last_manjab_draft_id,
                            'unit_kerja' => $val->nama_unit,
                            'jabatan' => $val->nama_jabatan,
                            'persuratan_surat_id' => $id_surat,
                            'flag' => 4
                        ];

                        PersuratanSuratDtl::create($paramsMengetahui);
                    }
                }


                // $nomorSurat = $this->_generateNomorSurat(
                //     [
                //         "unit_id" => $request->input('unit_id'),
                //         "unit_name" => $request->input('unit_name'),
                //         "kode_unit" => $request->input('kode_unit'),
                //         "klasifikasi_surat_id" => $request->input('klasifikasi_surat_id'),
                //         "manjab_draft_id" => $request->input('unit_manjab_draft_id')
                //     ]
                // );

                $paramsPersuratan = [
                    'id' => $id_surat,
                    'unit_id' => $request->input('unit_id'),
                    'kode_unit' => $request->input('kode_unit'),
                    'klasifikasi_surat_id' => $request->input('klasifikasi_surat_id'),
                    'perihal' => $request->input('perihal'),
                    'status_surat_id' => $request->input('status_surat_id'),
                    'nomor_surat' => "DRAFT",
                    'konten' => $request->input('konten'),
                    'ket_lampiran' => $request->input('ket_lampiran'),

                ];

                // jika ada file upload
                if ($request->hasFile('file_lampiran')) {
                    $direktori = 'persuratan/';
                    $generated = date('YmdHis') . '_' . str_replace("/", "_", $paramsPersuratan['nomor_surat']) . '_lampiran.pdf';
                    $upload = uploadFile($direktori, $request->file('file_lampiran'), $generated);
                    if (!$upload['status']) {
                        $log =  new LogWriter($user, 'Create Surat Masuk', 'gagal membuat karena file tidak ada', 'failed', $request->all(), null, 'simpanSuratKeluar');
                        $response = new Response(500, $upload['message'], [], $upload, true);
                        return $response->getResponse();
                    }
                    $paramsPersuratan['path_file'] = $direktori;
                    $paramsPersuratan['file_surat_original'] = $upload['file_asli'];
                    $paramsPersuratan['file_surat_generated'] = $upload['file_generated'];
                }

                // simpan persuratan
                $surat = PersuratanSurat::create($paramsPersuratan);

                $nomorSurat = PersuratanNomorSurat::where('unit_id', $paramsPersuratan['unit_id'])
                    ->where('kode_unit', $paramsPersuratan['kode_unit'])
                    ->where('tahun', date('Y'))
                    ->where('klasifikasi_surat_id', $paramsPersuratan['klasifikasi_surat_id'])
                    ->first();

                // simpan nomor surat
                $nomor = $nomorSurat ? $nomorSurat['nomor'] : 0;

                if ($nomorSurat) {
                    $nomorSurat->nomor = $nomor + 1;
                    $nomorSurat->save();
                } else {
                    $id = $this->uuid();
                    $data = PersuratanNomorSurat::create([
                        'id' => $id,
                        'unit_name' => $request->input('unit_name'),
                        'unit_id' => $request->input('unit_id'),
                        'kode_unit' => $request->input('kode_unit'),
                        'tahun' => date('Y'),
                        'klasifikasi_surat_id' => $request->input('klasifikasi_surat_id'),
                        'nomor' => $nomor + 1
                    ]);
                }
                $log =  new LogWriter($user, 'Create Surat Masuk', 'Berhasil membuat Surat Keluar', 'berhasil', $request->all(), null, 'simpanSuratKeluar');
                $this->dispatch($log);
            }




            DB::commit();
            $response = new Response(200, 'Berhasil di simpan', [], [], true);
            $_record = [
                'surat' => $surat,
                'nomer' => $nomorSurat
            ];

            return $response->getResponse();
        } catch (\Exception $e) {
            DB::rollback();
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            $log =  new LogWriter($user, 'Server is not responding', 'Ada Kesalahan Sebuah Server', 'failed', $request->all(), null, 'simpanSuratKeluar');
            return $response->getResponse();
        }
    }
    public function SendReview(Request $request)
    {
        //set akses eligible
        $eligible = ['2', '3', ''];
        $user = (array) $request['auth_user'];
        $user['ip'] =  $request['ip-address'];
        //get id form persuratanmodel
        //validation 
        $valid = Validator::make($request->all(), [
            'id' => 'required'
        ]);


        if ($valid->fails()) {
            $log =  new LogWriter($user, 'id is required', 'ada kesalahan inputan User', 'failed', $request->all(), null, 'sendRiview');
            $this->dispatch($log);
            $response = new Response(404, 'id is required', [], [], false);
            return $response->getResponse();
        }
        try {
            $surat = PersuratanSurat::where('id', $request['id'])->whereIn('is_approve', $eligible)->first();
            $surat->is_approve = 3;
            //update
            $surat->save();

            // melakukan notofikasi

            $notify =  new Notifikasi();
            $riwayat = $notify->riw_izin()->first();

            // get penerima 

            $pengirim = $surat->penerima()->first();
            $penerima = $surat->pengirim()->first();

            $judul = 'Riview Surat';
            $keterangan = 'There review letter from ' . $pengirim->nama . ' - ' . $pengirim->nip;
            $params = [
                'judul' =>  $judul,
                'form_pegawai_id' =>  $pengirim->pegawai_id,
                'is_read' => null,
                'keterangan' =>  $keterangan,
                'to_pegawai_id' => $penerima->pegawai_id,
                'riwayat' => $riwayat,
                'riwayat_id' => $surat->id,
                'catatan_penolakan' => null
            ];


            $notify =  new SendNotificationIms($params);
            $this->dispatch($notify);
        } catch (\Exception $e) {
            $log =  new LogWriter($user, 'Server is not responding', 'Ada Kesalahan Sebuah Server', 'failed', $request->all(), null, 'sendRiview');
            $this->dispatch($log);
            $response = new Response(500, 'internal sever errors', [], [], false);
            return $response->getResponse();
        }
        //write log
        $log =  new LogWriter($user, 'Pengajuan Review Succes', 'Ada Kesalahan Sebuah Server', 'failed', $request->all(), $surat, 'sendRiview');
        $this->dispatch($log);
        //feed back for response
        $response =  new Response(200, 'persuratan', 'pengajuan review berhasi', [], [], false);
        return $response->getResponse();
    }

    public function readStatus(Request $request)
    {
        // function untuk update reads status jika user melakukan reads

        $user = (array) $request['auth_user'];
        $user['ip'] =  $request['ip-address'];

        $valid = Validator::make($request->all(), [
            'id' => 'required'
        ]);



        if ($valid->fails()) {
            $log =  new LogWriter($user, 'id is required', 'terdapat kesalaha dalam inputan user', 'failed', $request->all(), null, 'ReadStatus');
            $this->dispatch($log);
            $response = new Response(402, 'id is required', [], [], false);
            return $response->getResponse()->json();
        }

        try {
            $surat = PersuratanSuratDtl::where('persuratan_surat_id', $request->id)->first();

            // validate apakah surat sudah diapprove
            if ($surat->is_read == 1) {
                $log =  new LogWriter($user, 'The letter already has Approved', 'failed', $request->all(), null, 'ReadStatus');
                $this->dispatch($log);
                $response = new Response(400, 'The letter already has Approved', [], [], false);
                return $response->getResponse();
            }

            if ($surat == null) {
                $log =  new LogWriter($user, 'Surat is notfound', 'failed', $request->all(), null, 'ReadStatus');
                $this->dispatch($log);
                $response = new Response(404, 'id is required', [], [], false);
                return $response->getResponse()->json();
            }
            $surat->is_read = 1;
            //update
            $surat->save();
            $log =  new LogWriter($user, 'berhasil read letter', 'success', $request->all(), null, 'ReadStatus');
            $this->dispatch($log);
            $response = new Response(200, 'berhasil read letter', [], [], false);
            return $response->getResponse();
        } catch (\Exception $e) {
            $log =  new LogWriter($user, 'Internal server errors', 'terdapat kesalahan server', 'failed', $request->all(), null, 'ReadStatus');
            $this->dispatch($log);
            $response = new Response(500, 'internal sever errors', [], [], false);
            return $response->getResponse();
        }
    }

    public function suratMasukEksternal()
    {
    }



    protected function _getDivision($id)
    {
        $explode = explode('.', $id);
        $idUnit = '';
        $eligible = ['PST001', 'PST002'];
        $data = [];
        for ($i = 0; $i < count($explode) - 1; $i++) {
            $idUnit .= $explode[$i] . '.';
            $unit = $this->unit($idUnit);
            if (in_array($unit->kelas_jabatan, $eligible)) {
                $value = [
                    'unit_id' => $unit->tree_id,
                    'unit_name' => $unit->unit,
                    'kode_unit' => $unit->unit_code,
                    'manjab_draft_id' => $unit->manjab_draft_id
                ];

                array_push($data, $value);
            }

            // AMBIL UNIT SAMPAI LEVEL DEPARTEMEN
            if ($unit->kelas_jabatan == 'PST002') {
                break;
            }
        }

        return $data;
    }

    protected function unit($id)
    {
        $unit = MasterUnit::where('tree_id', $id)->first();
        return $unit;
    }

    public function downloadTemplateSurat(Request $request)
    {
        $file_path = public_path('template/SURAT/MEMO.docx');
        return response()->download($file_path);
    }

    public function generateNomorSurat(Request $request)
    {
        try {
            $params = [
                "unit_id" => $request->input('unit_id'),
                "unit_name" => $request->input('unit_name'),
                "kode_unit" => $request->input('kode_unit'),
                "klasifikasi_surat_id" => $request->input('klasifikasi_surat_id'),
                "manjab_draft_id" => $request->input('manjab_draft_id')
            ];

            $nomor = $this->_generateNomorSurat($params);
            $response = new Response(200, 'Berhasil di dapat', $nomor, [], true);
            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    protected function _generateNomorSurat($params)
    {
        $klasifikasiCode = DB::table('persuratan_klasifikasi_surat')->where('id', $params['klasifikasi_surat_id'])->first();
        $klasifikasiCode = $klasifikasiCode->kode;
        $nomorSurat = PersuratanNomorSurat::where('unit_id', $params['unit_id'])
            ->where('kode_unit', $params['kode_unit'])
            ->where('tahun', date('Y'))
            ->where('klasifikasi_surat_id', $params['klasifikasi_surat_id'])
            ->first();


        // jika tidak ada nomor surat
        if (!$nomorSurat) {
            $id = $this->uuid();
            PersuratanNomorSurat::create([
                'id' => $this->uuid(),
                'kode_unit' => $params['kode_unit'],
                'unit_id' => $params['unit_id'],
                'unit_name' => $params['unit_name'],
                'manjab_draft_id' => $params['manjab_draft_id'],
                'klasifikasi_surat_id' => $params['klasifikasi_surat_id'],
                'nomor' => 0,
                'tahun' => date('Y')
            ]);
        }

        $ims = '';
        if ($klasifikasiCode == 'SD') {
            $ims = '/IMS';
        }

        $nomor = $nomorSurat ? $nomorSurat->nomor : 0;
        $generateNomor = $klasifikasiCode . '-' . str_pad($nomor + 1, 3, 0, STR_PAD_LEFT) . '/' . $params['kode_unit'] . $ims . '/' . date('Y');

        return ['full_nomor' => $generateNomor, 'nomor' => $nomor];
    }

    public function resetNomorSurat(Request $request)
    {
        try {

            $res = PersuratanNomorSurat::where('id', $request->input('id'))->first();
            $res->nomor = $request->input('nomor');
            $res->save();
            $response = new Response(200, 'Berhasil di simpan', [], [], true);
            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getDivisiPegawai(Request $request)
    {
        try {

            $peg = Pegawai::where('id', $request->input('pegawai_id'))->first();
            $unit = $this->_getDivision($peg->master_unit_tree_id);

            $response = new Response(200, 'Berhasil di dapat', $unit, [], true);
            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getSuratMasuk(Request $request)
    {
        try {
            $page = $request->input('is_mobile') ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');
            $data = PersuratanSuratDtl::with('surat.pengirim')->where('pegawai_id', $request->input('pegawai_id'))
                ->leftJoin('persuratan_surat', 'persuratan_surat.id', 'persuratan_surat_dtl.persuratan_surat_id')
                ->selectRaw('persuratan_surat_dtl.*, ROW_NUMBER() OVER (order by persuratan_surat_dtl.created_at desc) nomor')
                ->where('is_approve', 1)
                ->whereIn('flag', [3, 4])
                ->paginate($page);

            $response = new Response(200, 'Berhasil di dapat', $data, [], true);
            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getSuratMasukDivisi(Request $request)
    {
        try {
            $divisi = [];
            $idUnit = '';
            $explode = explode('.', $request->input('unit_id'));
            for ($i = 0; $i < count($explode) - 1; $i++) {
                $idUnit .= $explode[$i] . '.';
                $unit = $this->unit($idUnit);
                if (in_array($unit->kelas_jabatan, ['PST001'])) {
                    $value = [
                        'unit_id' => $unit->tree_id,
                        'unit_name' => $unit->nama,
                        'kode_unit' => $unit->unit_code
                    ];

                    array_push($divisi, $value);
                }

                // HANYA AMBIL DIVISI
                if ($unit->kelasjabatan == 'PST001') {
                    break;
                }
            }

            $divisi_id = $divisi ? $divisi[0]['unit_id'] : '';
            $page = $request->input('is_mobile') ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');


            $data = PersuratanSurat::with('pengirim')
                ->selectRaw("distinct persuratan_surat.id,persuratan_surat.klasifikasi_surat_id, ROW_NUMBER() OVER (partition by persuratan_surat_id, persuratan_surat_dtl.unit_id order by persuratan_surat_dtl.created_at desc) nomor, perihal, nomor_surat")
                ->where('persuratan_surat_dtl.unit_id', 'like', $divisi_id . '%')
                ->leftJoin('persuratan_surat_dtl', 'persuratan_surat_dtl.persuratan_surat_id', 'persuratan_surat.id')
                ->where('is_approve', 1)
                ->whereIn('flag', [3, 4])
                ->paginate($page);

            $response = new Response(200, 'Berhasil di dapat', $data, [], true);
            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getDisposisiKeluar(Request $request)
    {
        try {
            $page = $request->input('is_mobile') ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');
            $data = PersuratanSuratDtl::with(['surat' => function ($query) {
                $query->with('pengirim')->with('disposisi');
            }])
                ->where('persuratan_surat_dtl.pegawai_id', $request->input('pegawai_id'))
                ->where('persuratan_surat_dtl.flag', 3)
                ->whereIn('persuratan_surat_dtl.position_status_code', ['PST000', 'PST001', 'PST002'])
                ->selectRaw("persuratan_surat_dtl.persuratan_surat_id, ROW_NUMBER() OVER (order by persuratan_surat_dtl.created_at desc) nomor, DATE_FORMAT(persuratan_surat_dtl.created_at,'%d/%m/%Y') tgl_surat")
                ->where('pegawai_id', $request->input('pegawai_id'))
                ->where('persuratan_surat.is_approve', 1)
                ->leftJoin('persuratan_surat', 'persuratan_surat.id', 'persuratan_surat_dtl.persuratan_surat_id')
                ->paginate($page);

            $response = new Response(200, 'Berhasil di dapat', $data, [], true);
            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getDisposisiMasuk(Request $request)
    {
        try {
            $page = $request->input('is_mobile') ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');
            $data = PersuratanDisposisi::leftJoin('persuratan_disposisi_dtl', 'persuratan_disposisi_dtl.persuratan_disposisi_id', 'persuratan_disposisi.id')
                ->where('persuratan_disposisi_dtl.pegawai_id', $request->input('pegawai_id'))
                ->with('surat')
                ->selectRaw("ROW_NUMBER() OVER (order by persuratan_disposisi.created_at desc) nomor, persuratan_disposisi.persuratan_surat_id, persuratan_disposisi.catatan, persuratan_disposisi.id, concat(empname, ' (',empcode,')') as pengirim, DATE_FORMAT(persuratan_disposisi.created_at,'%d/%m/%Y') tgl_disposisi ")
                ->leftJoin('tblemployee', 'tblemployee.pegawai_id', 'persuratan_disposisi.pengirim_id')
                ->paginate($page);

            $response = new Response(200, 'Berhasil di dapat', $data, [], true);
            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    protected function getSuratKeluarMobile($params, $page)
    {
        $data = PersuratanSurat::with('drafter')
            ->selectRaw("distinct persuratan_surat.id,persuratan_surat.perihal,persuratan_surat.nomor_surat,DATE_FORMAT(persuratan_surat.created_at,'%d/%m/%Y') tgl_surat, right(persuratan_surat.file_surat_original,3) as ext, is_approve, klasifikasi_surat_id ")
            ->leftJoin('persuratan_surat_dtl', 'persuratan_surat_dtl.persuratan_surat_id', 'persuratan_surat.id')
            ->orderBy('persuratan_surat.created_at', 'desc')
            ->whereIn('persuratan_surat_dtl.flag', [1, 2])
            ->where('persuratan_surat_dtl.pegawai_id', $params['pegawai_id'])
            ->paginate($page);
        return $data;
    }

    public function getSuratKeluar(Request $request)
    {
        try {
            $divisi = [];
            $idUnit = '';
            $explode = explode('.', $request->input('unit_id'));
            for ($i = 0; $i < count($explode) - 1; $i++) {
                $idUnit .= $explode[$i] . '.';
                $unit = $this->unit($idUnit);
                if (in_array($unit->kelas_jabatan, ['PST001'])) {
                    $value = [
                        'unit_id' => $unit->tree_id,
                        'unit_name' => $unit->nama,
                        'kode_unit' => $unit->unit_code
                    ];

                    array_push($divisi, $value);
                }

                // AMBIL UNIT SAMPAI LEVEL DEPARTEMEN
                if ($unit->kelas_jabatan == 'PST002') {
                    break;
                }
            }

            $divisi_id = $divisi[0]['unit_id'];
            $page = $request->input('is_mobile') ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');

            if ($request->input('is_mobile')) {
                $params = [
                    'pegawai_id' => $request->input('pegawai_id'),
                    'unit_id' => $request->input('unit_id')
                ];
                $data = $this->getSuratKeluarMobile($params, $page);
            } else {
                $data = PersuratanSurat::with(['drafter', 'penerima', 'pengirim'])
                    ->selectRaw("distinct persuratan_surat.*,ROW_NUMBER() OVER (order by persuratan_surat.created_at desc) nomor,  DATE_FORMAT(persuratan_surat.created_at,'%d/%m/%Y') tgl_surat, right(persuratan_surat.file_surat_original,3) as ext ")
                    ->leftJoin('persuratan_surat_dtl', 'persuratan_surat_dtl.persuratan_surat_id', 'persuratan_surat.id')
                    ->whereIn('persuratan_surat_dtl.flag', [1, 2])
                    ->where('persuratan_surat_dtl.pegawai_id', $request->input('pegawai_id'))
                    ->paginate($page);
            }



            $response = new Response(200, 'Berhasil di dapat', $data, [], true);
            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getSuratKeluarDivisi(Request $request)
    {
        try {
            $divisi = [];
            $idUnit = '';
            $explode = explode('.', $request->input('unit_id'));
            for ($i = 0; $i < count($explode) - 1; $i++) {
                $idUnit .= $explode[$i] . '.';
                $unit = $this->unit($idUnit);
                if (in_array($unit->kelas_jabatan, ['PST001'])) {
                    $value = [
                        'unit_id' => $unit->tree_id,
                        'unit_name' => $unit->nama,
                        'kode_unit' => $unit->unit_code
                    ];

                    array_push($divisi, $value);
                }

                // AMBIL UNIT SAMPAI LEVEL DEPARTEMEN
                if ($unit->kelas_jabatan == 'PST001') {
                    break;
                }
            }

            $divisi_id = $divisi[0]['unit_id'];
            $page = $request->input('is_mobile') ? config('app.MOBILE_PER_PAGE') : config('app.WEB_PER_PAGE');
            $data = PersuratanSurat::with(['drafter', 'penerima', 'pengirim'])
                ->selectRaw("persuratan_surat.*,ROW_NUMBER() OVER (order by persuratan_surat.created_at desc) nomor,  DATE_FORMAT(created_at,'%d/%m/%Y') tgl_surat")
                ->where('persuratan_surat.unit_id', 'like', $divisi_id . '%')
                ->paginate($page);


            $response = new Response(200, 'Berhasil di dapat', $data, [], true);
            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getNomorSurat(Request $request)
    {
        try {
            $data = PersuratanNomorSurat::where('unit_id', $request->input('unit_id'))
                ->where('kode_unit', $request->input('kode_unit'))
                ->where('tahun', $request->input('tahun'))
                ->where('klasifikasi_surat_id', $request->input('klasifikasi_surat_id'))
                ->first();

            // jika tidak ada datanya maka insert
            if (!$data) {
                $id = $this->uuid();
                $data = PersuratanNomorSurat::create([
                    'id' => $id,
                    'unit_name' => $request->input('unit_name'),
                    'unit_id' => $request->input('unit_id'),
                    'kode_unit' => $request->input('kode_unit'),
                    'tahun' => $request->input('tahun'),
                    'klasifikasi_surat_id' => $request->input('klasifikasi_surat_id'),
                    'nomor' => 0
                ]);
                $data->id = $id;
            }

            $nomor = $data->nomor;
            $response = new Response(200, 'Berhasil di dapat', ['id' => $data->id, 'nomor' => $nomor], [], true);
            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(500, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }
}
