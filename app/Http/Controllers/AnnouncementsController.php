<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Jobs\LogWriter;
use App\Models\Notifikasi;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\GlobalClass\Response;
use App\Models\Announcements;
use Illuminate\Support\Facades\DB;



class AnnouncementsController extends Controller
{
    public function getAnnoun(Request $request)
    {
        try {

            $data = Announcements::selectRaw("id, content, title, attechment, date_format(created_at, '%d/%m/%Y') as tgl_created, ROW_NUMBER() OVER(ORDER BY created_at DESC) AS row_num")
                ->orderBy('created_at', 'DESC')
                ->paginate(10);

            // write log
            $log = new LogWriter(null, 'Success get Announcements', 'berhasi mendapatkan data pengumuman', 'success', $request->all(), $data, 'getAnnouncement');
            $this->dispatch($log);
            $response = new Response(200, 'Berhasil di dapat', $data, [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $log = new LogWriter(null, 'Internal server Error', 'Ada kesalahan Server', 'failed', $request->all(), null, 'getAnnouncement');
            $this->dispatch($log);
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function getAnnounLimit(Request $request)
    {
        try {

            $data = Announcements::selectRaw("id, content, title, attechment, date_format(created_at, '%d/%m/%Y') as tgl_created ")
                ->orderBy('created_at', 'DESC')
                ->limit(3)
                ->get();
            $log = new LogWriter(null, 'Success get Limit Announcements', 'berhasi mendapatkan data limit  pengumuman', 'success', $request->all(), $data, 'getAnnouncement');
            $this->dispatch($log);
            $response = new Response(200, 'Berhasil di dapat', $data, [], true);

            return $response->getResponse();
        } catch (\Exception $e) {
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }

    public function simpanAnnouncement(Request $request)
    {
        // dd($request->file());
        // exit;
        DB::beginTransaction();
        try {
            $request->validate([

                'file' => 'nullable|mimes:pdf,xlx,csv|max:2048',

            ]);

            $file = $request->file('file');
            $nama_file = null;

            if ($file) {

                $nama_file = date('yymd') . "_" . $file->hashName();
                // isi dengan nama folder tempat kemana file diupload
                $tujuan_upload = 'announcements';
                $file->move($tujuan_upload, $nama_file);
            }
            $params = [
                'title' => $request->input('title'),
                'content' => $request->input('content'),

            ];

            $params['excerpt'] = Str::limit(strip_tags($request->input('content')), 100);
            $id = $this->uuid();
            $announcement = new Announcements();
            $announcement->id = $id;
            $announcement->title = $params['title'];
            $announcement->content = $params['content'];
            $announcement->excerpt = $params['excerpt'];
            $announcement->attechment = $nama_file;
            $announcement->save();

            $data =  Announcements::find($id);

            $request = $request->except('file');

            $log = new LogWriter(null, 'Success Saved Announcements', 'berhasi menyimpan  pengumuman', 'success', $request, $data, 'getAnnouncement');
            $this->dispatch($log);

            DB::commit();
            $response = new Response(200, 'Data Berhasil Di simpan', [], [], true);
        } catch (\Exception $e) {
            DB::rollback();
            $errors[] = $e->getMessage();
            $response = new Response(500, 'Terjadi Kesalahan pada server', [], $errors, false);
        }
        return $response->getResponse();
    }

    public function updateAnnouncement(Request $request)
    {
        // dd($request->file());
        // exit;
        DB::beginTransaction();
        try {

            $file = $request->file('file');
            // dd($file);
            // exit;
            $nama_file = null;
            if ($file) {
                $nama_file = date('yymd') . "_" . $file->hashName();
                // isi dengan nama folder tempat kemana file diupload
                $tujuan_upload = 'announcements';
                $file->move($tujuan_upload, $nama_file);
            }
            $params = [
                'title' => $request->input('title'),
                'content' => $request->input('content'),

            ];

            $params['excerpt'] = Str::limit(strip_tags($request->input('content')), 100);

            $announcement = Announcements::where('id', $request->id)->first();
            // dd($id);
            // exit;
            $announcement->title = $params['title'];
            $announcement->content = $params['content'];
            $announcement->excerpt = $params['excerpt'];
            $announcement->attechment = $nama_file ? $nama_file : $announcement->attechment;
            $announcement->update();

            $data =  Announcements::find($request->id);

            $request = $request->except('file');

            $log = new LogWriter(null, 'Success Update Announcements', 'berhasi Memperbarui  pengumuman', 'success', $request, $data, 'getAnnouncement');
            $this->dispatch($log);

            DB::commit();
            $response = new Response(200, 'Data Berhasil Di simpan', [], [], true);
        } catch (\Exception $e) {
            DB::rollback();
            $errors[] = $e->getMessage();
            $response = new Response(500, 'Terjadi Kesalahan pada server', [], $errors, false);
        }
        return $response->getResponse();
    }

    public function delAnnouncement(Request $request)
    {
        DB::beginTransaction();
        try {
            $id = $request->input('id');
            $riw = Announcements::where('id', '=', $id)->first();
            $riw->deleted_at = Carbon::now();
            $riw->save();


            // $notif = Notifikasi::where('riwayat_id', '=', $id)->first();
            // $notif->deleted_at = Carbon::now();
            // $notif->save();

            $log = new LogWriter(null, 'Success Saved Announcements', 'berhasi menyimpan  pengumuman', 'success', $request->all(), $riw, 'getAnnouncement');
            $this->dispatch($log);

            $response = new Response(200, 'Berhasil di hapus', [], [], true);
            DB::commit();
            return $response->getResponse();
        } catch (\Exception $e) {
            DB::rollback();
            $response = new Response(200, 'Ada kesalahan server', [], $e->getMessage(), false);
            return $response->getResponse();
        }
    }
}
