<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GlobalClass\Response;
use App\Models\TblEmployee;
use App\Models\Notifikasi;
use App\Services\FtpService;
use Illuminate\Support\Facades\DB;

class PreviewController extends Controller
{

    public function index(Request $request){
       $direktori = $request->input('direktori');

        $path = config('app.FTP_FOLDER');
        $file = base64_decode($request->input('file'));
        switch ($direktori) {
              case 'cuti':
                $path.='ess/cuti/'.$file;
                break;
              default:
                # code...
                break;
            }
            $service = new FtpService();
            $filetype=pathinfo($file, PATHINFO_EXTENSION );
            $content = $service->readFile($path);
            if(strtolower($filetype) == 'pdf'){
                $type = 'Content-Type: application/pdf';
            }
            else{
                  $type = 'Content-Type: image/'.$filetype;
            }

           header($type);
           echo $content;
    }

    
}

?>