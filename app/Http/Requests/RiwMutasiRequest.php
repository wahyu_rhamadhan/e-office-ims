<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RiwMutasiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'pegawai_id' => 'required',
            'unit_name_prev' => 'required',
            'unit_tree_id_prev' => 'required',
            'unit_manjab_draft_id_prev' => 'required',
            'unit_name_aft' => 'required',
            'unit_tree_id_aft' => 'required',
            'unit_manjab_draft_id_aft' => 'required',
            'no_sk' => 'required',
            'tgl_mulai' => 'date'
        ];
    }
}
