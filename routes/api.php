<?php

use App\Http\Controllers\AnnouncementsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PegawaiController;
use App\Http\Controllers\MasterController;
use App\Http\Controllers\IzinController;
use App\Http\Controllers\CutiController;
use App\Http\Controllers\PreviewController;
use App\Http\Controllers\PersuratanController;
use App\Http\Controllers\CapegController;
use App\Http\Controllers\PkwtController;
use App\Http\Controllers\PkwttController;
use App\Http\Controllers\SppdController;
use App\Http\Controllers\MutasiController;
use App\Models\Announcements;

use App\Http\Controllers\PDFController;
use App\Http\Controllers\API\RiwMutasiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('tes', [PegawaiController::class, 'tes'])->name('pegawai.tes');
Route::get('preview-file', [PreviewController::class, 'index'])->name('preview.index');
//generate pdf
Route::get('persuratan/cetak-surat', [PDFController::class, 'index']);

Route::get('mutasi/cetak-mutasi', [PDFController::class, 'cetakMutasi']);
Route::get('cetak-disposisi', [PDFController::class, 'cetakDisposisi'])->name('persuratan.cetakDisposisi');

Route::group(['prefix' => 'master'], function () {
    Route::get('get-bank', [MasterController::class, 'getBank'])->name('master.getBank');
    Route::get('get-jabatan-non-str', [MasterController::class, 'getJabatanNonStr'])->name('master.getJabatanNonStr');
    Route::get('get-unit', [MasterController::class, 'getUnit'])->name('master.getUnit');
    Route::get('get-draft-manjab', [MasterController::class, 'getDraftManjab'])->name('master.getDraftManjab');
    Route::post('simpan-draft-manjab', [MasterController::class, 'simpanDraftManjab'])->name('master.simpanDraftManjab');
    Route::post('simpan-draft-unit', [MasterController::class, 'simpanDraftUnit'])->name('master.simpanDraftUnit');
    Route::post('simpan-manjab-jabatan', [MasterController::class, 'simpanManjabJabatan'])->name('master.simpanManjabJabatan');
    Route::post('release-draft-manjab', [MasterController::class, 'releaseDraftManjab'])->name('master.releaseDraftManjab');
    Route::post('hapus-draft-unit', [MasterController::class, 'hapusDraftUnit'])->name('master.hapusDraftUnit');
    Route::post('hapus-draft-manjab', [MasterController::class, 'hapusDraftManjab'])->name('master.hapusDraftManjab');
    Route::get('get-draft-unit', [MasterController::class, 'getDraftUnit'])->name('master.getDraftUnit');
    Route::get('get-manjab-struktural', [MasterController::class, 'getManjabStruktural'])->name('master.getManjabStrutural');
    Route::post('release-draft-jabatan', [MasterController::class, 'releaseDraftJabatan'])->name('master.releaseDraftJabatan');
});


Route::group(['prefix' => 'pegawai'], function () {
    Route::get('get-notifikasi', [PegawaiController::class, 'getNotifikasi'])->name('pegawai.getNotifikasi');
    Route::post('delete-all-notifikasi', [PegawaiController::class, 'deleteAllNotifikasi'])->name('pegawai.deleteAllNotifikasi');
    Route::get('get-all-approval', [PegawaiController::class, 'getAllApproval'])->name('pegawai.getAllApproval');
    Route::get('master-status-pegawai', [PegawaiController::class, 'masterStatusPegawai'])->name('pegawai.masterStatusPegawai');
    Route::post('update-status-notifikasi', [PegawaiController::class, 'updateStatusNotifikasi'])->name('pegawai.updateStatusNotifikasi');
    Route::get('get-notifikasi-detail', [PegawaiController::class, 'getNotifikasiDetail'])->name('pegawai.getNotifikasiDetail');
    Route::get('get-detail', [PegawaiController::class, 'getDetailPegawai'])->name('pegawai.getDetailPegawai');
    Route::get('get-pegawai', [PegawaiController::class, 'getPegawai'])->name('pegawai.getPegawai');
    Route::get('get-pegawai-mobile', [PegawaiController::class, 'getPegawaiMobile'])->name('pegawai.getPegawaiMobile');
    Route::get('get-pegawai-by-id', [PegawaiController::class, 'getPegawaiById'])->name('pegawai.getPegawaiById');
    Route::get('get-pegawai-struktural', [PegawaiController::class, 'getPegawaiStruktural'])->name('pegawai.getStruktural');
    Route::get('get-pegawai-struktural-special', [PegawaiController::class, 'getPegawaiStrukturalSpecial'])->name('pegawai.getStrukturalSpecial');
    Route::get('get-pegawai-struktural-mobile', [PegawaiController::class, 'getPegawaiStrukturalMobile'])->name('pegawai.getStrukturalMobile');
    Route::post('simpan-pegawai', [PegawaiController::class, 'simpanPegawai'])->name('pegawai.simpanPegawai');
    Route::post('update-pegawai', [PegawaiController::class, 'updatePegawai'])->name('pegawai.updatePegawai');
    Route::get('digi-sign-info', [PegawaiController::class, 'digiSignInfo'])->name('pegawai.digiSignInfo');
});

Route::group(['prefix' => 'capeg'], function () {
    Route::get('get-capeg', [CapegController::class, 'getCapeg'])->name('capeg.getCapeg');
    Route::get('detail-capeg', [CapegController::class, 'detailCapeg'])->name('capeg.detailCapeg');
    Route::post('simpan-capeg', [CapegController::class, 'simpanCapeg'])->name('capeg.simpanCapeg');
    Route::post('del-capeg', [CapegController::class, 'delCapeg'])->name('capeg.delCapeg');
});

Route::group(['prefix' => 'pkwt'], function () {
    Route::get('get-pkwt', [PkwtController::class, 'getPkwt'])->name('pkwt.getPkwt');
    Route::get('detail-pkwt', [PkwtController::class, 'detailPkwt'])->name('pkwt.detailPkwt');
    Route::post('simpan-pkwt', [PkwtController::class, 'simpanPkwt'])->name('pkwt.simpanPkwt');
    Route::post('del-pkwt', [PkwtController::class, 'delPkwt'])->name('pkwt.delPkwt');
});

Route::group(['prefix' => 'pkwtt'], function () {
    Route::get('get-pkwtt', [PkwttController::class, 'getPkwtt'])->name('pkwtt.getPkwtt');
    Route::get('detail-pkwtt', [PkwttController::class, 'detailPkwtt'])->name('pkwtt.detailPkwtt');
    Route::post('simpan-pkwtt', [PkwttController::class, 'simpanPkwtt'])->name('pkwtt.simpanPkwtt');
    Route::post('del-pkwtt', [PkwttController::class, 'delPkwtt'])->name('pkwtt.delPkwtt');
});

Route::group(['prefix' => 'izin'], function () {
    Route::get('get', [IzinController::class, 'getRiwIzin'])->name('izin.getRiwIzin');
    Route::get('get-detail', [IzinController::class, 'getDetail'])->name('izin.getDetail');
    Route::get('get-confirmed-by-satpam', [IzinController::class, 'getConfirmedBySatpam'])->name('izin.getConfirmedBySatpam');
    Route::get('izin-by-pegawai-id', [IzinController::class, 'izinByPegawaiId'])->name('izin.izinByPegawaiId');
    Route::get('izin-by-pegawai-id-mobile', [IzinController::class, 'izinByPegawaiIdMobile'])->name('izin.izinByPegawaiIdMobile');
    Route::get('get-summary-pegawai', [IzinController::class, 'getSummaryPegawai'])->name('izin.getSummaryPegawai');
    Route::get('get-approval', [IzinController::class, 'getApproval'])->name('izin.getApproval');
    Route::get('get-approval-mobile', [IzinController::class, 'getApprovalMobile'])->name('izin.getApprovalMobile');
    Route::get('generate-qr-code/{id}', [IzinController::class, 'generateQrCode'])->name('izin.generateQrCode');
    Route::put('update-izin/{id}', [IzinController::class, 'updateIzin'])->name('izin.updateIzin');
    // Route::put('approval/{id}', [IzinController::class, 'approval'])->name('izin.updateIzin');
    Route::post('simpan-izin', [IzinController::class, 'simpanIzin'])->name('izin.simpanIzin');
    Route::post('confirm-izin', [IzinController::class, 'confirmIzin'])->name('izin.confirmIzin');
    Route::delete('del-izin', [IzinController::class, 'delIzin'])->name('izin.delIzin');
    Route::post('export', [IzinController::class, 'exportIzin'])->name('izin.exportIzin');
    Route::post('export-konfirmasi-keluar', [IzinController::class, 'exportKonfirmasiKeluar'])->name('izin.exportKonfirmasiKeluar');
});

Route::group(['prefix' => 'cuti'], function () {
    Route::get('get', [CutiController::class, 'getCuti'])->name('cuti.getCuti');
    Route::get('get-status-saldo-cuti', [CutiController::class, 'getStatusSaldoCuti'])->name('cuti.getStatusSaldoCuti');
    Route::get('get-saldo-cuti', [CutiController::class, 'getSaldoCuti'])->name('cuti.getSaldoCuti');
    Route::get('get-pegawai-saldo-cuti', [CutiController::class, 'getPegawaiSaldoCuti'])->name('cuti.getPegawaiSaldoCuti');
    Route::get('get-approval', [CutiController::class, 'getApproval'])->name('cuti.getApproval');
    Route::post('simpan-cuti', [CutiController::class, 'simpanCuti'])->name('cuti.simpanCuti');
    Route::post('update-saldo-cuti', [CutiController::class, 'updateSaldoCuti'])->name('cuti.updateSaldoCuti');
    Route::post('update-status-saldo-cuti', [CutiController::class, 'updateStatusSaldoCuti'])->name('cuti.updateStatusSaldoCuti');
    Route::post('approval', [CutiController::class, 'approval'])->name('cuti.approval');
    Route::post('export', [CutiController::class, 'export'])->name('cuti.export');
    Route::get('cuti-by-pegawai-id', [CutiController::class, 'cutiByPegawaiId'])->name('cuti.cutiByPegawaiId');
    Route::post('del-cuti', [CutiController::class, 'delCuti'])->name('izin.delCuti');
    Route::get('cetak-cuti', [CutiController::class, 'cetakCuti'])->name('izin.cetakCuti');
    Route::get('validate-sign-cuti', [CutiController::class, 'validateSignCuti'])->name('izin.validateSignCuti');
});

Route::group(['prefix' => 'persuratan'], function () {
    Route::get('get-divisi-pegawai', [PersuratanController::class, 'getDivisiPegawai'])->name('persuratan.getDivisiPegawai');
    Route::get('get-nomor-surat', [PersuratanController::class, 'getNomorSurat'])->name('persuratan.getNomorSurat');
    Route::get('get-surat-keluar', [PersuratanController::class, 'getSuratKeluar'])->name('persuratan.getSuratKeluar');
    Route::get('get-surat-keluar-divisi', [PersuratanController::class, 'getSuratKeluarDivisi'])->name('persuratan.getSuratKeluarDivisi');
    Route::get('get-surat-masuk', [PersuratanController::class, 'getSuratMasuk'])->name('persuratan.getSuratMasuk');
    Route::get('get-surat-masuk-divisi', [PersuratanController::class, 'getSuratMasukDivisi'])->name('persuratan.getSuratMasukDivisi');
    Route::get('generate-nomor-surat', [PersuratanController::class, 'generateNomorSurat'])->name('persuratan.generateNomorSurat');
    Route::get('download-template-surat', [PersuratanController::class, 'downloadTemplateSurat'])->name('persuratan.downloadTemplateSurat');
    Route::post('reset-nomor-surat', [PersuratanController::class, 'resetNomorSurat'])->name('persuratan.resetNomorSurat');
    Route::post('simpan-surat-keluar', [PersuratanController::class, 'simpanSuratKeluar'])->name('persuratan.simpanSuratKeluar');
    Route::post('simpan-disposisi', [PersuratanController::class, 'simpanDisposisi'])->name('persuratan.simpanDisposisi');
    Route::post('cetak-surat', [PersuratanController::class, 'cetakSurat'])->name('persuratan.cetakSurat');
    Route::get('get-disposisi-keluar', [PersuratanController::class, 'getDisposisiKeluar'])->name('persuratan.getDisposisiKeluar');
    Route::get('get-disposisi-masuk', [PersuratanController::class, 'getDisposisiMasuk'])->name('persuratan.getDisposisiMasuk');
    Route::get('get-surat-keluar-by-id', [PersuratanController::class, 'getSuratKeluarById'])->name('persuratan.getSuratKeluarById');
    Route::post('approve-surat', [PersuratanController::class, 'approveSurat'])->name('persuratan.approveSurat');
    Route::post('unapprove-surat', [PersuratanController::class, 'unapproveSurat'])->name('persuratan.unproveSurat');
    Route::post('send-review', [PersuratanController::class, 'SendReview'])->name('persuratan.sendReview');
    Route::post('read-status', [PersuratanController::class, 'readStatus'])->name('persuratan.readStatus');
    Route::get('preview-file', [PersuratanController::class, 'previewFile'])->name('persuratan.previewFile');
    Route::post('delete-surat', [PersuratanController::class, 'deleteSurat'])->name('persuratan.deleteSurat');
});

Route::group(['prefix' => 'sppd'], function () {
    Route::get('get-sppd', [SppdController::class, 'getSppd'])->name('sppd.getSppd');
    Route::get('get-detail', [SppdController::class, 'getDetail'])->name('sppd.getDetail');
    Route::get('get-master-lumpsump', [SppdController::class, 'getMasterLumpsump'])->name('sppd.getMasterLumpsump');
    Route::get('get-approval', [SppdController::class, 'getApproval'])->name('sppd.getApproval');
    Route::post('approval-sppd', [SppdController::class, 'approvalSppd'])->name('sppd.approvalSppd');
    Route::get('get-all-sppd', [SppdController::class, 'getAllSppd'])->name('sppd.getAllSppd');
    // Route::get('view-sppd', [SppdController::class, 'indexSppd'])->name('sppd.getSppd');
    Route::get('cetak-sppd', [SppdController::class, 'cetak_sppd'])->name('sppd.cetak_sppd');
    Route::get('rincian-biaya-sppd', [SppdController::class, 'rincianBiayaSppd'])->name('sppd.rincianBiayaSppd');
    // Route::get('export-sppd', [SppdController::class, 'export'])->name('sppd.getSppd');
    Route::get('view-sppd/cetak-pdf', [SppdController::class, 'cetak_sppd']);
    Route::post('del-sppd', [SppdController::class, 'delSppd'])->name('sppd.delSppd');
    Route::post('simpan-sppd', [SppdController::class, 'simpanSppd'])->name('sppd.simpanSppd');
    Route::put('update-sppd/{id}', [SppdController::class, 'updateSppd'])->name('sppd.updateSppd');
    Route::get('sppd-by-pegawai-id', [SppdController::class, 'sppdByPegawaiId'])->name('sppd.sppdByPegawaiId');
    Route::get('sppd-by-pegawai-id-mobile', [SppdController::class, 'sppdByPegawaiIdMobile'])->name('sppd.sppdByPegawaiIdMobile');
    Route::get('get-summary-pegawai', [SppdController::class, 'getSummaryPegawai'])->name('sppd.getSummaryPegawai');
    Route::get('sppd-by-jenis', [SppdController::class, 'sppdByJenis'])->name('sppd.sppdByJenis');
    Route::get('get-tahapan', [SppdController::class, 'getTahapan'])->name('sppd.getTahapan');
});

Route::group(['prefix' => 'announcements'], function () {
    Route::get('get', [AnnouncementsController::class, 'getAnnoun'])->name('announcements.getAnnoun');
    Route::get('get-limit', [AnnouncementsController::class, 'getAnnounLimit'])->name('announcements.getAnnounLimit');
    Route::post('simpan-announcement', [AnnouncementsController::class, 'simpanAnnouncement'])->name('announcements.simpanAnnouncement');
    Route::post('del-announcement', [AnnouncementsController::class, 'delAnnouncement'])->name('announcements.delAnnouncement');
    Route::post('update-announcement', [AnnouncementsController::class, 'updateAnnouncement'])->name('announcements.updateAnnouncement');
});

Route::group(['prefix' => 'mutasi'], function () {
    Route::post('simpan-draft-mutasi', [MutasiController::class, 'simpanDraftMutasi'])->name('mutasi.simpanDraftMutasi');
    Route::get('get-draft-mutasi', [MutasiController::class, 'getDraftMutasi'])->name('mutasi.getDraftMutasi');
    Route::get('get-pegawai-for-mutasi', [MutasiController::class, 'getPegawaiForMutasi'])->name('mutasi.getPegawaiForMutasi');
    Route::post('simpan-draft-pegawai', [MutasiController::class, 'simpanDraftPegawai'])->name('mutasi.simpanDraftPegawai');
    Route::get('get-list-mutasi', [MutasiController::class, 'getListMutasi'])->name('mutasi.getListMutasi');
    Route::post('release-draft-mutasi', [MutasiController::class, 'releaseDraftMutasi'])->name('mutasi.releaseDraftMutasi');
    Route::post('hapus-draft-mutasi', [MutasiController::class, 'hapusDraftMutasi'])->name('mutasi.hapusDraftMutasi');
    // Route::get('cetak-mutasi', [MutasiController::class, 'cetakMutasi'])->name('mutasi.cetakMutasi');
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('riw_mutasi', [RiwMutasiController::class, 'get']);
Route::get('riw_mutasis/{id}', [RiwMutasiController::class, 'get']);
Route::post('riw_mutasis', [RiwMutasiController::class, 'store']);
Route::post('riw_mutasis_update/{id}', [RiwMutasiController::class, 'update']);
Route::post('delete_riw_mutasis/{id}', [RiwMutasiController::class, 'destroy']);