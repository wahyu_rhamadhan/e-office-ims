<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PegawaiController;
use App\Http\Controllers\IzinController;
use App\Http\Controllers\SppdController;
use Illuminate\Http\Request;
use App\Http\Controllers\PDFController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    echo 'welcome';
});



Route::get('/exportlaporan', [PDFController::class, 'export']);

// Route::get('/view_sppd', [SppdController::class, 'index'])->name('izin.getRiwIzin');
// Route::get('/view_sppd/cetak_pdf', [SppdController::class, 'cetak_pdf']);